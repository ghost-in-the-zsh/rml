/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class Vector3fTest {

    @Test
    public void testConstructorFromPrimitiveFloat2() {
        Vector3 v = new Vector3f(2f, 3f);

        assertEquals(0, Float.compare(v.x(), 2f));
        assertEquals(0, Float.compare(v.y(), 3f));
        assertEquals(0, Float.compare(v.z(), 0f));
    }

    @Test
    public void testConstructorFromPrimitiveFloat3() {
        Vector3 v = new Vector3f(2f, 3f, 4f);

        assertEquals(0, Float.compare(v.x(), 2f));
        assertEquals(0, Float.compare(v.y(), 3f));
        assertEquals(0, Float.compare(v.z(), 4f));
    }

    @Test
    public void testConstructorFromFloatArray() {
        Vector3 v = new Vector3f(new float[] { 2f, 3f, 4f });

        assertEquals(0, Float.compare(v.x(), 2f));
        assertEquals(0, Float.compare(v.y(), 3f));
        assertEquals(0, Float.compare(v.z(), 4f));
    }

    @Test
    public void testConstructorFromFloatArrayZeroesOutOptionalZ() {
        Vector3 v = new Vector3f(new float[] { 2f, 3f });

        assertEquals(0, Float.compare(v.x(), 2f));
        assertEquals(0, Float.compare(v.y(), 3f));
        assertEquals(0, Float.compare(v.z(), 0f));
    }

    @Test
    public void testConstructorFromFloatArrayIgnoresExtraInput() {
        new Vector3f(new float[] { 1f, 2f, 3f, 4f });
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromFloatArrayFailsOnIncompleteInput() {
        new Vector3f(new float[] { 1f });
    }

    @Test
    public void testConstructorFromPoint() {
        Point3 p = new Point3f(1, 2, 3);
        Vector3 v = new Vector3f(p);

        assertEquals(0, Float.compare(p.x(), v.x()));
        assertEquals(0, Float.compare(p.y(), v.y()));
        assertEquals(0, Float.compare(p.z(), v.z()));
    }

    @Test
    public void testZeroVectorFactory() {
        Vector3 v = Vector3f.zero();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 0f));
    }

    @Test
    public void testUnitXVectorFactory() {
        Vector3 v = Vector3f.unitX();

        assertEquals(0, Float.compare(v.x(), 1f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 0f));
    }

    @Test
    public void testUnitYVectorFactory() {
        Vector3 v = Vector3f.unitY();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 1f));
        assertEquals(0, Float.compare(v.z(), 0f));
    }

    @Test
    public void testUnitZVectorFactory() {
        Vector3 v = Vector3f.unitZ();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 1f));
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatPrimitive2() {
        Vector3 v = Vector3f.normalize(2f, 4f);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatPrimitive3() {
        Vector3 v = Vector3f.normalize(2f, 4f, 6f);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatArray() {
        Vector3 v = Vector3f.normalize(new float[] { 2f, 4f, 7f });

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatArrayZeroesOutOptionalZ() {
        Vector3 v = Vector3f.normalize(new float[] { 2f, 4f });

        assertEquals(v.z(), 0f);
        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromPoint() {
        Vector3 v = Vector3f.normalize(new Point3f(2f, 4f, 7f));

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testNormalizedVectorFactoryFromFloatArrayRejectsIncompleteInput() {
        Vector3f.normalize(new float[] { 2f });
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatArrayRejectsExtraInput() {
        Vector3f.normalize(new float[] { 2f, 2f, 2f, 4f });
    }

    @Test(expectedExceptions = { ArithmeticException.class })
    public void testNormalizedVectorFactoryRejectsZeroLengthVector() {
        Vector3f.normalize(0, 0, 0);
    }

    @Test
    public void testVectorAddition() {
        Vector3 v1 = new Vector3f(1f, 2f, 3f);
        Vector3 v2 = new Vector3f(4f, 5f, 6f);
        Vector3 rv = v1.add(v2);

        assertEquals(0, Float.compare(5f, rv.x()));
        assertEquals(0, Float.compare(7f, rv.y()));
        assertEquals(0, Float.compare(9f, rv.z()));
    }

    @Test
    public void testScalarAddition() {
        Vector3 v1 = new Vector3f(1f, 2f, 3f);
        Vector3 rv = v1.add(4f, 5f, 6f);

        assertEquals(0, Float.compare(5f, rv.x()));
        assertEquals(0, Float.compare(7f, rv.y()));
        assertEquals(0, Float.compare(9f, rv.z()));
    }

    @Test
    public void testVectorSubtraction() {
        Vector3 v1 = new Vector3f(5f, 5f, 4f);
        Vector3 v2 = new Vector3f(3f, 2f, 4f);
        Vector3 rv = v1.sub(v2);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(3f, rv.y()));
        assertEquals(0, Float.compare(0f, rv.z()));
    }

    @Test
    public void testScalarSubtraction() {
        Vector3 v1 = new Vector3f(5f, 5f, 4f);
        Vector3 rv = v1.sub(3f, 2f, 4f);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(3f, rv.y()));
        assertEquals(0, Float.compare(0f, rv.z()));
    }

    @Test
    public void testVectorRotationAroundVector() {
        Angle angle = new Degreef(180f);
        Vector3 axis = Vector3f.unitZ();

        Vector3 v1 = new Vector3f(1, 1, 1);
        Vector3 v2 = Matrix3f.rotation(angle, axis).mult(v1);

        // https://en.wikipedia.org/wiki/Rotation_matrix#Common_rotations
        // since the rotation is only about the z-axis, only the x/y values
        // should change
        assertEquals(-1f, v2.x(), TestUtil.TOLERANCE);
        assertEquals(-1f, v2.y(), TestUtil.TOLERANCE);
        assertEquals(1f, v2.z());
    }

    @Test
    public void testScalarMultiplicationFloat() {
        Vector3 v1 = new Vector3f(4f, 3f, 2f);
        Vector3 rv = v1.mult(2f);

        assertEquals(0, Float.compare(8f, rv.x()));
        assertEquals(0, Float.compare(6f, rv.y()));
        assertEquals(0, Float.compare(4f, rv.z()));
    }

    @Test
    public void testScalarDivisionFloat() {
        Vector3 v1 = new Vector3f(4f, 8f, 12f);
        Vector3 rv = v1.div(2f);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(4f, rv.y()));
        assertEquals(0, Float.compare(6f, rv.z()));
    }

    @Test
    public void testVectorDotProduct() {
        Vector3 v1 = new Vector3f(2f, 3f, 4f);
        Vector3 v2 = new Vector3f(5f, 6f, 7f);
        float p = dot(v1, v2);

        assertEquals(0, Float.compare(p, v1.dot(v2)));
    }

    @Test
    public void testAngleBetweenVectors() {
        Vector3 v = Vector3f.unitX();
        Vector3 s = Vector3f.unitY();
        Angle a = v.angleTo(s);
        Angle b = new Radianf((float) Math.acos(v.dot(s)));

        assertEquals(0, Float.compare(a.valueRadians(), b.valueRadians()));
    }

    @Test
    public void testVectorCrossProduct() {
        Vector3 v1 = new Vector3f(2f, 3f, 4f);
        Vector3 v2 = new Vector3f(5f, 6f, 7f);
        Vector3 cp = cross(v1, v2);
        Vector3 cv = v1.cross(v2);

        assertTrue(cv.equals(cp));
        assertTrue(cp.equals(cv));

        assertTrue(cv.hashCode() == cp.hashCode());
    }

    @Test
    public void testVectorNormalization() {
        final float x = 2f;
        final float y = 3f;
        final float z = 4f;
        final float oneOverLen = 1f / (float) Math.sqrt(x * x + y * y + z * z);
        Vector3 v = new Vector3f(x, y, z);
        Vector3 nv1 = new Vector3f(x * oneOverLen, y * oneOverLen, z * oneOverLen);
        Vector3 nv2 = v.normalize();

        assertEquals(0, Float.compare(nv1.x(), nv2.x()));
        assertEquals(0, Float.compare(nv1.y(), nv2.y()));
        assertEquals(0, Float.compare(nv1.z(), nv2.z()));

        assertEquals(0, Float.compare(nv1.length(), 1f));
        assertEquals(0, Float.compare(nv1.length(), nv2.length()));
    }

    @Test(expectedExceptions = { ArithmeticException.class })
    public void testZeroLengthVectorNormalizationThrowsException() {
        Vector3f.zero().normalize();
    }

    @Test
    public void testFloatArrayValues() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;
        Vector3 v = new Vector3f(x, y, z);
        float[] vals = v.toFloatArray();

        assertTrue(vals.length == 3);

        assertEquals(0, Float.compare(vals[0], x));
        assertEquals(0, Float.compare(vals[1], y));
        assertEquals(0, Float.compare(vals[2], z));

        assertEquals(0, Float.compare(vals[0], v.x()));
        assertEquals(0, Float.compare(vals[1], v.y()));
        assertEquals(0, Float.compare(vals[2], v.z()));
    }

    @Test
    public void testVectorLength() {
        Vector3 v = new Vector3f(2f, 3f, 4f);

        assertEquals(0, Float.compare(length(v), v.length()));
    }

    @Test
    public void testVectorLengthSquared() {
        Vector3 v = new Vector3f(2f, 3f, 4f);

        float squaredLength = length(v);
        squaredLength *= squaredLength;
        assertEquals(0, Float.compare(squaredLength, v.lengthSquared()));
    }

    @Test
    public void testVectorIsZeroLength() {
        Vector3 v = Vector3f.zero();

        assertTrue(v.isZeroLength());
        assertTrue(v.length() == 0);
    }

    @Test
    public void testLerp() {
        final float t = 0.5f;
        final float x1 = 0f;
        final float y1 = 0f;
        final float z1 = 0f;
        final float x2 = 1f;
        final float y2 = 0f;
        final float z2 = 0f;

        // lerp(p0, p1, t) = (1 - t) * p0 + t * p1
        // https://en.wikipedia.org/wiki/Slerp#Geometric_Slerp
        Vector3 p0 = new Vector3f(x1, y1, z1);
        Vector3 p1 = new Vector3f(x2, y2, z2);

        // temp values
        Vector3 t0 = p0.mult(1.0f - t);
        Vector3 t1 = p1.mult(t);

        // interpolated result
        Vector3 p2 = t0.add(t1);

        assertTrue(p0.lerp(p1, t).equals(p2));
    }

    @Test
    public void testLerpParameterClampedMax() {
        final float t = 1.25f;
        Vector3 v0 = Vector3f.unitX();
        Vector3 v1 = Vector3f.unitY();

        Vector3 v2 = v0.lerp(v1, t);
        Vector3 v3 = v0.lerp(v1, 1);

        assertTrue(v2.equals(v3));
    }

    @Test
    public void testLerpParameterClampedMin() {
        final float t = -1.25f;
        Vector3 v0 = Vector3f.unitX();
        Vector3 v1 = Vector3f.unitY();

        Vector3 v2 = v0.lerp(v1, t);
        Vector3 v3 = v0.lerp(v1, 0);

        assertTrue(v2.equals(v3));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testLerpThrowsNullPointerException() {
        Vector3 v0 = Vector3f.unitX();
        v0.lerp(null, 0.5f);
    }

    @Test
    public void testNegationMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        Vector3 v = new Vector3f(x, y, z);
        Vector3 n = v.negate();

        assertEquals(0, Float.compare(n.x(), -x));
        assertEquals(0, Float.compare(n.y(), -y));
        assertEquals(0, Float.compare(n.z(), -z));
    }

    @Test
    public void testComparison() {
        Vector3 v0 = new Vector3f(0f, 0f, 0f);
        Vector3 v1 = new Vector3f(1f, 1f, 1f);
        Vector3 v2 = new Vector3f(2f, 2f, 2f);

        assertEquals(0, v1.compareTo(v1));
        assertEquals(1, v1.compareTo(v0));
        assertEquals(-1, v1.compareTo(v2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Vector3 v = new Vector3f(1f, 1f, 1f);

        assertTrue(v.equals(v));
        assertFalse(v.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Vector3 v1 = new Vector3f(1f, 1f, 1f);
        Vector3 v2 = new Vector3f(1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v2.equals(v1));

        assertTrue(v1.hashCode() == v2.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Vector3 v1 = new Vector3f(1f, 1f, 1f);
        Vector3 v2 = new Vector3f(1f, 1f, 1f);
        Vector3 v3 = new Vector3f(1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v2.equals(v3));
        assertTrue(v1.equals(v3));

        assertTrue(v1.hashCode() == v2.hashCode());
        assertTrue(v2.hashCode() == v3.hashCode());
        assertTrue(v1.hashCode() == v3.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
        assertFalse(v3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Vector3 v1 = new Vector3f(1f, 1f, 1f);
        Vector3 v2 = new Vector3f(1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v1.equals(v2));

        assertTrue(v1.hashCode() == v2.hashCode());
        assertTrue(v1.hashCode() == v2.hashCode());

        v1 = new Vector3f(1.5f, 1.5f, 1.5f);
        assertFalse(v1.equals(v2));
        assertFalse(v1.hashCode() == v2.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Vector3f.zero().equals(null));
        assertFalse(new Vector3f(1f, 2f, 3f).equals(null));
    }

    @Test
    public void testToString() {
        Vector3 zv = Vector3f.zero();

        assertEquals(Vector3f.class.getSimpleName() + "(0.0, 0.0, 0.0)", zv.toString());
    }

    // ========================================================================
    // Helper implementation methods
    // ========================================================================

    private static float length(final Vector3 v) {
        float x = v.x() * v.x();
        float y = v.y() * v.y();
        float z = v.z() * v.z();
        return (float) Math.sqrt(x + y + z);
    }

    private static float dot(final Vector3 lv, final Vector3 rv) {
        float x = lv.x() * rv.x();
        float y = lv.y() * rv.y();
        float z = lv.z() * rv.z();
        return x + y + z;
    }

    private static Vector3 cross(final Vector3 lv, final Vector3 rv) {
        float x = lv.y() * rv.z() - lv.z() * rv.y();
        float y = lv.z() * rv.x() - lv.x() * rv.z();
        float z = lv.x() * rv.y() - lv.y() * rv.x();
        return new Vector3f(x, y, z);
    }

}
