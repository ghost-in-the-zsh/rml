/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class Vector4fTest {

    @Test
    public void testConstructorFromPrimitiveFloat3() {
        final float x = 2;
        final float y = 3;
        final float z = 4;
        Vector4 v = new Vector4f(x, y, z);

        assertEquals(0, Float.compare(v.x(), x));
        assertEquals(0, Float.compare(v.y(), y));
        assertEquals(0, Float.compare(v.z(), z));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testConstructorFromPrimitiveFloat4() {
        final float x = 2;
        final float y = 3;
        final float z = 4;
        final float w = 5;
        Vector4 v = new Vector4f(x, y, z, w);

        assertEquals(0, Float.compare(v.x(), x));
        assertEquals(0, Float.compare(v.y(), y));
        assertEquals(0, Float.compare(v.z(), z));
        assertEquals(0, Float.compare(v.w(), w));
    }

    @Test
    public void testConstructorFromArray() {
        final float[] values = new float[] { 2f, 3f, 4f, 5f };
        Vector4 v = new Vector4f(values);

        assertEquals(0, Float.compare(v.x(), values[0]));
        assertEquals(0, Float.compare(v.y(), values[1]));
        assertEquals(0, Float.compare(v.z(), values[2]));
        assertEquals(0, Float.compare(v.w(), values[3]));
    }

    @Test
    public void testConstructorFromArrayZeroesOutOptionalW() {
        final float[] values = new float[] { 2f, 3f, 4f };
        Vector4 v = new Vector4f(values);

        assertEquals(0, Float.compare(v.x(), values[0]));
        assertEquals(0, Float.compare(v.y(), values[1]));
        assertEquals(0, Float.compare(v.z(), values[2]));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testConstructorFromArrayIgnoresExtraInput() {
        new Vector4f(new float[] { 1f, 2f, 3f, 4f, 5f });
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromArrayRejectsIncompleteInput() {
        new Vector4f(new float[] { 1f });
    }

    @Test
    public void testConstructorFromVector3() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        Vector3 v1 = new Vector3f(x, y, z);
        Vector4 v2 = new Vector4f(v1);

        assertEquals(0, Float.compare(v2.x(), x));
        assertEquals(0, Float.compare(v2.y(), y));
        assertEquals(0, Float.compare(v2.z(), z));
        assertEquals(0, Float.compare(v2.w(), 0f));
    }

    @Test
    public void testConstructorFromVector3AndFloat() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        final float w = 4f;
        Vector3 v1 = new Vector3f(x, y, z);
        Vector4 v2 = new Vector4f(v1, w);

        assertEquals(0, Float.compare(v2.x(), x));
        assertEquals(0, Float.compare(v2.y(), y));
        assertEquals(0, Float.compare(v2.z(), z));
        assertEquals(0, Float.compare(v2.w(), w));
    }

    @Test
    public void testConstructorFromPoint3() {
        Point3 p = new Point3f(1, 2, 3);
        Vector4 v = new Vector4f(p);

        assertEquals(0, Float.compare(p.x(), v.x()));
        assertEquals(0, Float.compare(p.y(), v.y()));
        assertEquals(0, Float.compare(p.z(), v.z()));
        assertEquals(0, Float.compare(1f, v.w()));
    }

    @Test
    public void testConstructorFromPoint3AndFloat() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        final float w = 4f;
        Point3 p = new Point3f(x, y, z);
        Vector4 v = new Vector4f(p, w);

        assertEquals(0, Float.compare(v.x(), x));
        assertEquals(0, Float.compare(v.y(), y));
        assertEquals(0, Float.compare(v.z(), z));
        assertEquals(0, Float.compare(v.w(), w));
    }

    @Test
    public void testZeroVectorFactory() {
        Vector4 v = Vector4f.zero();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 0f));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testUnitXVectorFactory() {
        Vector4 v = Vector4f.unitX();

        assertEquals(0, Float.compare(v.x(), 1f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 0f));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testUnitYVectorFactory() {
        Vector4 v = Vector4f.unitY();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 1f));
        assertEquals(0, Float.compare(v.z(), 0f));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testUnitZVectorFactory() {
        Vector4 v = Vector4f.unitZ();

        assertEquals(0, Float.compare(v.x(), 0f));
        assertEquals(0, Float.compare(v.y(), 0f));
        assertEquals(0, Float.compare(v.z(), 1f));
        assertEquals(0, Float.compare(v.w(), 0f));
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatPrimitive3() {
        Vector4 v = Vector4f.normalize(2f, 4f, 6f);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatPrimitive4() {
        Vector4 v = Vector4f.normalize(2f, 4f, 6f, 2f);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromVector3() {
        Vector3 v = new Vector3f(4f, 8f, 16f);
        Vector4 n = Vector4f.normalize(v);

        assertEquals(1f, n.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromVector3AndPrimitiveFloat() {
        Vector3 v = new Vector3f(4f, 8f, 16f);
        Vector4 n = Vector4f.normalize(v, 1f);

        assertEquals(1f, n.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromPoint3() {
        Point3 p = new Point3f(4f, 8f, 16f);
        Vector4 v = Vector4f.normalize(p);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromPoint3AndPrimitiveFloat() {
        Point3 p = new Point3f(4f, 8f, 16f);
        Vector4 v = Vector4f.normalize(p, 1f);

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatArray() {
        Vector4 v = Vector4f.normalize(new float[] { 2f, 4f, 7f, 1f });

        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatArrayZeroesOutOptionalW() {
        Vector4 v = Vector4f.normalize(new float[] { 2f, 4f, 7f });

        assertEquals(v.w(), 0f);
        assertEquals(1f, v.length(), TestUtil.EPSILON);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testNormalizedVectorFactoryFromFloatFailsOnIncompleteInput() {
        Vector4f.normalize(new float[] { 2f });
    }

    @Test
    public void testNormalizedVectorFactoryFromFloatIgnoresExtraInput() {
        Vector4f.normalize(new float[] { 2f, 2f, 2f, 4f, 1f });
    }

    @Test(expectedExceptions = { ArithmeticException.class })
    public void testNormalizedVectorFactoryRejectsZeroLengthVector() {
        Vector4f.normalize(0, 0, 0);
    }

    @Test
    public void testVectorAddition() {
        Vector4 v0 = new Vector4f(1f, 2f, 3f, 1f);
        Vector4 v1 = new Vector4f(4f, 5f, 6f, 2f);
        Vector4 v2 = v0.add(v1);

        assertEquals(0, Float.compare(5f, v2.x()));
        assertEquals(0, Float.compare(7f, v2.y()));
        assertEquals(0, Float.compare(9f, v2.z()));
        assertEquals(0, Float.compare(3f, v2.w()));
    }

    @Test
    public void testScalarAdditionParam3() {
        Vector4 v0 = new Vector4f(1f, 2f, 3f, 1f);
        Vector4 v1 = v0.add(4f, 5f, 6f);

        assertEquals(0, Float.compare(5f, v1.x()));
        assertEquals(0, Float.compare(7f, v1.y()));
        assertEquals(0, Float.compare(9f, v1.z()));
        assertEquals(0, Float.compare(1f, v1.w()));
    }

    @Test
    public void testScalarAdditionParam4() {
        Vector4 v0 = new Vector4f(1f, 2f, 3f, 1f);
        Vector4 v1 = v0.add(4f, 5f, 6f, 7f);

        assertEquals(0, Float.compare(5f, v1.x()));
        assertEquals(0, Float.compare(7f, v1.y()));
        assertEquals(0, Float.compare(9f, v1.z()));
        assertEquals(0, Float.compare(8f, v1.w()));
    }

    @Test
    public void testVectorSubtraction() {
        Vector4 v0 = new Vector4f(5f, 5f, 4f, 1f);
        Vector4 v1 = new Vector4f(3f, 2f, 5f, 1f);
        Vector4 v2 = v0.sub(v1);

        assertEquals(0, Float.compare(2f, v2.x()));
        assertEquals(0, Float.compare(3f, v2.y()));
        assertEquals(0, Float.compare(-1f, v2.z()));
        assertEquals(0, Float.compare(0f, v2.w()));
    }

    @Test
    public void testScalarSubtractionParam3() {
        Vector4 v1 = new Vector4f(5f, 5f, 4f, 1f);
        Vector4 rv = v1.sub(3f, 2f, 4f);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(3f, rv.y()));
        assertEquals(0, Float.compare(0f, rv.z()));
        assertEquals(0, Float.compare(1f, rv.w()));
    }

    @Test
    public void testScalarSubtractionParam4() {
        Vector4 v1 = new Vector4f(5f, 5f, 4f, 1f);
        Vector4 rv = v1.sub(3f, 2f, 4f, 2f);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(3f, rv.y()));
        assertEquals(0, Float.compare(0f, rv.z()));
        assertEquals(0, Float.compare(-1f, rv.w()));
    }

    @Test
    public void testVectorRotationAroundVector() {
        Angle angle = new Degreef(180f);
        Vector4 axis = Vector4f.unitX();

        Vector4 v1 = new Vector4f(1, 1, 1);
        Vector4 v2 = Matrix4f.rotation(angle, axis).mult(v1);

        assertEquals(1f, v2.x());
        assertEquals(-1f, v2.y(), TestUtil.TOLERANCE);
        assertEquals(-1f, v2.z(), TestUtil.TOLERANCE);
        assertEquals(0f, v2.w());
    }

    @Test
    public void testScalarMultiplicationFloat() {
        Vector4 v1 = new Vector4f(4f, 3f, 2f, 1f);
        Vector4 rv = v1.mult(2f);

        assertEquals(0, Float.compare(8f, rv.x()));
        assertEquals(0, Float.compare(6f, rv.y()));
        assertEquals(0, Float.compare(4f, rv.z()));
        assertEquals(0, Float.compare(2f, rv.w()));
    }

    @Test
    public void testScalarDivisionFloat() {
        Vector4 v1 = new Vector4f(4f, 8f, 12f, 16f);
        Vector4 rv = v1.div(2f);

        assertEquals(0, Float.compare(2f, rv.x()));
        assertEquals(0, Float.compare(4f, rv.y()));
        assertEquals(0, Float.compare(6f, rv.z()));
        assertEquals(0, Float.compare(8f, rv.w()));
    }

    @Test
    public void testVectorDotProduct() {
        Vector4 v1 = new Vector4f(2f, 3f, 4f, 1f);
        Vector4 v2 = new Vector4f(5f, 6f, 7f, 1f);
        float p = dot(v1, v2);

        assertEquals(0, Float.compare(p, v1.dot(v2)));
    }

    @Test
    public void testAngleBetweenVectors() {
        Vector4 v = Vector4f.unitX();
        Vector4 s = Vector4f.unitY();
        Angle a = v.angleTo(s);
        Angle b = new Radianf((float) Math.acos(v.dot(s)));

        assertEquals(0, Float.compare(a.valueRadians(), b.valueRadians()));
    }

    @Test
    public void testVectorNormalization() {
        final float x = 2f;
        final float y = 3f;
        final float z = 4f;
        final float w = 1f;
        Vector4 v = new Vector4f(x, y, z, w);
        final float oneOverLen = 1f / length(v);

        Vector4 nv1 = new Vector4f(x * oneOverLen, y * oneOverLen, z * oneOverLen, w * oneOverLen);
        Vector4 nv2 = v.normalize();

        assertEquals(0, Float.compare(nv1.x(), nv2.x()));
        assertEquals(0, Float.compare(nv1.y(), nv2.y()));
        assertEquals(0, Float.compare(nv1.z(), nv2.z()));
        assertEquals(0, Float.compare(nv1.w(), nv2.w()));

        assertEquals(0, Float.compare(nv1.length(), 1f));
        assertEquals(0, Float.compare(nv1.length(), nv2.length()));
    }

    @Test(expectedExceptions = { ArithmeticException.class })
    public void testZeroLengthVectorNormalizationThrowsException() {
        Vector4f.zero().normalize();
    }

    @Test
    public void testFloatArrayValues() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;
        final float w = 1f;
        Vector4 v = new Vector4f(x, y, z, w);
        float[] vals = v.toFloatArray();

        assertTrue(vals.length == 4);

        assertEquals(0, Float.compare(vals[0], x));
        assertEquals(0, Float.compare(vals[1], y));
        assertEquals(0, Float.compare(vals[2], z));
        assertEquals(0, Float.compare(vals[3], w));

        assertEquals(0, Float.compare(vals[0], v.x()));
        assertEquals(0, Float.compare(vals[1], v.y()));
        assertEquals(0, Float.compare(vals[2], v.z()));
        assertEquals(0, Float.compare(vals[3], v.w()));
    }

    @Test
    public void testVectorLength() {
        final float x = 2f;
        final float y = 3f;
        final float z = 4f;
        final float w = 5f;
        Vector4 v = new Vector4f(x, y, z, w);

        assertEquals(0, Float.compare(length(v), v.length()));
    }

    @Test
    public void testVectorLengthSquared() {
        final float x = 2f;
        final float y = 3f;
        final float z = 4f;
        final float w = 1f;
        Vector4 v = new Vector4f(x, y, z, w);

        final float sqlen = x * x + y * y + z * z + w * w;
        assertEquals(0, Float.compare(sqlen, v.lengthSquared()));
    }

    @Test
    public void testToVector3() {
        Vector4 v4 = new Vector4f(1, 2, 3);
        Vector3 v3 = v4.toVector3();

        assertEquals(0, Float.compare(v3.x(), 1f));
        assertEquals(0, Float.compare(v3.y(), 2f));
        assertEquals(0, Float.compare(v3.z(), 3f));
    }

    @Test
    public void testVectorIsZeroLength() {
        Vector4 v = Vector4f.zero();

        assertTrue(v.isZeroLength());
        assertTrue(v.length() == 0);
    }

    @Test
    public void testLerp() {
        final float t = 0.75f;
        final float x1 = 0f;
        final float y1 = 0f;
        final float z1 = 0f;
        final float x2 = 1f;
        final float y2 = 0f;
        final float z2 = 0f;

        // lerp(p0, p1, t) = (1 - t) * p0 + t * p1
        // https://en.wikipedia.org/wiki/Slerp#Geometric_Slerp
        Vector4 p0 = new Vector4f(x1, y1, z1);
        Vector4 p1 = new Vector4f(x2, y2, z2);

        // temp values
        Vector4 t0 = p0.mult(1.0f - t);
        Vector4 t1 = p1.mult(t);

        // interpolated result
        Vector4 p2 = t0.add(t1);

        assertTrue(p0.lerp(p1, t).equals(p2));
    }

    @Test
    public void testLerpParameterClampedMax() {
        final float t = 1.25f;
        Vector4 v0 = Vector4f.unitX();
        Vector4 v1 = Vector4f.unitY();

        Vector4 v2 = v0.lerp(v1, t);
        Vector4 v3 = v0.lerp(v1, 1);

        assertTrue(v2.equals(v3));
    }

    @Test
    public void testLerpParameterClampedMin() {
        final float t = -1.25f;
        Vector4 v0 = Vector4f.unitX();
        Vector4 v1 = Vector4f.unitY();

        Vector4 v2 = v0.lerp(v1, t);
        Vector4 v3 = v0.lerp(v1, 0);

        assertTrue(v2.equals(v3));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testLerpThrowsNullPointerException() {
        Vector4 v0 = Vector4f.unitX();
        v0.lerp(null, 0.5f);
    }

    @Test
    public void testNegationMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        final float w = 4f;
        Vector4 v = new Vector4f(x, y, z, w);
        Vector4 n = v.negate();

        assertEquals(0, Float.compare(n.x(), -x));
        assertEquals(0, Float.compare(n.y(), -y));
        assertEquals(0, Float.compare(n.z(), -z));
        assertEquals(0, Float.compare(n.w(), -w));
    }

    @Test
    public void testComparison() {
        Vector4 v0 = new Vector4f(0f, 0f, 0f, 0f);
        Vector4 v1 = new Vector4f(1f, 1f, 1f, 1f);
        Vector4 v2 = new Vector4f(2f, 2f, 2f, 2f);

        assertEquals(0, v1.compareTo(v1));
        assertEquals(1, v1.compareTo(v0));
        assertEquals(-1, v1.compareTo(v2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Vector4 v = new Vector4f(1f, 1f, 1f, 1f);

        assertTrue(v.equals(v));
        assertFalse(v.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Vector4 v1 = new Vector4f(1f, 1f, 1f, 1f);
        Vector4 v2 = new Vector4f(1f, 1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v2.equals(v1));

        assertTrue(v1.hashCode() == v2.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Vector4 v1 = new Vector4f(1f, 1f, 1f, 1f);
        Vector4 v2 = new Vector4f(1f, 1f, 1f, 1f);
        Vector4 v3 = new Vector4f(1f, 1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v2.equals(v3));
        assertTrue(v1.equals(v3));

        assertTrue(v1.hashCode() == v2.hashCode());
        assertTrue(v2.hashCode() == v3.hashCode());
        assertTrue(v1.hashCode() == v3.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
        assertFalse(v3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Vector4 v1 = new Vector4f(1f, 1f, 1f, 1f);
        Vector4 v2 = new Vector4f(1f, 1f, 1f, 1f);

        assertTrue(v1.equals(v2));
        assertTrue(v1.equals(v2));

        assertTrue(v1.hashCode() == v2.hashCode());
        assertTrue(v1.hashCode() == v2.hashCode());

        v1 = new Vector4f(1.5f, 1.5f, 1.5f, 1.5f);
        assertFalse(v1.equals(v2));
        assertFalse(v1.hashCode() == v2.hashCode());

        assertFalse(v1.equals(null));
        assertFalse(v2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Vector4f.zero().equals(null));
        assertFalse(new Vector4f(1f, 2f, 3f, 4f).equals(null));
    }

    @Test
    public void testToString() {
        Vector4 zv = Vector4f.zero();

        assertEquals(Vector4f.class.getSimpleName() + "(0.0, 0.0, 0.0, 0.0)", zv.toString());
    }

    // ========================================================================
    // Helper implementation methods
    // ========================================================================

    private static float length(final Vector4 v) {
        float x = v.x() * v.x();
        float y = v.y() * v.y();
        float z = v.z() * v.z();
        float w = v.w() * v.w();
        return (float) Math.sqrt(x + y + z + w);
    }

    private static float dot(final Vector4 lv, final Vector4 rv) {
        return lv.x() * rv.x() + lv.y() * rv.y() + lv.z() * rv.z() + lv.w() * rv.w();
    }

}
