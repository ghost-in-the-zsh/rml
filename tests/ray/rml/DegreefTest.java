/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class DegreefTest {

    @Test
    public void testConstructorInputDegreeValue() {
        Angle a = new Degreef(0);

        assertEquals(0, Float.compare(a.valueDegrees(), 0f));
        assertEquals(0, Float.compare(a.valueRadians(), 0f));
    }

    @Test
    public void testConstructorInputRadianValue() {
        Angle a = new Degreef(new Radianf((float) Math.PI));

        assertEquals(0, Float.compare(a.valueDegrees(), 180f));
        assertEquals(0, Float.compare(a.valueRadians(), (float) Math.PI));
    }

    @Test
    public void testAddition() {
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(2);
        Angle ar = a1.add(a2);

        assertEquals(0, Float.compare(ar.valueDegrees(), 7f));
    }

    @Test
    public void testSubtraction() {
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(2);
        Angle ar = a1.sub(a2);

        assertEquals(0, Float.compare(ar.valueDegrees(), 3f));
    }

    @Test
    public void testMultiplicationByScalar() {
        Angle a = new Degreef(5);
        Angle r = a.mult(2f);

        assertEquals(0, Float.compare(r.valueDegrees(), 10f));
    }

    @Test
    public void testMultiplicationByAngle() {
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(2);
        Angle ar = a1.mult(a2);

        assertEquals(0, Float.compare(ar.valueDegrees(), 10f));
    }

    @Test
    public void testDivision() {
        Angle a = new Degreef(10);
        Angle r = a.div(2f);

        assertEquals(0, Float.compare(r.valueDegrees(), 5f));
    }

    @Test
    public void testNegationMatches() {
        final float value = 10;
        Angle a = new Degreef(value);
        Angle r = a.negate();

        assertEquals(0, Float.compare(r.valueDegrees(), -value));
    }

    @Test
    public void testComparison() {
        Angle a0 = new Degreef(0);
        Angle a1 = new Degreef(1);
        Angle a2 = new Degreef(2);

        assertEquals(0, a1.compareTo(a1));
        assertEquals(1, a1.compareTo(a0));
        assertEquals(-1, a1.compareTo(a2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Angle a = new Degreef(5);

        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(5);

        assertTrue(a1.equals(a2));
        assertTrue(a2.equals(a1));

        assertTrue(a1.hashCode() == a2.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(5);
        Angle a3 = new Degreef(5);

        assertTrue(a1.equals(a2));
        assertTrue(a2.equals(a3));
        assertTrue(a1.equals(a3));

        assertTrue(a1.hashCode() == a2.hashCode());
        assertTrue(a2.hashCode() == a3.hashCode());
        assertTrue(a1.hashCode() == a3.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
        assertFalse(a3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Angle a1 = new Degreef(5);
        Angle a2 = new Degreef(5);

        assertTrue(a1.equals(a2));
        assertTrue(a1.equals(a2));

        assertTrue(a1.hashCode() == a2.hashCode());
        assertTrue(a1.hashCode() == a2.hashCode());

        a1 = new Degreef(180);
        assertFalse(a1.equals(a2));
        assertFalse(a1.hashCode() == a2.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(new Degreef(5).equals(null));
    }

    @Test
    public void testToString() {
        assertEquals(Degreef.class.getSimpleName() + "(5.0)", new Degreef(5).toString());
    }
}
