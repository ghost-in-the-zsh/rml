/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class RadianfTest {

    @Test
    public void testConstructorInputRadianValue() {
        Angle a = new Radianf(0);

        assertEquals(0, Float.compare(a.valueRadians(), 0f));
        assertEquals(0, Float.compare(a.valueDegrees(), 0f));
    }

    @Test
    public void testConstructorInputDegreeValue() {
        Angle a = new Radianf(new Degreef(180));

        assertEquals(0, Float.compare(a.valueRadians(), (float) Math.PI));
        assertEquals(0, Float.compare(a.valueDegrees(), 180f));
    }

    @Test
    public void testAddition() {
        Angle a1 = new Radianf(1);
        Angle a2 = new Radianf(2);
        Angle ar = a1.add(a2);

        assertEquals(0, Float.compare(ar.valueRadians(), 3f));
    }

    @Test
    public void testSubtraction() {
        Angle a1 = new Radianf(5);
        Angle a2 = new Radianf(2);
        Angle ar = a1.sub(a2);

        assertEquals(0, Float.compare(ar.valueRadians(), 3f));
    }

    @Test
    public void testMultiplicationByScalar() {
        float twoPI = (float) (2.0 * Math.PI);
        Angle a = new Radianf((float) Math.PI);
        Angle r = a.mult(2f);

        assertEquals(0, Float.compare(r.valueRadians(), twoPI));
    }

    @Test
    public void testMultiplicationByRadian() {
        float squaredPI = (float) Math.PI * (float) Math.PI;
        Angle a1 = new Radianf((float) Math.PI);
        Angle a2 = new Radianf((float) Math.PI);
        Angle ar = a1.mult(a2);

        assertEquals(0, Float.compare(ar.valueRadians(), squaredPI));
    }

    @Test
    public void testDivision() {
        Angle a = new Radianf((float) Math.PI);
        Angle r = a.div(2f);

        assertEquals(0, Float.compare(r.valueRadians(), (float) (Math.PI / 2.0)));
    }

    @Test
    public void testNegationMatches() {
        final float value = 1.5f;
        Angle a = new Radianf(value);
        Angle r = a.negate();

        assertEquals(0, Float.compare(r.valueRadians(), -value));
    }

    @Test
    public void testComparison() {
        Angle a0 = new Radianf(0);
        Angle a1 = new Radianf(1);
        Angle a2 = new Radianf(2);

        assertEquals(0, a1.compareTo(a1));
        assertEquals(1, a1.compareTo(a0));
        assertEquals(-1, a1.compareTo(a2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Angle a = new Radianf(2);

        assertTrue(a.equals(a));
        assertFalse(a.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Angle a1 = new Radianf(5);
        Angle a2 = new Radianf(5);

        assertTrue(a1.equals(a2));
        assertTrue(a2.equals(a1));

        assertTrue(a1.hashCode() == a2.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Angle a1 = new Radianf(5);
        Angle a2 = new Radianf(5);
        Angle a3 = new Radianf(5);

        assertTrue(a1.equals(a2));
        assertTrue(a2.equals(a3));
        assertTrue(a1.equals(a3));

        assertTrue(a1.hashCode() == a2.hashCode());
        assertTrue(a2.hashCode() == a3.hashCode());
        assertTrue(a1.hashCode() == a3.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
        assertFalse(a3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Angle a1 = new Radianf(5);
        Angle a2 = new Radianf(5);

        assertTrue(a1.equals(a2));
        assertTrue(a1.equals(a2));

        assertTrue(a1.hashCode() == a2.hashCode());
        assertTrue(a1.hashCode() == a2.hashCode());

        a1 = new Radianf((float) Math.PI);
        assertFalse(a1.equals(a2));
        assertFalse(a1.hashCode() == a2.hashCode());

        assertFalse(a1.equals(null));
        assertFalse(a2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(new Radianf(5).equals(null));
    }

    @Test
    public void testToString() {
        float pi = (float) Math.PI;
        assertEquals(Radianf.class.getSimpleName() + "(" + pi + ")", new Radianf(pi).toString());
    }
}
