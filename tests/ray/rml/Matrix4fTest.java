/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class Matrix4fTest {

    @Test
    public void testConstructorFromPrimitiveFloats() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        assertEquals(0, Float.compare(m.value(0, 0), 0));
        assertEquals(0, Float.compare(m.value(1, 0), 1));
        assertEquals(0, Float.compare(m.value(2, 0), 2));
        assertEquals(0, Float.compare(m.value(3, 0), 3));

        assertEquals(0, Float.compare(m.value(0, 1), 4));
        assertEquals(0, Float.compare(m.value(1, 1), 5));
        assertEquals(0, Float.compare(m.value(2, 1), 6));
        assertEquals(0, Float.compare(m.value(3, 1), 7));

        assertEquals(0, Float.compare(m.value(0, 2), 8));
        assertEquals(0, Float.compare(m.value(1, 2), 9));
        assertEquals(0, Float.compare(m.value(2, 2), 10));
        assertEquals(0, Float.compare(m.value(3, 2), 11));

        assertEquals(0, Float.compare(m.value(0, 3), 12));
        assertEquals(0, Float.compare(m.value(1, 3), 13));
        assertEquals(0, Float.compare(m.value(2, 3), 14));
        assertEquals(0, Float.compare(m.value(3, 3), 15));
    }

    @Test
    public void testConstructorFromArray1D() {
        // @formatter:off
        Matrix4 m = new Matrix4f(new float[] {
             0,  1,  2,  3,
             4,  5,  6,  7,
             8,  9, 10, 11,
            12, 13, 14, 15
        });
        // @formatter:on
        assertEquals(0, Float.compare(m.value(0, 0), 0));
        assertEquals(0, Float.compare(m.value(1, 0), 1));
        assertEquals(0, Float.compare(m.value(2, 0), 2));
        assertEquals(0, Float.compare(m.value(3, 0), 3));

        assertEquals(0, Float.compare(m.value(0, 1), 4));
        assertEquals(0, Float.compare(m.value(1, 1), 5));
        assertEquals(0, Float.compare(m.value(2, 1), 6));
        assertEquals(0, Float.compare(m.value(3, 1), 7));

        assertEquals(0, Float.compare(m.value(0, 2), 8));
        assertEquals(0, Float.compare(m.value(1, 2), 9));
        assertEquals(0, Float.compare(m.value(2, 2), 10));
        assertEquals(0, Float.compare(m.value(3, 2), 11));

        assertEquals(0, Float.compare(m.value(0, 3), 12));
        assertEquals(0, Float.compare(m.value(1, 3), 13));
        assertEquals(0, Float.compare(m.value(2, 3), 14));
        assertEquals(0, Float.compare(m.value(3, 3), 15));
    }

    @Test
    public void testConstructorFromArray2D() {
        // @formatter:off
        Matrix4 m = new Matrix4f(new float[][] {
            {  0,  1,  2,  3 },
            {  4,  5,  6,  7 },
            {  8,  9, 10, 11 },
            { 12, 13, 14, 15 }
        });
        // @formatter:on
        assertEquals(0, Float.compare(m.value(0, 0), 0));
        assertEquals(0, Float.compare(m.value(1, 0), 1));
        assertEquals(0, Float.compare(m.value(2, 0), 2));
        assertEquals(0, Float.compare(m.value(3, 0), 3));

        assertEquals(0, Float.compare(m.value(0, 1), 4));
        assertEquals(0, Float.compare(m.value(1, 1), 5));
        assertEquals(0, Float.compare(m.value(2, 1), 6));
        assertEquals(0, Float.compare(m.value(3, 1), 7));

        assertEquals(0, Float.compare(m.value(0, 2), 8));
        assertEquals(0, Float.compare(m.value(1, 2), 9));
        assertEquals(0, Float.compare(m.value(2, 2), 10));
        assertEquals(0, Float.compare(m.value(3, 2), 11));

        assertEquals(0, Float.compare(m.value(0, 3), 12));
        assertEquals(0, Float.compare(m.value(1, 3), 13));
        assertEquals(0, Float.compare(m.value(2, 3), 14));
        assertEquals(0, Float.compare(m.value(3, 3), 15));
    }

    @Test
    public void testConstructorFromColumnVector3() {
        Vector3 c0 = new Vector3f(1f, 2f, 3f);
        Vector3 c1 = new Vector3f(4f, 5f, 6);
        Vector3 c2 = new Vector3f(7f, 8f, 9f);
        Vector3 c3 = new Vector3f(10f, 11f, 12f);
        Matrix4 m = new Matrix4f(c0, c1, c2, c3);

        assertEquals(0, Float.compare(m.value(0, 0), c0.x()));
        assertEquals(0, Float.compare(m.value(1, 0), c0.y()));
        assertEquals(0, Float.compare(m.value(2, 0), c0.z()));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), c1.x()));
        assertEquals(0, Float.compare(m.value(1, 1), c1.y()));
        assertEquals(0, Float.compare(m.value(2, 1), c1.z()));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), c2.x()));
        assertEquals(0, Float.compare(m.value(1, 2), c2.y()));
        assertEquals(0, Float.compare(m.value(2, 2), c2.z()));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), c3.x()));
        assertEquals(0, Float.compare(m.value(1, 3), c3.y()));
        assertEquals(0, Float.compare(m.value(2, 3), c3.z()));
        assertEquals(0, Float.compare(m.value(3, 3), 1f));
    }

    @Test
    public void testConstructorFromVector3AndPoint3() {
        Vector3 v0 = new Vector3f(1f, 2f, 3f);
        Vector3 v1 = new Vector3f(4f, 5f, 6);
        Vector3 v2 = new Vector3f(7f, 8f, 9f);
        Point3 p = new Point3f(10f, 11f, 12f);
        Matrix4 m = new Matrix4f(v0, v1, v2, p);

        assertEquals(0, Float.compare(m.value(0, 0), v0.x()));
        assertEquals(0, Float.compare(m.value(1, 0), v0.y()));
        assertEquals(0, Float.compare(m.value(2, 0), v0.z()));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), v1.x()));
        assertEquals(0, Float.compare(m.value(1, 1), v1.y()));
        assertEquals(0, Float.compare(m.value(2, 1), v1.z()));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), v2.x()));
        assertEquals(0, Float.compare(m.value(1, 2), v2.y()));
        assertEquals(0, Float.compare(m.value(2, 2), v2.z()));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), p.x()));
        assertEquals(0, Float.compare(m.value(1, 3), p.y()));
        assertEquals(0, Float.compare(m.value(2, 3), p.z()));
        assertEquals(0, Float.compare(m.value(3, 3), 1f));
    }

    @Test
    public void testConstructorFromColumnVector4() {
        Vector4 c0 = new Vector4f(1f, 2f, 3f, 4f);
        Vector4 c1 = new Vector4f(5f, 6f, 7f, 8f);
        Vector4 c2 = new Vector4f(9f, 10f, 11f, 12f);
        Vector4 c3 = new Vector4f(13f, 14f, 15f, 16f);
        Matrix4 m = new Matrix4f(c0, c1, c2, c3);

        assertEquals(0, Float.compare(m.value(0, 0), c0.x()));
        assertEquals(0, Float.compare(m.value(1, 0), c0.y()));
        assertEquals(0, Float.compare(m.value(2, 0), c0.z()));
        assertEquals(0, Float.compare(m.value(3, 0), c0.w()));

        assertEquals(0, Float.compare(m.value(0, 1), c1.x()));
        assertEquals(0, Float.compare(m.value(1, 1), c1.y()));
        assertEquals(0, Float.compare(m.value(2, 1), c1.z()));
        assertEquals(0, Float.compare(m.value(3, 1), c1.w()));

        assertEquals(0, Float.compare(m.value(0, 2), c2.x()));
        assertEquals(0, Float.compare(m.value(1, 2), c2.y()));
        assertEquals(0, Float.compare(m.value(2, 2), c2.z()));
        assertEquals(0, Float.compare(m.value(3, 2), c2.w()));

        assertEquals(0, Float.compare(m.value(0, 3), c3.x()));
        assertEquals(0, Float.compare(m.value(1, 3), c3.y()));
        assertEquals(0, Float.compare(m.value(2, 3), c3.z()));
        assertEquals(0, Float.compare(m.value(3, 3), c3.w()));
    }

    @Test
    public void testConstructorFromMatrix3() {
        // @formatter:off
        Matrix3 m3x3 = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        );
        // @formatter:on
        Matrix4 m4x4 = new Matrix4f(m3x3);

        assertEquals(0, Float.compare(m4x4.value(0, 0), m3x3.value(0, 0)));
        assertEquals(0, Float.compare(m4x4.value(1, 0), m3x3.value(1, 0)));
        assertEquals(0, Float.compare(m4x4.value(2, 0), m3x3.value(2, 0)));
        assertEquals(0, Float.compare(m4x4.value(3, 0), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 1), m3x3.value(0, 1)));
        assertEquals(0, Float.compare(m4x4.value(1, 1), m3x3.value(1, 1)));
        assertEquals(0, Float.compare(m4x4.value(2, 1), m3x3.value(2, 1)));
        assertEquals(0, Float.compare(m4x4.value(3, 1), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 2), m3x3.value(0, 2)));
        assertEquals(0, Float.compare(m4x4.value(1, 2), m3x3.value(1, 2)));
        assertEquals(0, Float.compare(m4x4.value(2, 2), m3x3.value(2, 2)));
        assertEquals(0, Float.compare(m4x4.value(3, 2), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 3), 0f));
        assertEquals(0, Float.compare(m4x4.value(1, 3), 0f));
        assertEquals(0, Float.compare(m4x4.value(2, 3), 0f));
        assertEquals(0, Float.compare(m4x4.value(3, 3), 1f));
    }

    @Test
    public void testConstructorFromMatrix3AndVector3() {
        Vector3 pos = Vector3f.zero();
        // @formatter:off
        Matrix3 m3x3 = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        );
        // @formatter:on
        Matrix4 m4x4 = new Matrix4f(m3x3, pos);

        assertEquals(0, Float.compare(m4x4.value(0, 0), m3x3.value(0, 0)));
        assertEquals(0, Float.compare(m4x4.value(1, 0), m3x3.value(1, 0)));
        assertEquals(0, Float.compare(m4x4.value(2, 0), m3x3.value(2, 0)));
        assertEquals(0, Float.compare(m4x4.value(3, 0), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 1), m3x3.value(0, 1)));
        assertEquals(0, Float.compare(m4x4.value(1, 1), m3x3.value(1, 1)));
        assertEquals(0, Float.compare(m4x4.value(2, 1), m3x3.value(2, 1)));
        assertEquals(0, Float.compare(m4x4.value(3, 1), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 2), m3x3.value(0, 2)));
        assertEquals(0, Float.compare(m4x4.value(1, 2), m3x3.value(1, 2)));
        assertEquals(0, Float.compare(m4x4.value(2, 2), m3x3.value(2, 2)));
        assertEquals(0, Float.compare(m4x4.value(3, 2), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 3), pos.x()));
        assertEquals(0, Float.compare(m4x4.value(1, 3), pos.y()));
        assertEquals(0, Float.compare(m4x4.value(2, 3), pos.z()));
        assertEquals(0, Float.compare(m4x4.value(3, 3), 1f));
    }

    @Test
    public void testConstructorFromMatrix3AndPoint3() {
        Point3 pos = Point3f.origin();
        // @formatter:off
        Matrix3 m3x3 = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        );
        // @formatter:on
        Matrix4 m4x4 = new Matrix4f(m3x3, pos);

        assertEquals(0, Float.compare(m4x4.value(0, 0), m3x3.value(0, 0)));
        assertEquals(0, Float.compare(m4x4.value(1, 0), m3x3.value(1, 0)));
        assertEquals(0, Float.compare(m4x4.value(2, 0), m3x3.value(2, 0)));
        assertEquals(0, Float.compare(m4x4.value(3, 0), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 1), m3x3.value(0, 1)));
        assertEquals(0, Float.compare(m4x4.value(1, 1), m3x3.value(1, 1)));
        assertEquals(0, Float.compare(m4x4.value(2, 1), m3x3.value(2, 1)));
        assertEquals(0, Float.compare(m4x4.value(3, 1), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 2), m3x3.value(0, 2)));
        assertEquals(0, Float.compare(m4x4.value(1, 2), m3x3.value(1, 2)));
        assertEquals(0, Float.compare(m4x4.value(2, 2), m3x3.value(2, 2)));
        assertEquals(0, Float.compare(m4x4.value(3, 2), 0f));

        assertEquals(0, Float.compare(m4x4.value(0, 3), pos.x()));
        assertEquals(0, Float.compare(m4x4.value(1, 3), pos.y()));
        assertEquals(0, Float.compare(m4x4.value(2, 3), pos.z()));
        assertEquals(0, Float.compare(m4x4.value(3, 3), 1f));
    }

    @Test
    public void testFactoryIdentityMatrix() {
        Matrix4 m = Matrix4f.identity();

        assertEquals(0, Float.compare(m.value(0, 0), 1f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 1f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 1f));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), 0f));
        assertEquals(0, Float.compare(m.value(1, 3), 0f));
        assertEquals(0, Float.compare(m.value(2, 3), 0f));
        assertEquals(0, Float.compare(m.value(3, 3), 1f));
    }

    @Test
    public void testFactoryZeroMatrix() {
        Matrix4 m = Matrix4f.zero();

        assertEquals(0, Float.compare(m.value(0, 0), 0f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 0f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 0f));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), 0f));
        assertEquals(0, Float.compare(m.value(1, 3), 0f));
        assertEquals(0, Float.compare(m.value(2, 3), 0f));
        assertEquals(0, Float.compare(m.value(3, 3), 0f));
    }

    @Test
    public void testFactoryPerspectiveMatrix() {
        final float near = 5f;
        final float far = 15f;
        final float aspect = 1.33333f;
        final Angle fovy = new Radianf((float) Math.PI / 3f); // 60 degs

        final float q = (float) (1.0 / Math.tan(Math.toRadians(0.5f * fovy.valueDegrees())));
        final float A = q / aspect;
        final float B = (near + far) / (near - far);
        final float C = (2.0f * near * far) / (near - far);

        // @formatter:off
        Matrix4 pm1 = new Matrix4f(
            A , 0f,  0f, 0f,
            0f, q ,  0f, 0f,
            0f, 0f,  B , C,
            0f, 0f, -1f, 0f
        );
        // @formatter:on
        Matrix4 pm2 = Matrix4f.perspective(fovy, aspect, near, far);

        assertEquals(pm1.value(0, 0), pm2.value(0, 0), TestUtil.TOLERANCE);
        assertEquals(pm1.value(1, 0), pm2.value(1, 0), TestUtil.TOLERANCE);
        assertEquals(pm1.value(2, 0), pm2.value(2, 0), TestUtil.TOLERANCE);
        assertEquals(pm1.value(3, 0), pm2.value(3, 0), TestUtil.TOLERANCE);

        assertEquals(pm1.value(0, 1), pm2.value(0, 1), TestUtil.TOLERANCE);
        assertEquals(pm1.value(1, 1), pm2.value(1, 1), TestUtil.TOLERANCE);
        assertEquals(pm1.value(2, 1), pm2.value(2, 1), TestUtil.TOLERANCE);
        assertEquals(pm1.value(3, 1), pm2.value(3, 1), TestUtil.TOLERANCE);

        assertEquals(pm1.value(0, 2), pm2.value(0, 2), TestUtil.TOLERANCE);
        assertEquals(pm1.value(1, 2), pm2.value(1, 2), TestUtil.TOLERANCE);
        assertEquals(pm1.value(2, 2), pm2.value(2, 2), TestUtil.TOLERANCE);
        assertEquals(pm1.value(3, 2), pm2.value(3, 2), TestUtil.TOLERANCE);

        assertEquals(pm1.value(0, 3), pm2.value(0, 3), TestUtil.TOLERANCE);
        assertEquals(pm1.value(1, 3), pm2.value(1, 3), TestUtil.TOLERANCE);
        assertEquals(pm1.value(2, 3), pm2.value(2, 3), TestUtil.TOLERANCE);
        assertEquals(pm1.value(3, 3), pm2.value(3, 3), TestUtil.TOLERANCE);
    }

    @Test(
        expectedExceptions = { IllegalArgumentException.class },
        expectedExceptionsMessageRegExp = "Vertical field of view must be > 0")
    public void testFactoryPerspectiveMatrixRejectsInvalidViewAngle() {
        Matrix4f.perspective(new Radianf(0f), 1.3333f, 5f, 10f);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class }, expectedExceptionsMessageRegExp = "Near must be > 0")
    public void testFactoryPerspectiveMatrixRejectsInvalidNearValueFloat() {
        Matrix4f.perspective(new Degreef(60f), 1.3333f, 0f, 10f);
    }

    @Test(expectedExceptions = { IllegalArgumentException.class }, expectedExceptionsMessageRegExp = "Far must be > 0")
    public void testFactoryPerspectiveMatrixRejectsInvalidFarValueFloat() {
        Matrix4f.perspective(new Degreef(60f), 1.3333f, 0.1f, 0f);
    }

    @Test(
        expectedExceptions = { IllegalArgumentException.class },
        expectedExceptionsMessageRegExp = "Near must be < Far")
    public void testFactoryPerspectiveMatrixRejectsInvalidFarNearerThanNearValueFloat() {
        Matrix4f.perspective(new Degreef(60f), 1.3333f, 2f, 1f);
    }

    @Test(
        expectedExceptions = { IllegalArgumentException.class },
        expectedExceptionsMessageRegExp = "Aspect ratio must be > 0")
    public void testFactoryPerspectiveMatrixRejectsInvalidAspectRatioValueFloat() {
        Matrix4f.perspective(new Degreef(60f), 0f, 0.1f, 10f);
    }

    @Test
    public void testFactoryViewMatrixFromVector4() {
        Vector4 u = Vector4f.unitX();
        Vector4 v = Vector4f.unitY();
        Vector4 n = Vector4f.unitZ().mult(-1f);
        Vector4 p = new Vector4f(1, 2, 0);

        // @formatter:off
        Matrix4 rm = new Matrix4f(
            u.x(), u.y(), u.z(), 0f,
            v.x(), v.y(), v.z(), 0f,
            n.x(), n.y(), n.z(), 0f,
             0f  ,  0f  ,  0f  , 1f
        );
        Matrix4 pm = new Matrix4f(
            1f, 0f, 0f, -p.x(),
            0f, 1f, 0f, -p.y(),
            0f, 0f, 1f, -p.z(),
            0f, 0f, 0f, 1f
        );
        // @formatter:on

        Matrix4 vm1 = multiply(rm, pm);
        Matrix4 vm2 = Matrix4f.view(u, v, n, p);

        assertEquals(0, Float.compare(vm1.value(0, 0), vm2.value(0, 0)));
        assertEquals(0, Float.compare(vm1.value(1, 0), vm2.value(1, 0)));
        assertEquals(0, Float.compare(vm1.value(2, 0), vm2.value(2, 0)));
        assertEquals(0, Float.compare(vm1.value(3, 0), vm2.value(3, 0)));

        assertEquals(0, Float.compare(vm1.value(0, 1), vm2.value(0, 1)));
        assertEquals(0, Float.compare(vm1.value(1, 1), vm2.value(1, 1)));
        assertEquals(0, Float.compare(vm1.value(2, 1), vm2.value(2, 1)));
        assertEquals(0, Float.compare(vm1.value(3, 1), vm2.value(3, 1)));

        assertEquals(0, Float.compare(vm1.value(0, 2), vm2.value(0, 2)));
        assertEquals(0, Float.compare(vm1.value(1, 2), vm2.value(1, 2)));
        assertEquals(0, Float.compare(vm1.value(2, 2), vm2.value(2, 2)));
        assertEquals(0, Float.compare(vm1.value(3, 2), vm2.value(3, 2)));

        assertEquals(0, Float.compare(vm1.value(0, 3), vm2.value(0, 3)));
        assertEquals(0, Float.compare(vm1.value(1, 3), vm2.value(1, 3)));
        assertEquals(0, Float.compare(vm1.value(2, 3), vm2.value(2, 3)));
        assertEquals(0, Float.compare(vm1.value(3, 3), vm2.value(3, 3)));
    }

    @Test
    public void testFactoryViewMatrixFromVector3() {
        Vector3 u = Vector3f.unitX();
        Vector3 v = Vector3f.unitY();
        Vector3 n = Vector3f.unitZ().mult(-1f);
        Point3 p = new Point3f(1, 2, 0);

        // @formatter:off
        Matrix4 r = new Matrix4f(
            u.x(), u.y(), u.z(), 0f,
            v.x(), v.y(), v.z(), 0f,
            n.x(), n.y(), n.z(), 0f,
             0f  ,  0f  ,  0f  , 1f
        );
        Matrix4 t = new Matrix4f(
            1f, 0f, 0f, -p.x(),
            0f, 1f, 0f, -p.y(),
            0f, 0f, 1f, -p.z(),
            0f, 0f, 0f, 1f
        );
        // @formatter:on

        Matrix4 vm1 = multiply(r, t);
        Matrix4 vm2 = Matrix4f.view(u, v, n, p);

        assertEquals(0, Float.compare(vm1.value(0, 0), vm2.value(0, 0)));
        assertEquals(0, Float.compare(vm1.value(1, 0), vm2.value(1, 0)));
        assertEquals(0, Float.compare(vm1.value(2, 0), vm2.value(2, 0)));
        assertEquals(0, Float.compare(vm1.value(3, 0), vm2.value(3, 0)));

        assertEquals(0, Float.compare(vm1.value(0, 1), vm2.value(0, 1)));
        assertEquals(0, Float.compare(vm1.value(1, 1), vm2.value(1, 1)));
        assertEquals(0, Float.compare(vm1.value(2, 1), vm2.value(2, 1)));
        assertEquals(0, Float.compare(vm1.value(3, 1), vm2.value(3, 1)));

        assertEquals(0, Float.compare(vm1.value(0, 2), vm2.value(0, 2)));
        assertEquals(0, Float.compare(vm1.value(1, 2), vm2.value(1, 2)));
        assertEquals(0, Float.compare(vm1.value(2, 2), vm2.value(2, 2)));
        assertEquals(0, Float.compare(vm1.value(3, 2), vm2.value(3, 2)));

        assertEquals(0, Float.compare(vm1.value(0, 3), vm2.value(0, 3)));
        assertEquals(0, Float.compare(vm1.value(1, 3), vm2.value(1, 3)));
        assertEquals(0, Float.compare(vm1.value(2, 3), vm2.value(2, 3)));
        assertEquals(0, Float.compare(vm1.value(3, 3), vm2.value(3, 3)));
    }

    @Test
    public void testFactoryLookAtMatrixFromVector4() {
        Vector3 cameraLoc = new Vector3f(0f, 0f, 1f);
        Vector3 targetLoc = new Vector3f(0f, 0f, -2f);
        Vector3 up = new Vector3f(0f, 1f, 0f);

        Vector3 f = cameraLoc.sub(targetLoc).normalize();
        Vector3 s = up.cross(f).normalize();
        Vector3 u = f.cross(s);

        // @formatter:off
        Matrix4 lm1 = Matrix4f.inverse(
            s.x(), s.y(), s.z(), -s.dot(cameraLoc),
            u.x(), u.y(), u.z(), -u.dot(cameraLoc),
            f.x(), f.y(), f.z(), -f.dot(cameraLoc),
             0f  ,  0f  ,  0f  ,   1f
        );
        Matrix4 lm2 = Matrix4f.lookAt(
            new Vector4f(cameraLoc, 1f),
            new Vector4f(targetLoc, 1f),
            new Vector4f(up)
        );
        // @formatter:on

        assertEquals(0, Float.compare(lm1.value(0, 0), lm2.value(0, 0)));
        assertEquals(0, Float.compare(lm1.value(1, 0), lm2.value(1, 0)));
        assertEquals(0, Float.compare(lm1.value(2, 0), lm2.value(2, 0)));
        assertEquals(0, Float.compare(lm1.value(3, 0), lm2.value(3, 0)));

        assertEquals(0, Float.compare(lm1.value(0, 1), lm2.value(0, 1)));
        assertEquals(0, Float.compare(lm1.value(1, 1), lm2.value(1, 1)));
        assertEquals(0, Float.compare(lm1.value(2, 1), lm2.value(2, 1)));
        assertEquals(0, Float.compare(lm1.value(3, 1), lm2.value(3, 1)));

        assertEquals(0, Float.compare(lm1.value(0, 2), lm2.value(0, 2)));
        assertEquals(0, Float.compare(lm1.value(1, 2), lm2.value(1, 2)));
        assertEquals(0, Float.compare(lm1.value(2, 2), lm2.value(2, 2)));
        assertEquals(0, Float.compare(lm1.value(3, 2), lm2.value(3, 2)));

        assertEquals(0, Float.compare(lm1.value(0, 3), lm2.value(0, 3)));
        assertEquals(0, Float.compare(lm1.value(1, 3), lm2.value(1, 3)));
        assertEquals(0, Float.compare(lm1.value(2, 3), lm2.value(2, 3)));
        assertEquals(0, Float.compare(lm1.value(3, 3), lm2.value(3, 3)));
    }

    @Test
    public void testFactoryLookAtMatrixFromVector3() {
        Vector3 cameraLoc = new Vector3f(0f, 0f, 1f);
        Vector3 targetLoc = new Vector3f(0f, 0f, -2f);
        Vector3 up = new Vector3f(0f, 1f, 0f);

        Vector3 f = cameraLoc.sub(targetLoc).normalize();
        Vector3 s = up.cross(f).normalize();
        Vector3 u = f.cross(s);

        // @formatter:off
        Matrix4 lm1 = Matrix4f.inverse(
            s.x(), s.y(), s.z(), -s.dot(cameraLoc),
            u.x(), u.y(), u.z(), -u.dot(cameraLoc),
            f.x(), f.y(), f.z(), -f.dot(cameraLoc),
             0f  ,  0f  ,  0f  ,   1f
        );
        // @formatter:on
        Matrix4 lm2 = Matrix4f.lookAt(new Point3f(cameraLoc), new Point3f(targetLoc), up);

        assertEquals(0, Float.compare(lm1.value(0, 0), lm2.value(0, 0)));
        assertEquals(0, Float.compare(lm1.value(1, 0), lm2.value(1, 0)));
        assertEquals(0, Float.compare(lm1.value(2, 0), lm2.value(2, 0)));
        assertEquals(0, Float.compare(lm1.value(3, 0), lm2.value(3, 0)));

        assertEquals(0, Float.compare(lm1.value(0, 1), lm2.value(0, 1)));
        assertEquals(0, Float.compare(lm1.value(1, 1), lm2.value(1, 1)));
        assertEquals(0, Float.compare(lm1.value(2, 1), lm2.value(2, 1)));
        assertEquals(0, Float.compare(lm1.value(3, 1), lm2.value(3, 1)));

        assertEquals(0, Float.compare(lm1.value(0, 2), lm2.value(0, 2)));
        assertEquals(0, Float.compare(lm1.value(1, 2), lm2.value(1, 2)));
        assertEquals(0, Float.compare(lm1.value(2, 2), lm2.value(2, 2)));
        assertEquals(0, Float.compare(lm1.value(3, 2), lm2.value(3, 2)));

        assertEquals(0, Float.compare(lm1.value(0, 3), lm2.value(0, 3)));
        assertEquals(0, Float.compare(lm1.value(1, 3), lm2.value(1, 3)));
        assertEquals(0, Float.compare(lm1.value(2, 3), lm2.value(2, 3)));
        assertEquals(0, Float.compare(lm1.value(3, 3), lm2.value(3, 3)));
    }

    @Test
    public void testFactoryLookAtMatrixFromVector3AndQuaternion() {
        Point3 position = new Point3f(1, 0, 0);
        Quaternion orientation = Quaternionf.identity();

        Matrix3 rmT = new Matrix3f(orientation).transpose();
        Point3 xlate = rmT.mult(position).mult(-1f);

        Vector3 c0 = rmT.column(0);
        Vector3 c1 = rmT.column(1);
        Vector3 c2 = rmT.column(2);

        Matrix4 lm1 = new Matrix4f(c0, c1, c2, xlate);
        Matrix4 lm2 = Matrix4f.lookAt(orientation, position);

        assertEquals(0, Float.compare(lm1.value(0, 0), lm2.value(0, 0)));
        assertEquals(0, Float.compare(lm1.value(1, 0), lm2.value(1, 0)));
        assertEquals(0, Float.compare(lm1.value(2, 0), lm2.value(2, 0)));
        assertEquals(0, Float.compare(lm1.value(3, 0), lm2.value(3, 0)));

        assertEquals(0, Float.compare(lm1.value(0, 1), lm2.value(0, 1)));
        assertEquals(0, Float.compare(lm1.value(1, 1), lm2.value(1, 1)));
        assertEquals(0, Float.compare(lm1.value(2, 1), lm2.value(2, 1)));
        assertEquals(0, Float.compare(lm1.value(3, 1), lm2.value(3, 1)));

        assertEquals(0, Float.compare(lm1.value(0, 2), lm2.value(0, 2)));
        assertEquals(0, Float.compare(lm1.value(1, 2), lm2.value(1, 2)));
        assertEquals(0, Float.compare(lm1.value(2, 2), lm2.value(2, 2)));
        assertEquals(0, Float.compare(lm1.value(3, 2), lm2.value(3, 2)));

        assertEquals(0, Float.compare(lm1.value(0, 3), lm2.value(0, 3)));
        assertEquals(0, Float.compare(lm1.value(1, 3), lm2.value(1, 3)));
        assertEquals(0, Float.compare(lm1.value(2, 3), lm2.value(2, 3)));
        assertEquals(0, Float.compare(lm1.value(3, 3), lm2.value(3, 3)));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsNegativeRowIndex() {
        Matrix4f.identity().value(-1, 0);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsLargeRowIndex() {
        Matrix4f.identity().value(5, 0);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsNegativeColumnIndex() {
        Matrix4f.identity().value(0, -1);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsLargeColumnIndex() {
        Matrix4f.identity().value(0, 7);
    }

    @Test
    public void testCanRetrieveRow() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
             1,  2,  3,  4,
             5,  6,  7,  8,
             9, 10, 11, 12,
            13, 14, 15, 16
        );
        // @formatter:on
        Vector4 rv0 = m.row(0);
        Vector4 rv1 = m.row(1);
        Vector4 rv2 = m.row(2);
        Vector4 rv3 = m.row(3);

        assertEquals(0, Float.compare(rv0.x(), 1f));
        assertEquals(0, Float.compare(rv0.y(), 2f));
        assertEquals(0, Float.compare(rv0.z(), 3f));
        assertEquals(0, Float.compare(rv0.w(), 4f));

        assertEquals(0, Float.compare(rv1.x(), 5f));
        assertEquals(0, Float.compare(rv1.y(), 6f));
        assertEquals(0, Float.compare(rv1.z(), 7f));
        assertEquals(0, Float.compare(rv1.w(), 8f));

        assertEquals(0, Float.compare(rv2.x(), 9f));
        assertEquals(0, Float.compare(rv2.y(), 10f));
        assertEquals(0, Float.compare(rv2.z(), 11f));
        assertEquals(0, Float.compare(rv2.w(), 12f));

        assertEquals(0, Float.compare(rv3.x(), 13f));
        assertEquals(0, Float.compare(rv3.y(), 14f));
        assertEquals(0, Float.compare(rv3.z(), 15f));
        assertEquals(0, Float.compare(rv3.w(), 16f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testRowRejectsLargeIndex() {
        Matrix4f.identity().row(4);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testRowRejectsSmallIndex() {
        Matrix4f.identity().row(-1);
    }

    @Test
    public void testCanRetrieveColumn() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Vector4 cv0 = m.column(0);
        Vector4 cv1 = m.column(1);
        Vector4 cv2 = m.column(2);
        Vector4 cv3 = m.column(3);

        assertEquals(0, Float.compare(cv0.x(), 0f));
        assertEquals(0, Float.compare(cv0.y(), 1f));
        assertEquals(0, Float.compare(cv0.z(), 2f));
        assertEquals(0, Float.compare(cv0.w(), 3f));

        assertEquals(0, Float.compare(cv1.x(), 4f));
        assertEquals(0, Float.compare(cv1.y(), 5f));
        assertEquals(0, Float.compare(cv1.z(), 6f));
        assertEquals(0, Float.compare(cv1.w(), 7f));

        assertEquals(0, Float.compare(cv2.x(), 8f));
        assertEquals(0, Float.compare(cv2.y(), 9f));
        assertEquals(0, Float.compare(cv2.z(), 10f));
        assertEquals(0, Float.compare(cv2.w(), 11f));

        assertEquals(0, Float.compare(cv3.x(), 12f));
        assertEquals(0, Float.compare(cv3.y(), 13f));
        assertEquals(0, Float.compare(cv3.z(), 14f));
        assertEquals(0, Float.compare(cv3.w(), 15f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testColumnRejectsLargeIndex() {
        Matrix4f.identity().column(4);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testColumnRejectsSmallIndex() {
        Matrix4f.identity().column(-1);
    }

    @Test
    public void testMatrixAddition() {
        // @formatter:off
        Matrix4 m1 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 m2 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 rm = m1.add(m2);

        assertEquals(0, Float.compare(rm.value(0, 0), m1.value(0, 0) + m2.value(0, 0)));
        assertEquals(0, Float.compare(rm.value(1, 0), m1.value(1, 0) + m2.value(1, 0)));
        assertEquals(0, Float.compare(rm.value(2, 0), m1.value(2, 0) + m2.value(2, 0)));
        assertEquals(0, Float.compare(rm.value(3, 0), m1.value(3, 0) + m2.value(3, 0)));

        assertEquals(0, Float.compare(rm.value(0, 1), m1.value(0, 1) + m2.value(0, 1)));
        assertEquals(0, Float.compare(rm.value(1, 1), m1.value(1, 1) + m2.value(1, 1)));
        assertEquals(0, Float.compare(rm.value(2, 1), m1.value(2, 1) + m2.value(2, 1)));
        assertEquals(0, Float.compare(rm.value(3, 1), m1.value(3, 1) + m2.value(3, 1)));

        assertEquals(0, Float.compare(rm.value(0, 2), m1.value(0, 2) + m2.value(0, 2)));
        assertEquals(0, Float.compare(rm.value(1, 2), m1.value(1, 2) + m2.value(1, 2)));
        assertEquals(0, Float.compare(rm.value(2, 2), m1.value(2, 2) + m2.value(2, 2)));
        assertEquals(0, Float.compare(rm.value(3, 2), m1.value(3, 2) + m2.value(3, 2)));

        assertEquals(0, Float.compare(rm.value(0, 3), m1.value(0, 3) + m2.value(0, 3)));
        assertEquals(0, Float.compare(rm.value(1, 3), m1.value(1, 3) + m2.value(1, 3)));
        assertEquals(0, Float.compare(rm.value(2, 3), m1.value(2, 3) + m2.value(2, 3)));
        assertEquals(0, Float.compare(rm.value(3, 3), m1.value(3, 3) + m2.value(3, 3)));
    }

    @Test
    public void testMatrixSubtraction() {
        // @formatter:off
        Matrix4 m1 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 m2 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 rm = m1.sub(m2);

        assertEquals(0, Float.compare(rm.value(0, 0), m1.value(0, 0) - m2.value(0, 0)));
        assertEquals(0, Float.compare(rm.value(1, 0), m1.value(1, 0) - m2.value(1, 0)));
        assertEquals(0, Float.compare(rm.value(2, 0), m1.value(2, 0) - m2.value(2, 0)));
        assertEquals(0, Float.compare(rm.value(3, 0), m1.value(3, 0) - m2.value(3, 0)));

        assertEquals(0, Float.compare(rm.value(0, 1), m1.value(0, 1) - m2.value(0, 1)));
        assertEquals(0, Float.compare(rm.value(1, 1), m1.value(1, 1) - m2.value(1, 1)));
        assertEquals(0, Float.compare(rm.value(2, 1), m1.value(2, 1) - m2.value(2, 1)));
        assertEquals(0, Float.compare(rm.value(3, 1), m1.value(3, 1) - m2.value(3, 1)));

        assertEquals(0, Float.compare(rm.value(0, 2), m1.value(0, 2) - m2.value(0, 2)));
        assertEquals(0, Float.compare(rm.value(1, 2), m1.value(1, 2) - m2.value(1, 2)));
        assertEquals(0, Float.compare(rm.value(2, 2), m1.value(2, 2) - m2.value(2, 2)));
        assertEquals(0, Float.compare(rm.value(3, 2), m1.value(3, 2) - m2.value(3, 2)));

        assertEquals(0, Float.compare(rm.value(0, 3), m1.value(0, 3) - m2.value(0, 3)));
        assertEquals(0, Float.compare(rm.value(1, 3), m1.value(1, 3) - m2.value(1, 3)));
        assertEquals(0, Float.compare(rm.value(2, 3), m1.value(2, 3) - m2.value(2, 3)));
        assertEquals(0, Float.compare(rm.value(3, 3), m1.value(3, 3) - m2.value(3, 3)));
    }

    @Test
    public void testMatrixScalarMultiplication() {
        final float scalar = 5f;
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 rm = m.mult(scalar);

        assertEquals(0, Float.compare(rm.value(0, 0), m.value(0, 0) * scalar));
        assertEquals(0, Float.compare(rm.value(1, 0), m.value(1, 0) * scalar));
        assertEquals(0, Float.compare(rm.value(2, 0), m.value(2, 0) * scalar));
        assertEquals(0, Float.compare(rm.value(3, 0), m.value(3, 0) * scalar));

        assertEquals(0, Float.compare(rm.value(0, 1), m.value(0, 1) * scalar));
        assertEquals(0, Float.compare(rm.value(1, 1), m.value(1, 1) * scalar));
        assertEquals(0, Float.compare(rm.value(2, 1), m.value(2, 1) * scalar));
        assertEquals(0, Float.compare(rm.value(3, 1), m.value(3, 1) * scalar));

        assertEquals(0, Float.compare(rm.value(0, 2), m.value(0, 2) * scalar));
        assertEquals(0, Float.compare(rm.value(1, 2), m.value(1, 2) * scalar));
        assertEquals(0, Float.compare(rm.value(2, 2), m.value(2, 2) * scalar));
        assertEquals(0, Float.compare(rm.value(3, 2), m.value(3, 2) * scalar));

        assertEquals(0, Float.compare(rm.value(0, 3), m.value(0, 3) * scalar));
        assertEquals(0, Float.compare(rm.value(1, 3), m.value(1, 3) * scalar));
        assertEquals(0, Float.compare(rm.value(2, 3), m.value(2, 3) * scalar));
        assertEquals(0, Float.compare(rm.value(3, 3), m.value(3, 3) * scalar));
    }

    @Test
    public void testMatrixVectorMultiplication() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            2f, 2f, 2f, 2f,
            3f, 3f, 3f, 3f,
            4f, 4f, 4f, 4f,
            1f, 1f, 1f, 1f
        );
        // @formatter:on
        Vector4 v = new Vector4f(2f, 3f, 4f, 1f);
        Vector4 rv = m.mult(v);

        assertEquals(0, Float.compare(rv.x(), 20f));
        assertEquals(0, Float.compare(rv.y(), 30f));
        assertEquals(0, Float.compare(rv.z(), 40f));
        assertEquals(0, Float.compare(rv.w(), 10f));
    }

    @Test
    public void testMatrixMatrixMultiplication() {
        // @formatter:off
        Matrix4 m1 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 m2 = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 rm1 = m1.mult(m2);
        Matrix4 rm2 = multiply(m1, m2);

        assertEquals(0, Float.compare(rm1.value(0, 0), rm2.value(0, 0)));
        assertEquals(0, Float.compare(rm1.value(1, 0), rm2.value(1, 0)));
        assertEquals(0, Float.compare(rm1.value(2, 0), rm2.value(2, 0)));
        assertEquals(0, Float.compare(rm1.value(3, 0), rm2.value(3, 0)));

        assertEquals(0, Float.compare(rm1.value(0, 1), rm2.value(0, 1)));
        assertEquals(0, Float.compare(rm1.value(1, 1), rm2.value(1, 1)));
        assertEquals(0, Float.compare(rm1.value(2, 1), rm2.value(2, 1)));
        assertEquals(0, Float.compare(rm1.value(3, 1), rm2.value(3, 1)));

        assertEquals(0, Float.compare(rm1.value(0, 2), rm2.value(0, 2)));
        assertEquals(0, Float.compare(rm1.value(1, 2), rm2.value(1, 2)));
        assertEquals(0, Float.compare(rm1.value(2, 2), rm2.value(2, 2)));
        assertEquals(0, Float.compare(rm1.value(3, 2), rm2.value(3, 2)));

        assertEquals(0, Float.compare(rm1.value(0, 3), rm2.value(0, 3)));
        assertEquals(0, Float.compare(rm1.value(1, 3), rm2.value(1, 3)));
        assertEquals(0, Float.compare(rm1.value(2, 3), rm2.value(2, 3)));
        assertEquals(0, Float.compare(rm1.value(3, 3), rm2.value(3, 3)));
    }

    @Test
    public void testMatrixIdentityMultiplicationYieldsOriginalMatrix() {
        // @formatter:off
        Matrix4 cm = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 im = Matrix4f.identity();
        Matrix4 pm = cm.mult(im);

        assertEquals(0, Float.compare(cm.value(0, 0), pm.value(0, 0)));
        assertEquals(0, Float.compare(cm.value(1, 0), pm.value(1, 0)));
        assertEquals(0, Float.compare(cm.value(2, 0), pm.value(2, 0)));
        assertEquals(0, Float.compare(cm.value(3, 0), pm.value(3, 0)));

        assertEquals(0, Float.compare(cm.value(0, 1), pm.value(0, 1)));
        assertEquals(0, Float.compare(cm.value(1, 1), pm.value(1, 1)));
        assertEquals(0, Float.compare(cm.value(2, 1), pm.value(2, 1)));
        assertEquals(0, Float.compare(cm.value(3, 1), pm.value(3, 1)));

        assertEquals(0, Float.compare(cm.value(0, 2), pm.value(0, 2)));
        assertEquals(0, Float.compare(cm.value(1, 2), pm.value(1, 2)));
        assertEquals(0, Float.compare(cm.value(2, 2), pm.value(2, 2)));
        assertEquals(0, Float.compare(cm.value(3, 2), pm.value(3, 2)));

        assertEquals(0, Float.compare(cm.value(0, 3), pm.value(0, 3)));
        assertEquals(0, Float.compare(cm.value(1, 3), pm.value(1, 3)));
        assertEquals(0, Float.compare(cm.value(2, 3), pm.value(2, 3)));
        assertEquals(0, Float.compare(cm.value(3, 3), pm.value(3, 3)));
    }

    @Test
    public void testMatrixInverseMultiplicationYieldsIdentity() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            1, 0, 0, 1,
            0, 2, 1, 2,
            2, 1, 0, 1,
            2, 0, 1, 4
        );
        Matrix4 i = Matrix4f.inverse(
            1, 0, 0, 1,
            0, 2, 1, 2,
            2, 1, 0, 1,
            2, 0, 1, 4
        );
        // @formatter:on
        Matrix4 rm = m.mult(i);
        Matrix4 im = Matrix4f.identity();

        assertEquals(0, Float.compare(im.value(0, 0), rm.value(0, 0)));
        assertEquals(0, Float.compare(im.value(1, 0), rm.value(1, 0)));
        assertEquals(0, Float.compare(im.value(2, 0), rm.value(2, 0)));
        assertEquals(0, Float.compare(im.value(3, 0), rm.value(3, 0)));

        assertEquals(0, Float.compare(im.value(0, 1), rm.value(0, 1)));
        assertEquals(0, Float.compare(im.value(1, 1), rm.value(1, 1)));
        assertEquals(0, Float.compare(im.value(2, 1), rm.value(2, 1)));
        assertEquals(0, Float.compare(im.value(3, 1), rm.value(3, 1)));

        assertEquals(0, Float.compare(im.value(0, 2), rm.value(0, 2)));
        assertEquals(0, Float.compare(im.value(1, 2), rm.value(1, 2)));
        assertEquals(0, Float.compare(im.value(2, 2), rm.value(2, 2)));
        assertEquals(0, Float.compare(im.value(3, 2), rm.value(3, 2)));

        assertEquals(0, Float.compare(im.value(0, 3), rm.value(0, 3)));
        assertEquals(0, Float.compare(im.value(1, 3), rm.value(1, 3)));
        assertEquals(0, Float.compare(im.value(2, 3), rm.value(2, 3)));
        assertEquals(0, Float.compare(im.value(3, 3), rm.value(3, 3)));
    }

    @Test
    public void testScalingByPrimitive() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix4 im = Matrix4f.identity();
        Matrix4 sm = im.scale(sx, sy, sz);

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));
        assertEquals(0, Float.compare(sm.value(3, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));
        assertEquals(0, Float.compare(sm.value(3, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
        assertEquals(0, Float.compare(sm.value(3, 2), 0f));

        assertEquals(0, Float.compare(sm.value(0, 3), 0f));
        assertEquals(0, Float.compare(sm.value(1, 3), 0f));
        assertEquals(0, Float.compare(sm.value(2, 3), 0f));
        assertEquals(0, Float.compare(sm.value(3, 3), 1f));
    }

    @Test
    public void testScalingByVector3() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix4 im = Matrix4f.identity();
        Matrix4 sm = im.scale(new Vector3f(sx, sy, sz));

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));
        assertEquals(0, Float.compare(sm.value(3, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));
        assertEquals(0, Float.compare(sm.value(3, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
        assertEquals(0, Float.compare(sm.value(3, 2), 0f));

        assertEquals(0, Float.compare(sm.value(0, 3), 0f));
        assertEquals(0, Float.compare(sm.value(1, 3), 0f));
        assertEquals(0, Float.compare(sm.value(2, 3), 0f));
        assertEquals(0, Float.compare(sm.value(3, 3), 1f));
    }

    @Test
    public void testScalingByVector4() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix4 im = Matrix4f.identity();
        Matrix4 sm = im.scale(new Vector4f(sx, sy, sz));

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));
        assertEquals(0, Float.compare(sm.value(3, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));
        assertEquals(0, Float.compare(sm.value(3, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
        assertEquals(0, Float.compare(sm.value(3, 2), 0f));

        assertEquals(0, Float.compare(sm.value(0, 3), 0f));
        assertEquals(0, Float.compare(sm.value(1, 3), 0f));
        assertEquals(0, Float.compare(sm.value(2, 3), 0f));
        assertEquals(0, Float.compare(sm.value(3, 3), 1f));
    }

    @Test
    public void testFactoryRotationFromAngleAxis() {
        final Angle angle = new Degreef(45f);
        final Vector4 axis = new Vector4f(1f, 0f, 0f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 r1 = Matrix4f.rotation(angle, axis);
        Matrix4 r2 = rotate(m, angle, axis);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(3, 0), r2.value(3, 0)));

        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(3, 1), r2.value(3, 1)));

        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
        assertEquals(0, Float.compare(r1.value(3, 2), r2.value(3, 2)));

        assertEquals(0, Float.compare(r1.value(0, 3), r2.value(0, 3)));
        assertEquals(0, Float.compare(r1.value(1, 3), r2.value(1, 3)));
        assertEquals(0, Float.compare(r1.value(2, 3), r2.value(2, 3)));
        assertEquals(0, Float.compare(r1.value(3, 3), r2.value(3, 3)));
    }

    @Test
    public void testMatrixDeterminantMatches() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            1, 0, 0, 1,
            0, 2, 1, 2,
            2, 1, 0, 1,
            2, 0, 1, 4
        );
        // @formatter:on
        assertEquals(0, Float.compare(m.determinant(), 2.0f));
    }

    @Test
    public void testInverseMatches() {
        // @formatter:off
        // matrix data from: https://www.youtube.com/watch?v=nNOz6Ez8Fn4
        Matrix4 m = new Matrix4f(
            1, 0, 0, 1,
            0, 2, 1, 2,
            2, 1, 0, 1,
            2, 0, 1, 4
        );
        Matrix4 i1 = m.inverse();
        Matrix4 i2 = new Matrix4f(
            -2f, -0.5f,  1f,  0.5f,
             1f,  0.5f,  0f, -0.5f,
            -8f,  -1f ,  2f,   2f ,
             3f,  0.5f, -1f, -0.5f
        );
        // @formatter:on
        Matrix4 i3 = inverse(m);

        // test implementation against hard-coded inverse data
        assertEquals(0, Float.compare(i1.value(0, 0), i2.value(0, 0)));
        assertEquals(0, Float.compare(i1.value(1, 0), i2.value(1, 0)));
        assertEquals(0, Float.compare(i1.value(2, 0), i2.value(2, 0)));
        assertEquals(0, Float.compare(i1.value(3, 0), i2.value(3, 0)));

        assertEquals(0, Float.compare(i1.value(0, 1), i2.value(0, 1)));
        assertEquals(0, Float.compare(i1.value(1, 1), i2.value(1, 1)));
        assertEquals(0, Float.compare(i1.value(2, 1), i2.value(2, 1)));
        assertEquals(0, Float.compare(i1.value(3, 1), i2.value(3, 1)));

        assertEquals(0, Float.compare(i1.value(0, 2), i2.value(0, 2)));
        assertEquals(0, Float.compare(i1.value(1, 2), i2.value(1, 2)));
        assertEquals(0, Float.compare(i1.value(2, 2), i2.value(2, 2)));
        assertEquals(0, Float.compare(i1.value(3, 2), i2.value(3, 2)));

        assertEquals(0, Float.compare(i1.value(0, 3), i2.value(0, 3)));
        assertEquals(0, Float.compare(i1.value(1, 3), i2.value(1, 3)));
        assertEquals(0, Float.compare(i1.value(2, 3), i2.value(2, 3)));
        assertEquals(0, Float.compare(i1.value(3, 3), i2.value(3, 3)));

        // test 2nd test implementation against hard-coded inverse data
        assertEquals(0, Float.compare(i2.value(0, 0), i3.value(0, 0)));
        assertEquals(0, Float.compare(i2.value(1, 0), i3.value(1, 0)));
        assertEquals(0, Float.compare(i2.value(2, 0), i3.value(2, 0)));
        assertEquals(0, Float.compare(i2.value(3, 0), i3.value(3, 0)));

        assertEquals(0, Float.compare(i2.value(0, 1), i3.value(0, 1)));
        assertEquals(0, Float.compare(i2.value(1, 1), i3.value(1, 1)));
        assertEquals(0, Float.compare(i2.value(2, 1), i3.value(2, 1)));
        assertEquals(0, Float.compare(i2.value(3, 1), i3.value(3, 1)));

        assertEquals(0, Float.compare(i2.value(0, 2), i3.value(0, 2)));
        assertEquals(0, Float.compare(i2.value(1, 2), i3.value(1, 2)));
        assertEquals(0, Float.compare(i2.value(2, 2), i3.value(2, 2)));
        assertEquals(0, Float.compare(i2.value(3, 2), i3.value(3, 2)));

        assertEquals(0, Float.compare(i2.value(0, 3), i3.value(0, 3)));
        assertEquals(0, Float.compare(i2.value(1, 3), i3.value(1, 3)));
        assertEquals(0, Float.compare(i2.value(2, 3), i3.value(2, 3)));
        assertEquals(0, Float.compare(i2.value(3, 3), i3.value(3, 3)));
    }

    @Test(expectedExceptions = { SingularMatrixException.class })
    public void testSingularMatrixInverseThrowsException() {
        Matrix4f.zero().inverse();
    }

    @Test(expectedExceptions = { SingularMatrixException.class })
    public void testFactorySingularMatrixInverseThrowsException() {
        // @formatter:off
        Matrix4f.inverse(
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 0
        );
        // @formatter:on
    }

    @Test
    public void testFactoryInverseFromVectors() {
        // @formatter:off
        Matrix4 mat4 = new Matrix4f(
            1, 0, 0, 1,
            0, 2, 1, 2,
            2, 1, 0, 1,
            2, 0, 1, 4
        );
        // @formatter:on
        Vector4 c0 = mat4.column(0);
        Vector4 c1 = mat4.column(1);
        Vector4 c2 = mat4.column(2);
        Vector4 c3 = mat4.column(3);

        Matrix4 inv1 = Matrix4f.inverse(c0, c1, c2, c3);
        Matrix4 inv2 = inverse(mat4);

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));
        assertEquals(0, Float.compare(inv1.value(3, 0), inv2.value(3, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));
        assertEquals(0, Float.compare(inv1.value(3, 1), inv2.value(3, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
        assertEquals(0, Float.compare(inv1.value(3, 2), inv2.value(3, 2)));

        assertEquals(0, Float.compare(inv1.value(0, 3), inv2.value(0, 3)));
        assertEquals(0, Float.compare(inv1.value(1, 3), inv2.value(1, 3)));
        assertEquals(0, Float.compare(inv1.value(2, 3), inv2.value(2, 3)));
        assertEquals(0, Float.compare(inv1.value(3, 3), inv2.value(3, 3)));
    }

    @Test
    public void testFactoryInverseFromArray1D() {
        // @formatter:off
        Matrix4 m = Matrix4f.inverse(new float[] {
            1, 0, 2, 2,
            0, 2, 1, 0,
            0, 1, 0, 1,
            1, 2, 1, 4
        });
        // @formatter:on
        Vector4 c0 = m.column(0);
        Vector4 c1 = m.column(1);
        Vector4 c2 = m.column(2);
        Vector4 c3 = m.column(3);

        Matrix4 inv1 = Matrix4f.inverse(c0, c1, c2, c3);
        Matrix4 inv2 = inverse(m);

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));
        assertEquals(0, Float.compare(inv1.value(3, 0), inv2.value(3, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));
        assertEquals(0, Float.compare(inv1.value(3, 1), inv2.value(3, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
        assertEquals(0, Float.compare(inv1.value(3, 2), inv2.value(3, 2)));

        assertEquals(0, Float.compare(inv1.value(0, 3), inv2.value(0, 3)));
        assertEquals(0, Float.compare(inv1.value(1, 3), inv2.value(1, 3)));
        assertEquals(0, Float.compare(inv1.value(2, 3), inv2.value(2, 3)));
        assertEquals(0, Float.compare(inv1.value(3, 3), inv2.value(3, 3)));
    }

    @Test
    public void testFactoryInverseFromArray2D() {
        // @formatter:off
        Matrix4 m = Matrix4f.inverse(new float[][] {
            { 1, 0, 2, 2 },
            { 0, 2, 1, 0 },
            { 0, 1, 0, 1 },
            { 1, 2, 1, 4 }
        });
        // @formatter:on
        Vector4 c0 = m.column(0);
        Vector4 c1 = m.column(1);
        Vector4 c2 = m.column(2);
        Vector4 c3 = m.column(3);

        Matrix4 inv1 = Matrix4f.inverse(c0, c1, c2, c3);
        Matrix4 inv2 = inverse(m);

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));
        assertEquals(0, Float.compare(inv1.value(3, 0), inv2.value(3, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));
        assertEquals(0, Float.compare(inv1.value(3, 1), inv2.value(3, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
        assertEquals(0, Float.compare(inv1.value(3, 2), inv2.value(3, 2)));

        assertEquals(0, Float.compare(inv1.value(0, 3), inv2.value(0, 3)));
        assertEquals(0, Float.compare(inv1.value(1, 3), inv2.value(1, 3)));
        assertEquals(0, Float.compare(inv1.value(2, 3), inv2.value(2, 3)));
        assertEquals(0, Float.compare(inv1.value(3, 3), inv2.value(3, 3)));
    }

    @Test
    public void testMatrixTransposeMatches() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Matrix4 t = m.transpose();

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(0, 3), t.value(3, 0)));

        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(1, 3), t.value(3, 1)));

        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
        assertEquals(0, Float.compare(m.value(2, 3), t.value(3, 2)));

        assertEquals(0, Float.compare(m.value(3, 0), t.value(0, 3)));
        assertEquals(0, Float.compare(m.value(3, 1), t.value(1, 3)));
        assertEquals(0, Float.compare(m.value(3, 2), t.value(2, 3)));
        assertEquals(0, Float.compare(m.value(3, 3), t.value(3, 3)));
    }

    @Test
    public void testFactoryTransposeFromPrimitives() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 t = Matrix4f.transpose(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(0, 3), t.value(3, 0)));

        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(1, 3), t.value(3, 1)));

        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
        assertEquals(0, Float.compare(m.value(2, 3), t.value(3, 2)));

        assertEquals(0, Float.compare(m.value(3, 0), t.value(0, 3)));
        assertEquals(0, Float.compare(m.value(3, 1), t.value(1, 3)));
        assertEquals(0, Float.compare(m.value(3, 2), t.value(2, 3)));
        assertEquals(0, Float.compare(m.value(3, 3), t.value(3, 3)));
    }

    @Test
    public void testFactoryTransposeFromVectors() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on
        Vector4 c0 = m.column(0);
        Vector4 c1 = m.column(1);
        Vector4 c2 = m.column(2);
        Vector4 c3 = m.column(3);
        Matrix4 tm = Matrix4f.transpose(c0, c1, c2, c3);

        // getValue(row, column)
        assertEquals(0, Float.compare(tm.value(0, 0), c0.x()));
        assertEquals(0, Float.compare(tm.value(0, 1), c0.y()));
        assertEquals(0, Float.compare(tm.value(0, 2), c0.z()));
        assertEquals(0, Float.compare(tm.value(0, 3), c0.w()));

        assertEquals(0, Float.compare(tm.value(1, 0), c1.x()));
        assertEquals(0, Float.compare(tm.value(1, 1), c1.y()));
        assertEquals(0, Float.compare(tm.value(1, 2), c1.z()));
        assertEquals(0, Float.compare(tm.value(1, 3), c1.w()));

        assertEquals(0, Float.compare(tm.value(2, 0), c2.x()));
        assertEquals(0, Float.compare(tm.value(2, 1), c2.y()));
        assertEquals(0, Float.compare(tm.value(2, 2), c2.z()));
        assertEquals(0, Float.compare(tm.value(2, 3), c2.w()));

        assertEquals(0, Float.compare(tm.value(3, 0), c3.x()));
        assertEquals(0, Float.compare(tm.value(3, 1), c3.y()));
        assertEquals(0, Float.compare(tm.value(3, 2), c3.z()));
        assertEquals(0, Float.compare(tm.value(3, 3), c3.w()));
    }

    @Test
    public void testFactoryTransposeFromArray1D() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 t = Matrix4f.transpose(new float[] {
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        });
        // @formatter:on

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(0, 3), t.value(3, 0)));

        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(1, 3), t.value(3, 1)));

        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
        assertEquals(0, Float.compare(m.value(2, 3), t.value(3, 2)));

        assertEquals(0, Float.compare(m.value(3, 0), t.value(0, 3)));
        assertEquals(0, Float.compare(m.value(3, 1), t.value(1, 3)));
        assertEquals(0, Float.compare(m.value(3, 2), t.value(2, 3)));
        assertEquals(0, Float.compare(m.value(3, 3), t.value(3, 3)));
    }

    @Test
    public void testFactoryTransposeFromArray2D() {
        // @formatter:off
        Matrix4 m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        Matrix4 t = Matrix4f.transpose(new float[][] {
            {  0,  1,  2,  3 },
            {  4,  5,  6,  7 },
            {  8,  9, 10, 11 },
            { 12, 13, 14, 15 }
        });
        // @formatter:on

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(0, 3), t.value(3, 0)));

        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(1, 3), t.value(3, 1)));

        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
        assertEquals(0, Float.compare(m.value(2, 3), t.value(3, 2)));

        assertEquals(0, Float.compare(m.value(3, 0), t.value(0, 3)));
        assertEquals(0, Float.compare(m.value(3, 1), t.value(1, 3)));
        assertEquals(0, Float.compare(m.value(3, 2), t.value(2, 3)));
        assertEquals(0, Float.compare(m.value(3, 3), t.value(3, 3)));
    }

    @Test
    public void testFactoryTranslationFromPrimitives() {
        final float tx = 2f;
        final float ty = 4f;
        final float tz = 6f;
        Matrix4 tm = Matrix4f.translation(tx, ty, tz);

        assertEquals(0, Float.compare(tm.value(0, 0), 1f));
        assertEquals(0, Float.compare(tm.value(1, 0), 0f));
        assertEquals(0, Float.compare(tm.value(2, 0), 0f));
        assertEquals(0, Float.compare(tm.value(3, 0), 0f));

        assertEquals(0, Float.compare(tm.value(0, 1), 0f));
        assertEquals(0, Float.compare(tm.value(1, 1), 1f));
        assertEquals(0, Float.compare(tm.value(2, 1), 0f));
        assertEquals(0, Float.compare(tm.value(3, 1), 0f));

        assertEquals(0, Float.compare(tm.value(0, 2), 0f));
        assertEquals(0, Float.compare(tm.value(1, 2), 0f));
        assertEquals(0, Float.compare(tm.value(2, 2), 1f));
        assertEquals(0, Float.compare(tm.value(3, 2), 0f));

        assertEquals(0, Float.compare(tm.value(0, 3), tx));
        assertEquals(0, Float.compare(tm.value(1, 3), ty));
        assertEquals(0, Float.compare(tm.value(2, 3), tz));
        assertEquals(0, Float.compare(tm.value(3, 3), 1f));
    }

    @Test
    public void testFactoryTranslationFromVector3() {
        Vector3 v = new Vector3f(2f, 4f, 6f);
        Matrix4 m = Matrix4f.translation(v);

        assertEquals(0, Float.compare(m.value(0, 0), 1f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 1f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 1f));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), v.x()));
        assertEquals(0, Float.compare(m.value(1, 3), v.y()));
        assertEquals(0, Float.compare(m.value(2, 3), v.z()));
        assertEquals(0, Float.compare(m.value(3, 3), 1f));
    }

    @Test
    public void testFactoryTranslationFromPoint3() {
        Point3 p = new Point3f(2f, 4f, 6f);
        Matrix4 m = Matrix4f.translation(p);

        assertEquals(0, Float.compare(m.value(0, 0), 1f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));
        assertEquals(0, Float.compare(m.value(3, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 1f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));
        assertEquals(0, Float.compare(m.value(3, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 1f));
        assertEquals(0, Float.compare(m.value(3, 2), 0f));

        assertEquals(0, Float.compare(m.value(0, 3), p.x()));
        assertEquals(0, Float.compare(m.value(1, 3), p.y()));
        assertEquals(0, Float.compare(m.value(2, 3), p.z()));
        assertEquals(0, Float.compare(m.value(3, 3), 1f));
    }

    @Test
    public void testFactoryTranslationFromVector4() {
        Vector4 tv = new Vector4f(2f, 4f, 6f);
        Matrix4 tm = Matrix4f.translation(tv);

        assertEquals(0, Float.compare(tm.value(0, 0), 1f));
        assertEquals(0, Float.compare(tm.value(1, 0), 0f));
        assertEquals(0, Float.compare(tm.value(2, 0), 0f));
        assertEquals(0, Float.compare(tm.value(3, 0), 0f));

        assertEquals(0, Float.compare(tm.value(0, 1), 0f));
        assertEquals(0, Float.compare(tm.value(1, 1), 1f));
        assertEquals(0, Float.compare(tm.value(2, 1), 0f));
        assertEquals(0, Float.compare(tm.value(3, 1), 0f));

        assertEquals(0, Float.compare(tm.value(0, 2), 0f));
        assertEquals(0, Float.compare(tm.value(1, 2), 0f));
        assertEquals(0, Float.compare(tm.value(2, 2), 1f));
        assertEquals(0, Float.compare(tm.value(3, 2), 0f));

        assertEquals(0, Float.compare(tm.value(0, 3), tv.x()));
        assertEquals(0, Float.compare(tm.value(1, 3), tv.y()));
        assertEquals(0, Float.compare(tm.value(2, 3), tv.z()));
        assertEquals(0, Float.compare(tm.value(3, 3), 1f));
    }

    @Test
    public void testTranslationPrimitive() {
        final float tx = 2f;
        final float ty = 4f;
        final float tz = 6f;
        Matrix4 m = Matrix4f.identity();
        Matrix4 t1 = m.translate(tx, ty, tz);

        assertEquals(0, Float.compare(t1.value(0, 0), 1f));
        assertEquals(0, Float.compare(t1.value(0, 1), 0f));
        assertEquals(0, Float.compare(t1.value(0, 2), 0f));
        assertEquals(0, Float.compare(t1.value(0, 3), tx));

        assertEquals(0, Float.compare(t1.value(1, 0), 0f));
        assertEquals(0, Float.compare(t1.value(1, 1), 1f));
        assertEquals(0, Float.compare(t1.value(1, 2), 0f));
        assertEquals(0, Float.compare(t1.value(1, 3), ty));

        assertEquals(0, Float.compare(t1.value(2, 0), 0f));
        assertEquals(0, Float.compare(t1.value(2, 1), 0f));
        assertEquals(0, Float.compare(t1.value(2, 2), 1f));
        assertEquals(0, Float.compare(t1.value(2, 3), tz));

        assertEquals(0, Float.compare(t1.value(3, 0), 0f));
        assertEquals(0, Float.compare(t1.value(3, 1), 0f));
        assertEquals(0, Float.compare(t1.value(3, 2), 0f));
        assertEquals(0, Float.compare(t1.value(3, 3), 1f));
    }

    @Test
    public void testTranslationVector3() {
        Vector3 tv = new Vector3f(2f, 4f, 6f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 t1 = m.translate(tv);

        assertEquals(0, Float.compare(t1.value(0, 0), 1f));
        assertEquals(0, Float.compare(t1.value(0, 1), 0f));
        assertEquals(0, Float.compare(t1.value(0, 2), 0f));
        assertEquals(0, Float.compare(t1.value(0, 3), tv.x()));

        assertEquals(0, Float.compare(t1.value(1, 0), 0f));
        assertEquals(0, Float.compare(t1.value(1, 1), 1f));
        assertEquals(0, Float.compare(t1.value(1, 2), 0f));
        assertEquals(0, Float.compare(t1.value(1, 3), tv.y()));

        assertEquals(0, Float.compare(t1.value(2, 0), 0f));
        assertEquals(0, Float.compare(t1.value(2, 1), 0f));
        assertEquals(0, Float.compare(t1.value(2, 2), 1f));
        assertEquals(0, Float.compare(t1.value(2, 3), tv.z()));

        assertEquals(0, Float.compare(t1.value(3, 0), 0f));
        assertEquals(0, Float.compare(t1.value(3, 1), 0f));
        assertEquals(0, Float.compare(t1.value(3, 2), 0f));
        assertEquals(0, Float.compare(t1.value(3, 3), 1f));
    }

    @Test
    public void testTranslationPoint3() {
        Point3 p = new Point3f(2f, 4f, 6f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 t = m.translate(p);

        assertEquals(0, Float.compare(t.value(0, 0), 1f));
        assertEquals(0, Float.compare(t.value(0, 1), 0f));
        assertEquals(0, Float.compare(t.value(0, 2), 0f));
        assertEquals(0, Float.compare(t.value(0, 3), p.x()));

        assertEquals(0, Float.compare(t.value(1, 0), 0f));
        assertEquals(0, Float.compare(t.value(1, 1), 1f));
        assertEquals(0, Float.compare(t.value(1, 2), 0f));
        assertEquals(0, Float.compare(t.value(1, 3), p.y()));

        assertEquals(0, Float.compare(t.value(2, 0), 0f));
        assertEquals(0, Float.compare(t.value(2, 1), 0f));
        assertEquals(0, Float.compare(t.value(2, 2), 1f));
        assertEquals(0, Float.compare(t.value(2, 3), p.z()));

        assertEquals(0, Float.compare(t.value(3, 0), 0f));
        assertEquals(0, Float.compare(t.value(3, 1), 0f));
        assertEquals(0, Float.compare(t.value(3, 2), 0f));
        assertEquals(0, Float.compare(t.value(3, 3), 1f));
    }

    @Test
    public void testTranslationVector4() {
        Vector4 tv = new Vector4f(2f, 4f, 6f, 8f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 t1 = m.translate(tv);

        assertEquals(0, Float.compare(t1.value(0, 0), 1f));
        assertEquals(0, Float.compare(t1.value(0, 1), 0f));
        assertEquals(0, Float.compare(t1.value(0, 2), 0f));
        assertEquals(0, Float.compare(t1.value(0, 3), tv.x()));

        assertEquals(0, Float.compare(t1.value(1, 0), 0f));
        assertEquals(0, Float.compare(t1.value(1, 1), 1f));
        assertEquals(0, Float.compare(t1.value(1, 2), 0f));
        assertEquals(0, Float.compare(t1.value(1, 3), tv.y()));

        assertEquals(0, Float.compare(t1.value(2, 0), 0f));
        assertEquals(0, Float.compare(t1.value(2, 1), 0f));
        assertEquals(0, Float.compare(t1.value(2, 2), 1f));
        assertEquals(0, Float.compare(t1.value(2, 3), tv.z()));

        assertEquals(0, Float.compare(t1.value(3, 0), 0f));
        assertEquals(0, Float.compare(t1.value(3, 1), 0f));
        assertEquals(0, Float.compare(t1.value(3, 2), 0f));
        assertEquals(0, Float.compare(t1.value(3, 3), 1f));
    }

    @Test
    public void testFactoryScalingFromPrimitiveFloats() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix4 tm = Matrix4f.scaling(sx, sy, sz);

        assertEquals(0, Float.compare(tm.value(0, 0), sx));
        assertEquals(0, Float.compare(tm.value(1, 0), 0f));
        assertEquals(0, Float.compare(tm.value(2, 0), 0f));
        assertEquals(0, Float.compare(tm.value(3, 0), 0f));

        assertEquals(0, Float.compare(tm.value(0, 1), 0f));
        assertEquals(0, Float.compare(tm.value(1, 1), sy));
        assertEquals(0, Float.compare(tm.value(2, 1), 0f));
        assertEquals(0, Float.compare(tm.value(3, 1), 0f));

        assertEquals(0, Float.compare(tm.value(0, 2), 0f));
        assertEquals(0, Float.compare(tm.value(1, 2), 0f));
        assertEquals(0, Float.compare(tm.value(2, 2), sz));
        assertEquals(0, Float.compare(tm.value(3, 2), 0f));

        assertEquals(0, Float.compare(tm.value(0, 3), 0f));
        assertEquals(0, Float.compare(tm.value(1, 3), 0f));
        assertEquals(0, Float.compare(tm.value(2, 3), 0f));
        assertEquals(0, Float.compare(tm.value(3, 3), 1f));
    }

    @Test
    public void testFactoryScalingFromVector3() {
        Vector3 sv = new Vector3f(2f, 4f, 6f);
        Matrix4 sm = Matrix4f.scaling(sv);

        assertEquals(0, Float.compare(sm.value(0, 0), sv.x()));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));
        assertEquals(0, Float.compare(sm.value(3, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sv.y()));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));
        assertEquals(0, Float.compare(sm.value(3, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sv.z()));
        assertEquals(0, Float.compare(sm.value(3, 2), 0f));

        assertEquals(0, Float.compare(sm.value(0, 3), 0f));
        assertEquals(0, Float.compare(sm.value(1, 3), 0f));
        assertEquals(0, Float.compare(sm.value(2, 3), 0f));
        assertEquals(0, Float.compare(sm.value(3, 3), 1f));
    }

    @Test
    public void testFactoryScalingFromVector4() {
        Vector4 sv = new Vector4f(2f, 4f, 6f);
        Matrix4 sm = Matrix4f.scaling(sv);

        assertEquals(0, Float.compare(sm.value(0, 0), sv.x()));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));
        assertEquals(0, Float.compare(sm.value(3, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sv.y()));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));
        assertEquals(0, Float.compare(sm.value(3, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sv.z()));
        assertEquals(0, Float.compare(sm.value(3, 2), 0f));

        assertEquals(0, Float.compare(sm.value(0, 3), 0f));
        assertEquals(0, Float.compare(sm.value(1, 3), 0f));
        assertEquals(0, Float.compare(sm.value(2, 3), 0f));
        assertEquals(0, Float.compare(sm.value(3, 3), 1f));
    }

    @Test
    public void testToMatrix3() {
        // @formatter:off
        Matrix3 m3x3 = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        );
        Matrix4 m4x4 = new Matrix4f(
            1f,  4f,  7f, -1f,
            2f,  5f,  8f, -2f,
            3f,  6f,  9f, -3f,
           -1f, -2f, -3f, -4f
        );
        // @formatter:on
        Matrix3 rm3x3 = m4x4.toMatrix3();

        assertEquals(0, Float.compare(rm3x3.value(0, 0), m3x3.value(0, 0)));
        assertEquals(0, Float.compare(rm3x3.value(1, 0), m3x3.value(1, 0)));
        assertEquals(0, Float.compare(rm3x3.value(2, 0), m3x3.value(2, 0)));

        assertEquals(0, Float.compare(rm3x3.value(0, 1), m3x3.value(0, 1)));
        assertEquals(0, Float.compare(rm3x3.value(1, 1), m3x3.value(1, 1)));
        assertEquals(0, Float.compare(rm3x3.value(2, 1), m3x3.value(2, 1)));

        assertEquals(0, Float.compare(rm3x3.value(0, 2), m3x3.value(0, 2)));
        assertEquals(0, Float.compare(rm3x3.value(1, 2), m3x3.value(1, 2)));
        assertEquals(0, Float.compare(rm3x3.value(2, 2), m3x3.value(2, 2)));
    }

    @Test
    public void testFloatArrayValues() {
        // @formatter:off
        float[] m = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        ).toFloatArray();
        // @formatter:on

        assertEquals(0, Float.compare(m[0], 0f));
        assertEquals(0, Float.compare(m[1], 1f));
        assertEquals(0, Float.compare(m[2], 2f));
        assertEquals(0, Float.compare(m[3], 3f));

        assertEquals(0, Float.compare(m[4], 4f));
        assertEquals(0, Float.compare(m[5], 5f));
        assertEquals(0, Float.compare(m[6], 6f));
        assertEquals(0, Float.compare(m[7], 7f));

        assertEquals(0, Float.compare(m[8], 8f));
        assertEquals(0, Float.compare(m[9], 9f));
        assertEquals(0, Float.compare(m[10], 10f));
        assertEquals(0, Float.compare(m[11], 11));

        assertEquals(0, Float.compare(m[12], 12f));
        assertEquals(0, Float.compare(m[13], 13f));
        assertEquals(0, Float.compare(m[14], 14));
        assertEquals(0, Float.compare(m[15], 15f));
    }

    @Test
    public void testRotationWithAngles() {
        final Angle angleX = new Degreef(45f);
        final Angle angleY = new Degreef(45f);
        final Angle angleZ = new Degreef(45f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 r1 = m.rotate(angleX, angleY, angleZ);
        Matrix4 r2 = rotate(m, angleX, angleY, angleZ);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));
        assertEquals(0, Float.compare(r1.value(0, 3), r2.value(0, 3)));

        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));
        assertEquals(0, Float.compare(r1.value(1, 3), r2.value(1, 3)));

        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
        assertEquals(0, Float.compare(r1.value(2, 3), r2.value(2, 3)));

        assertEquals(0, Float.compare(r1.value(3, 0), r2.value(3, 0)));
        assertEquals(0, Float.compare(r1.value(3, 1), r2.value(3, 1)));
        assertEquals(0, Float.compare(r1.value(3, 2), r2.value(3, 2)));
        assertEquals(0, Float.compare(r1.value(3, 3), r2.value(3, 3)));
    }

    @Test
    public void testRotationAngleAndAxisVector3() {
        final Angle angle = new Degreef(45f);
        final Vector3 axis = new Vector3f(0f, 1f, 0f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 r1 = m.rotate(angle, axis);
        Matrix4 r2 = rotate(m, angle, new Vector4f(axis));

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));
        assertEquals(0, Float.compare(r1.value(0, 3), r2.value(0, 3)));

        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));
        assertEquals(0, Float.compare(r1.value(1, 3), r2.value(1, 3)));

        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
        assertEquals(0, Float.compare(r1.value(2, 3), r2.value(2, 3)));

        assertEquals(0, Float.compare(r1.value(3, 0), r2.value(3, 0)));
        assertEquals(0, Float.compare(r1.value(3, 1), r2.value(3, 1)));
        assertEquals(0, Float.compare(r1.value(3, 2), r2.value(3, 2)));
        assertEquals(0, Float.compare(r1.value(3, 3), r2.value(3, 3)));
    }

    @Test
    public void testRotationAngleAndAxisVector4() {
        final Angle angle = new Degreef(45f);
        final Vector4 axis = new Vector4f(0f, 1f, 0f);
        Matrix4 m = Matrix4f.identity();
        Matrix4 r1 = m.rotate(angle, axis);
        Matrix4 r2 = rotate(m, angle, axis);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));
        assertEquals(0, Float.compare(r1.value(0, 3), r2.value(0, 3)));

        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));
        assertEquals(0, Float.compare(r1.value(1, 3), r2.value(1, 3)));

        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
        assertEquals(0, Float.compare(r1.value(2, 3), r2.value(2, 3)));

        assertEquals(0, Float.compare(r1.value(3, 0), r2.value(3, 0)));
        assertEquals(0, Float.compare(r1.value(3, 1), r2.value(3, 1)));
        assertEquals(0, Float.compare(r1.value(3, 2), r2.value(3, 2)));
        assertEquals(0, Float.compare(r1.value(3, 3), r2.value(3, 3)));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Matrix4 m = Matrix4f.identity();

        assertTrue(m.equals(m));
        assertFalse(m.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Matrix4 m1 = Matrix4f.identity();
        Matrix4 m2 = Matrix4f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));

        assertTrue(m1.hashCode() == m2.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Matrix4 m1 = Matrix4f.identity();
        Matrix4 m2 = Matrix4f.identity();
        Matrix4 m3 = Matrix4f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m3));
        assertTrue(m1.equals(m3));

        assertTrue(m1.hashCode() == m2.hashCode());
        assertTrue(m2.hashCode() == m3.hashCode());
        assertTrue(m1.hashCode() == m3.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
        assertFalse(m3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Matrix4 m1 = Matrix4f.identity();
        Matrix4 m2 = Matrix4f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m1.equals(m2));

        assertTrue(m1.hashCode() == m2.hashCode());
        assertTrue(m1.hashCode() == m2.hashCode());

        m1 = Matrix4f.zero();
        assertFalse(m1.equals(m2));
        assertFalse(m1.hashCode() == m2.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Matrix4f.zero().equals(null));
        assertFalse(Matrix4f.identity().equals(null));
    }

    @Test
    public void testToString() {
        // @formatter:off
        Matrix4 zm = new Matrix4f(
            0, 4,  8, 12,
            1, 5,  9, 13,
            2, 6, 10, 14,
            3, 7, 11, 15
        );
        // @formatter:on

        // TODO: Improve formatting
        String format = Matrix4f.class.getSimpleName();
        format += " = [  0.00000 |   4.00000 |   8.00000 |  12.00000]\n";
        format += "           [  1.00000 |   5.00000 |   9.00000 |  13.00000]\n";
        format += "           [  2.00000 |   6.00000 |  10.00000 |  14.00000]\n";
        format += "           [  3.00000 |   7.00000 |  11.00000 |  15.00000]";

        assertEquals(format, zm.toString());
    }

    // ========================================================================
    // Helper implementation methods
    // ========================================================================

    private static Matrix4 multiply(Matrix4 m, Matrix4 n) {
        final float m00 = m.row(0).dot(n.column(0));
        final float m01 = m.row(0).dot(n.column(1));
        final float m02 = m.row(0).dot(n.column(2));
        final float m03 = m.row(0).dot(n.column(3));

        final float m10 = m.row(1).dot(n.column(0));
        final float m11 = m.row(1).dot(n.column(1));
        final float m12 = m.row(1).dot(n.column(2));
        final float m13 = m.row(1).dot(n.column(3));

        final float m20 = m.row(2).dot(n.column(0));
        final float m21 = m.row(2).dot(n.column(1));
        final float m22 = m.row(2).dot(n.column(2));
        final float m23 = m.row(2).dot(n.column(3));

        final float m30 = m.row(3).dot(n.column(0));
        final float m31 = m.row(3).dot(n.column(1));
        final float m32 = m.row(3).dot(n.column(2));
        final float m33 = m.row(3).dot(n.column(3));

        // @formatter:off
        return new Matrix4f(
            m00, m01, m02, m03,
            m10, m11, m12, m13,
            m20, m21, m22, m23,
            m30, m31, m32, m33
        );
        // @formatter:on
    }

    private static Matrix4 rotate(Matrix4 lhs, Angle x, Angle y, Angle z) {
        float radx = x.valueRadians();
        float rady = y.valueRadians();
        float radz = z.valueRadians();
        float sinx = (float) Math.sin(radx);
        float siny = (float) Math.sin(rady);
        float sinz = (float) Math.sin(radz);
        float cosx = (float) Math.cos(radx);
        float cosy = (float) Math.cos(rady);
        float cosz = (float) Math.cos(radz);

        // @formatter:off
        Matrix4 rx = new Matrix4f(
            1f, 0f  ,  0f  , 0f,
            0f, cosx, -sinx, 0f,
            0f, sinx,  cosx, 0f,
            0f, 0f  ,  0f  , 1f
        );
        Matrix4 ry = new Matrix4f(
             cosy, 0f, siny , 0f,
              0f , 1f,  0f  , 0f,
            -siny, 0f, cosy , 0f,
              0f , 0f,  0f  , 1f
        );
        Matrix4 rz = new Matrix4f(
            cosz, -sinz, 0f, 0f,
            sinz,  cosz, 0f, 0f,
             0f ,   0f , 1f, 0f,
             0f ,   0f , 0f, 1f
        );
        // @formatter:on
        return lhs.mult(rx).mult(ry).mult(rz);
    }

    private static Matrix4 rotate(Matrix4 m, Angle angle, Vector4 axis) {
        Vector4 nv = axis.normalize();
        float cos = (float) Math.cos(angle.valueRadians());
        float sin = (float) Math.sin(angle.valueRadians());
        float t = 1.0f - cos;

        float x = nv.x();
        float y = nv.y();
        float z = nv.z();

        float xx = x * x;
        float yy = y * y;
        float zz = z * z;
        float xy = x * y;
        float xz = x * z;
        float yz = y * z;

        // @formatter:off
        return m.mult(
            new Matrix4f(
                (  t * xx + cos  ), (t * xy - z * sin), (t * xz + y * sin), 0f,
                (t * xy + z * sin), (  t * yy + cos  ), (t * yz - x * sin), 0f,
                (t * xz - y * sin), (t * yz + x * sin), (  t * zz + cos  ), 0f,
                (       0f       ), (       0f       ), (       0f       ), 1f
            )
        );
        // @formatter:on
    }

    private static Matrix4 inverse(Matrix4 m) {
        Vector3 a = m.column(0).toVector3();
        Vector3 b = m.column(1).toVector3();
        Vector3 c = m.column(2).toVector3();
        Vector3 d = m.column(3).toVector3();

        final float x = m.value(3, 0);
        final float y = m.value(3, 1);
        final float z = m.value(3, 2);
        final float w = m.value(3, 3);

        Vector3 s = a.cross(b);
        Vector3 t = c.cross(d);
        Vector3 u = a.mult(y).sub(b.mult(x));
        Vector3 v = c.mult(w).sub(d.mult(z));

        final float invDet = 1f / (s.dot(v) + t.dot(u));

        s = s.mult(invDet);
        t = t.mult(invDet);
        u = u.mult(invDet);
        v = v.mult(invDet);

        Vector3 r0 = b.cross(v).add(t.mult(y));
        Vector3 r1 = v.cross(a).sub(t.mult(x));
        Vector3 r2 = d.cross(u).add(s.mult(w));
        Vector3 r3 = u.cross(c).sub(s.mult(z));

        // @formatter:off
        return new Matrix4f(
            r0.x(), r0.y(), r0.z(), -b.dot(t),
            r1.x(), r1.y(), r1.z(),  a.dot(t),
            r2.x(), r2.y(), r2.z(), -d.dot(s),
            r3.x(), r3.y(), r3.z(),  c.dot(s)
        );
        // @formatter:on
    }
}
