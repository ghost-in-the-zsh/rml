/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static java.lang.Math.*;
import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class QuaternionfTest {

    @Test
    public void testConstructorFromAngleAxis() {
        final float w = 1f;
        final float x = 0f;
        final float y = 1f;
        final float z = 0f;

        Angle angle = new Degreef((float) toDegrees(acos(w)) * 2f);
        Vector3 axis = new Vector3f(x, y, z);
        Quaternion q = new Quaternionf(angle, axis);

        Angle halfAngle = angle.div(2f);
        final float cosOfHalfAngle = (float) cos(halfAngle.valueRadians());
        final float sinOfHalfAngle = (float) sin(halfAngle.valueRadians());

        final float rw = cosOfHalfAngle;
        final float rx = x * sinOfHalfAngle;
        final float ry = y * sinOfHalfAngle;
        final float rz = z * sinOfHalfAngle;

        assertEquals(0, Float.compare(q.w(), rw));
        assertEquals(0, Float.compare(q.x(), rx));
        assertEquals(0, Float.compare(q.y(), ry));
        assertEquals(0, Float.compare(q.z(), rz));
    }

    @Test
    public void testConstructorFromScalarAxis() {
        final float w = 1f;
        final float x = 0f;
        final float y = 1f;
        final float z = 0f;

        Vector3 axis = new Vector3f(x, y, z);
        Quaternion q = new Quaternionf(w, axis);

        assertEquals(0, Float.compare(q.w(), w));
        assertEquals(0, Float.compare(q.x(), x));
        assertEquals(0, Float.compare(q.y(), y));
        assertEquals(0, Float.compare(q.z(), z));
    }

    @Test
    public void testConstructorFromArrayAcceptsCorrectLength() {
        Quaternion q = new Quaternionf(new float[] { 1f, 2f, 3f, 4f });

        assertEquals(0, Float.compare(q.w(), 1f));
        assertEquals(0, Float.compare(q.x(), 2f));
        assertEquals(0, Float.compare(q.y(), 3f));
        assertEquals(0, Float.compare(q.z(), 4f));
    }

    @Test
    public void testConstructorFromArrayIgnoresInputLengthTooLong() {
        Quaternion q = new Quaternionf(new float[] { 1f, 2f, 3f, 4f, 5f });

        assertEquals(0, Float.compare(q.w(), 1f));
        assertEquals(0, Float.compare(q.x(), 2f));
        assertEquals(0, Float.compare(q.y(), 3f));
        assertEquals(0, Float.compare(q.z(), 4f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromArrayFailsOnInputLengthTooShort() {
        new Quaternionf(new float[] { 1f, 2f, 3f });
    }

    @Test
    public void testConstructorFromArrayClampsInvalidNegativeScalarW() {
        // the 1st value must be in the [-1, 1] range because it is NOT
        // an angle: it is a raw value used with the cosine function, whose
        // domain is in [-1, 1]. Therefore, this must clamp the value.
        Quaternion q = new Quaternionf(new float[] { -1.1f, 2f, 3f, 4f });

        assertEquals(0, Float.compare(q.w(), -1f));
        assertEquals(0, Float.compare(q.x(), 2f));
        assertEquals(0, Float.compare(q.y(), 3f));
        assertEquals(0, Float.compare(q.z(), 4f));
    }

    @Test
    public void testConstructorFromArrayClampsInvalidPositiveScalarW() {
        Quaternion q = new Quaternionf(new float[] { 1.1f, 2f, 3f, 4f });

        assertEquals(0, Float.compare(q.w(), 1f));
        assertEquals(0, Float.compare(q.x(), 2f));
        assertEquals(0, Float.compare(q.y(), 3f));
        assertEquals(0, Float.compare(q.z(), 4f));
    }

    @Test
    public void testConstructorFromMatrix() {
        // This test covers 2 cases:
        // 1. That the ctor works, and
        // 2. That an identity matrix produces an identity quaternion
        Matrix3 m = Matrix3f.identity();
        Quaternion q = new Quaternionf(m);

        assertEquals(0, Float.compare(q.w(), 1f));
        assertEquals(0, Float.compare(q.x(), 0f));
        assertEquals(0, Float.compare(q.y(), 0f));
        assertEquals(0, Float.compare(q.z(), 0f));
    }

    @Test
    public void testFactoryIdentityQuaternion() {
        Quaternion q = Quaternionf.identity();

        assertEquals(0, Float.compare(q.w(), 1f));
        assertEquals(0, Float.compare(q.x(), 0f));
        assertEquals(0, Float.compare(q.y(), 0f));
        assertEquals(0, Float.compare(q.z(), 0f));
    }

    @Test
    public void testFactoryZeroQuaternion() {
        Quaternion q = Quaternionf.zero();

        assertEquals(0, Float.compare(q.w(), 0f));
        assertEquals(0, Float.compare(q.x(), 0f));
        assertEquals(0, Float.compare(q.y(), 0f));
        assertEquals(0, Float.compare(q.z(), 0f));
    }

    @Test
    public void testFactoryNormalizedFromArray() {
        Quaternion q = Quaternionf.normalize(new float[] { 0, 1, 0, 1 });

        assertEquals(q.length(), 1f, TestUtil.TOLERANCE);
    }

    @Test
    public void testFactoryNormalizedFromPrimitiveFloats() {
        Quaternion q = Quaternionf.normalize(1, 0, 1, 0);
        assertEquals(q.length(), 1f, TestUtil.TOLERANCE);
    }

    @Test
    public void testFactoryNormalizedFromAngleAxis() {
        final float w = 1f;
        final float x = 0f;
        final float y = 1f;
        final float z = 0f;

        Angle angle = new Degreef((float) toDegrees(acos(w)) * 2f);
        Vector3 axis = new Vector3f(x, y, z);
        Quaternion q = Quaternionf.normalize(angle, axis);

        assertEquals(0, Float.compare(q.length(), 1f));
    }

    @Test
    public void testFactoryNormalizedFromScalarAxis() {
        Quaternion q = Quaternionf.normalize(1f, Vector3f.zero());
        assertEquals(0, Float.compare(q.length(), 1f));
    }

    @Test
    public void testFactoryInverseFromArray() {
        float[] values = new float[] { 1, 1, 1, 1 };
        Quaternion q = inverse(new Quaternionf(values));
        Quaternion p = Quaternionf.inverse(values);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryInverseFromPrimitives() {
        float w = -1;
        float x = 1;
        float y = 1;
        float z = 1;
        Quaternion q = inverse(new Quaternionf(w, x, y, z));
        Quaternion p = Quaternionf.inverse(w, x, y, z);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryInverseFromScalarAxis() {
        float w = 0;
        Vector3 v = new Vector3f(1, 2, 3);
        Quaternion q = inverse(new Quaternionf(w, v));
        Quaternion p = Quaternionf.inverse(w, v);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryInverseFromAngleAxis() {
        Angle t = new Degreef(90f);
        Vector3 v = new Vector3f(1, 2, 3);
        Quaternion q = inverse(new Quaternionf(t, v));
        Quaternion p = Quaternionf.inverse(t, v);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryConjugateFromArray() {
        float[] values = new float[] { 0, 1, 2, 3 };
        Quaternion p = conjugate(new Quaternionf(values));
        Quaternion q = Quaternionf.conjugate(values);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryConjugateFromPrimitives() {
        float w = 0;
        float x = 1;
        float y = 2;
        float z = 3;
        Quaternion p = conjugate(new Quaternionf(w, x, y, z));
        Quaternion q = Quaternionf.conjugate(w, x, y, z);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryConjugateFromScalarAxis() {
        float w = 0;
        float x = 1;
        float y = 2;
        float z = 3;
        Quaternion p = conjugate(new Quaternionf(w, x, y, z));
        Quaternion q = Quaternionf.conjugate(w, new Vector3f(x, y, z));

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFactoryConjugateFromAngleAxis() {
        Angle t = new Degreef(0f);
        Vector3 v = new Vector3f(1, 2, 3);
        Quaternion p = conjugate(new Quaternionf(t, v));
        Quaternion q = Quaternionf.conjugate(t, v);

        assertEquals(0, Float.compare(q.w(), p.w()));
        assertEquals(0, Float.compare(q.x(), p.x()));
        assertEquals(0, Float.compare(q.y(), p.y()));
        assertEquals(0, Float.compare(q.z(), p.z()));
    }

    @Test
    public void testFloatArrayValues() {
        final float w = 1f;
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;
        Quaternion q = new Quaternionf(w, x, y, z);
        final float[] vals = q.toFloatArray();

        assertTrue(vals.length == 4);

        assertEquals(0, Float.compare(vals[0], w));
        assertEquals(0, Float.compare(vals[1], x));
        assertEquals(0, Float.compare(vals[2], y));
        assertEquals(0, Float.compare(vals[3], z));
    }

    @Test(
        expectedExceptions = { ArithmeticException.class },
        expectedExceptionsMessageRegExp = "Cannot normalize zero-length quaternion")
    public void testNormalizedQuaternionFactoryRejectsZeroLengthQuaternion() {
        Quaternionf.normalize(0, 0, 0, 0);
    }

    @Test
    public void testAddition() {
        Quaternion q1 = new Quaternionf(0.5f, 2, 3, 4);
        Quaternion q2 = new Quaternionf(0.5f, 2, 3, 4);
        Quaternion q3 = q1.add(q2);

        assertEquals(0, Float.compare(q3.w(), q1.w() + q2.w()));
        assertEquals(0, Float.compare(q3.x(), q1.x() + q2.x()));
        assertEquals(0, Float.compare(q3.y(), q1.y() + q2.y()));
        assertEquals(0, Float.compare(q3.z(), q1.z() + q2.z()));
    }

    @Test
    public void testSubtraction() {
        Quaternion q1 = new Quaternionf(0.5f, 2, 3, 4);
        Quaternion q2 = new Quaternionf(0.25f, 1, 1, 1);
        Quaternion q3 = q1.sub(q2);

        assertEquals(0, Float.compare(q3.w(), q1.w() - q2.w()));
        assertEquals(0, Float.compare(q3.x(), q1.x() - q2.x()));
        assertEquals(0, Float.compare(q3.y(), q1.y() - q2.y()));
        assertEquals(0, Float.compare(q3.z(), q1.z() - q2.z()));
    }

    @Test
    public void testQuaternionMultiplication() {
        // q₁ = (w₁, v₁), where v₁ = [x₁, y₁, z₁]
        // q₂ = (w₂, v₂), where v₂ = [x₂, y₂, z₂]
        final float w1 = 1.0f;
        final float w2 = 0.5f;
        Vector3 v1 = Vector3f.unitX();
        Vector3 v2 = Vector3f.unitY();

        // q₃ = q₁ * q₂ = (w₁ * w₂ - (v₁ · v₂), [v₂ * w₁ + v₁ * w₂ + (v₁ × v₂)])
        // Reference: http://mathworld.wolfram.com/Quaternion.html
        float rw = w1 * w2 - v1.dot(v2);
        Vector3 rv = v2.mult(w1).add(v1.mult(w2)).add(v1.cross(v2));

        // result must be normalized
        final float len = MathUtil.sqrt(rw * rw + rv.lengthSquared());
        final float nw = rw / len;
        final float nx = rv.x() / len;
        final float ny = rv.y() / len;
        final float nz = rv.z() / len;

        Quaternion p = new Quaternionf(w1, v1);
        Quaternion q = new Quaternionf(w2, v2);
        Quaternion r = p.mult(q);

        assertEquals(nw, r.w(), TestUtil.TOLERANCE);
        assertEquals(nx, r.x(), TestUtil.TOLERANCE);
        assertEquals(ny, r.y(), TestUtil.TOLERANCE);
        assertEquals(nz, r.z(), TestUtil.TOLERANCE);
    }

    @Test
    public void testScalarMultiplication() {
        Quaternion q = new Quaternionf(0.5f, 2, 3, 4);
        final float scalar = 2f;
        Quaternion rq = q.mult(scalar);

        assertEquals(0, Float.compare(rq.w(), q.w() * scalar));
        assertEquals(0, Float.compare(rq.x(), q.x() * scalar));
        assertEquals(0, Float.compare(rq.y(), q.y() * scalar));
        assertEquals(0, Float.compare(rq.z(), q.z() * scalar));
    }

    @Test
    public void testQuaternionAngle() {
        Quaternion q = Quaternionf.identity();
        final float theta = (float) (2.0 * Math.acos(q.w()));

        assertEquals(0, Float.compare(theta, q.angle().valueRadians()));
    }

    @Test
    public void testQuaternionAxis() {
        Quaternion q = new Quaternionf(0.5f, 1f, -1f, 0f);
        final float halfTheta = (float) Math.acos(q.w());

        float sinHalfAngle = (float) Math.sin(halfTheta);
        Vector3 v1 = Vector3f.normalize(q.x() / sinHalfAngle, q.y() / sinHalfAngle, q.z() / sinHalfAngle);
        Vector3 v2 = q.axis();

        assertEquals(0, Float.compare(v1.x(), v2.x()));
        assertEquals(0, Float.compare(v1.y(), v2.y()));
        assertEquals(0, Float.compare(v1.z(), v2.z()));
    }

    @Test
    public void testQuaternionDotProduct() {
        final float w1 = 0.5f;
        final float w2 = 0.25f;
        final float x1 = 1f;
        final float y1 = 1f;
        final float z1 = 1f;
        final float x2 = 1f;
        final float y2 = 1f;
        final float z2 = 1f;
        final Vector3 v1 = new Vector3f(x1, y1, z1);
        final Vector3 v2 = new Vector3f(x2, y1, z2);

        final float result = w1 * w2 + v1.dot(v2);

        Quaternion q1 = new Quaternionf(w1, x1, y1, z1);
        Quaternion q2 = new Quaternionf(w2, x2, y2, z1);

        assertEquals(0, Float.compare(result, q1.dot(q2)));
    }

    @Test
    public void testAngleBetweenQuaternions() {
        Quaternion p = Quaternionf.identity();
        Quaternion q = new Quaternionf(0.5f, Vector3f.unitY());
        Angle a = p.angleTo(q);
        Angle b = new Radianf((float) Math.acos(p.dot(q)));

        assertEquals(0, Float.compare(a.valueRadians(), b.valueRadians()));
    }

    @Test
    public void testQuaternionConjugate() {
        Quaternion q1 = new Quaternionf(1f, 2f, 4f, 6f);
        Quaternion q2 = q1.conjugate();

        assertEquals(0, Float.compare(q2.w(), q1.w()));
        assertEquals(0, Float.compare(q2.x(), -q1.x()));
        assertEquals(0, Float.compare(q2.y(), -q1.y()));
        assertEquals(0, Float.compare(q2.z(), -q1.z()));
    }

    @Test
    public void testTwiceConjugatedQuaternionReturnsOriginal() {
        Quaternion q1 = new Quaternionf(1f, 2f, 4f, 6f);
        Quaternion q2 = q1.conjugate().conjugate();

        assertEquals(q1.w(), q2.w(), TestUtil.TOLERANCE);
        assertEquals(q1.x(), q2.x(), TestUtil.TOLERANCE);
        assertEquals(q1.y(), q2.y(), TestUtil.TOLERANCE);
        assertEquals(q1.z(), q2.z(), TestUtil.TOLERANCE);
    }

    @Test
    public void testQuaternionConjugateProductEquivalency() {
        Quaternion p = Quaternionf.normalize(1f, 1f, 2f, 3f);
        Quaternion q = Quaternionf.normalize(-1f, 2f, 4f, 6f);

        // (pq)* <=> q*p*
        // where p and q are quaternions, p* and q* are conjugates of
        // quaternions p and q respectively, (pq)* is the conjugate of the
        // product, and q*p* is the product of conjugates
        Quaternion conjugateOfProducts = p.mult(q).conjugate();
        Quaternion productOfConjugates = q.conjugate().mult(p.conjugate());

        assertEquals(conjugateOfProducts.w(), productOfConjugates.w(), TestUtil.TOLERANCE);
        assertEquals(conjugateOfProducts.x(), productOfConjugates.x(), TestUtil.TOLERANCE);
        assertEquals(conjugateOfProducts.y(), productOfConjugates.y(), TestUtil.TOLERANCE);
        assertEquals(conjugateOfProducts.z(), productOfConjugates.z(), TestUtil.TOLERANCE);
    }

    @Test
    public void testQuaternionNormalizationMatchesLength() {
        Quaternion q = new Quaternionf(1, 2, 3, 4);

        assertEquals(q.normalize().length(), 1f, TestUtil.TOLERANCE);
    }

    @Test
    public void testInverseQuaternionMatches() {
        final float w = 1;
        final float x = 2;
        final float y = 3;
        final float z = 4;
        final float l = w * w + x * x + y * y + z * z;

        // conjugate & inverse in single step
        final float il = 1f / l;
        final float iw = w * il;
        final float ix = -x * il;
        final float iy = -y * il;
        final float iz = -z * il;

        Quaternion q = new Quaternionf(w, x, y, z).inverse();
        assertEquals(0, Float.compare(q.w(), iw));
        assertEquals(0, Float.compare(q.x(), ix));
        assertEquals(0, Float.compare(q.y(), iy));
        assertEquals(0, Float.compare(q.z(), iz));
    }

    @Test
    public void testQuaternionInverseProductEquivalency() {
        Quaternion p = Quaternionf.normalize(1f, 1f, 2f, 3f);
        Quaternion q = Quaternionf.normalize(-1f, 2f, 4f, 6f);

        // (pq)⁻¹ <=> q⁻¹ p⁻¹
        // where p and q are quaternions, p⁻¹ and q⁻¹ are the inverse of
        // quaternions p and q respectively, (pq)⁻¹ is the inverse of the
        // product, and q⁻¹ p⁻¹ is the product of inverse
        Quaternion inverseOfProducts = p.mult(q).inverse();
        Quaternion productOfInverses = q.inverse().mult(p.inverse());

        assertEquals(inverseOfProducts.w(), productOfInverses.w(), TestUtil.TOLERANCE);
        assertEquals(inverseOfProducts.x(), productOfInverses.x(), TestUtil.TOLERANCE);
        assertEquals(inverseOfProducts.y(), productOfInverses.y(), TestUtil.TOLERANCE);
        assertEquals(inverseOfProducts.z(), productOfInverses.z(), TestUtil.TOLERANCE);
    }

    @Test
    public void testInverseQuaternionMultiplicationYieldsIdentity() {
        Quaternion iq = Quaternionf.identity();
        Quaternion q1 = Quaternionf.normalize(1f, 1f, -1f, 1f);
        Quaternion q2 = q1.inverse();
        Quaternion q3 = q1.mult(q2);

        assertEquals(0, Float.compare(q3.w(), iq.w()));
        assertEquals(0, Float.compare(q3.x(), iq.x()));
        assertEquals(0, Float.compare(q3.y(), iq.y()));
        assertEquals(0, Float.compare(q3.z(), iq.z()));
    }

    @Test
    public void testNormalizedQuaternionConjugateAndInverseAreEqual() {
        final float w = 1;
        final float x = 2;
        final float y = 3;
        final float z = 4;
        Quaternion n = Quaternionf.normalize(w, x, y, z);
        Quaternion i = n.inverse();
        Quaternion c = n.conjugate();

        assertTrue(i.equals(c));
        assertEquals(0, Float.compare(n.length(), 1f));
    }

    @Test
    public void testQuaternionLength() {
        final float w = 1;
        final float x = 2;
        final float y = 3;
        final float z = 4;
        final float m = (float) Math.sqrt(w * w + x * x + y * y + z * z);
        Quaternion q = new Quaternionf(w, x, y, z);

        assertEquals(0, Float.compare(q.length(), m));
    }

    @Test
    public void testQuaternionLengthSquared() {
        final float w = 1;
        final float x = 2;
        final float y = 3;
        final float z = 4;
        final float m = w * w + x * x + y * y + z * z;
        Quaternion q = new Quaternionf(w, x, y, z);

        assertEquals(0, Float.compare(q.lengthSquared(), m));
    }

    @Test
    public void testQuaternionRotationVector() {
        final float w = 1;
        final float x = 2;
        final float y = 3;
        final float z = 4;
        Quaternion q = new Quaternionf(w, x, y, z);
        Vector3 v = new Vector3f(x, y, z);

        Vector3 rv1 = rotate(q, v);
        Vector3 rv2 = q.rotate(v);

        assertEquals(0, Float.compare(rv1.x(), rv2.x()));
        assertEquals(0, Float.compare(rv1.y(), rv2.y()));
        assertEquals(0, Float.compare(rv1.z(), rv2.z()));
    }

    @Test
    public void testQuaternionIsZeroLength() {
        Quaternion q = new Quaternionf(0, 0, 0, 0);

        assertTrue(q.isZeroLength());
        assertTrue(q.length() == 0);
    }

    @Test
    public void testLerp() {
        final float t = 0.4f;
        Quaternion p = new Quaternionf(1, 2, 3, 4);
        Quaternion q = new Quaternionf(1, 3, 4, 5);

        Quaternion pq1 = lerp(p, q, t);
        Quaternion pq2 = p.lerp(q, t);

        assertEquals(pq1.w(), pq2.w(), TestUtil.TOLERANCE);
        assertEquals(pq1.x(), pq2.x(), TestUtil.TOLERANCE);
        assertEquals(pq1.y(), pq2.y(), TestUtil.TOLERANCE);
        assertEquals(pq1.z(), pq2.z(), TestUtil.TOLERANCE);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testLerpThrowsNullPointerException() {
        Quaternionf.identity().lerp(null, 0.5f);
    }

    @Test
    public void testSlerp() {
        final float t = 0.6f;
        Quaternion p = new Quaternionf(1, 2, 3, 4);
        Quaternion q = new Quaternionf(1, 3, 4, 5);

        Quaternion pq1 = slerp(p, q, t);
        Quaternion pq2 = p.slerp(q, t);

        assertEquals(pq1.w(), pq2.w(), TestUtil.TOLERANCE);
        assertEquals(pq1.x(), pq2.x(), TestUtil.TOLERANCE);
        assertEquals(pq1.y(), pq2.y(), TestUtil.TOLERANCE);
        assertEquals(pq1.z(), pq2.z(), TestUtil.TOLERANCE);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSlerpThrowsNullPointerException() {
        Quaternionf.identity().slerp(null, 0.5f);
    }

    @Test
    public void testNegationMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        final float w = 0.5f;
        Quaternion p = new Quaternionf(w, x, y, z);
        Quaternion q = p.negate();

        assertEquals(0, Float.compare(q.x(), -x));
        assertEquals(0, Float.compare(q.y(), -y));
        assertEquals(0, Float.compare(q.z(), -z));
        assertEquals(0, Float.compare(q.w(), -w));
    }

    @Test
    public void testComparison() {
        Quaternion q0 = new Quaternionf(0f, 0f, 0f, 0f);
        Quaternion q1 = new Quaternionf(1f, 0f, 0f, 0f);
        Quaternion q2 = new Quaternionf(-1f, .5f, -.5f, .5f);

        assertEquals(0, q1.compareTo(q1));
        assertEquals(1, q1.compareTo(q0));
        assertEquals(-1, q1.compareTo(q2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Quaternion q = new Quaternionf(1f, 1f, 1f, 1f);

        assertTrue(q.equals(q));
        assertFalse(q.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Quaternion q1 = new Quaternionf(1f, 1f, 1f, 1f);
        Quaternion q2 = new Quaternionf(1f, 1f, 1f, 1f);

        assertTrue(q1.equals(q2));
        assertTrue(q2.equals(q1));

        assertTrue(q1.hashCode() == q2.hashCode());

        assertFalse(q1.equals(null));
        assertFalse(q2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Quaternion q1 = new Quaternionf(1f, 1f, 1f, 1f);
        Quaternion q2 = new Quaternionf(1f, 1f, 1f, 1f);
        Quaternion q3 = new Quaternionf(1f, 1f, 1f, 1f);

        assertTrue(q1.equals(q2));
        assertTrue(q2.equals(q3));
        assertTrue(q1.equals(q3));

        assertTrue(q1.hashCode() == q2.hashCode());
        assertTrue(q2.hashCode() == q3.hashCode());
        assertTrue(q1.hashCode() == q3.hashCode());

        assertFalse(q1.equals(null));
        assertFalse(q2.equals(null));
        assertFalse(q3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Quaternion q1 = new Quaternionf(1f, 1f, 1f, 1f);
        Quaternion q2 = new Quaternionf(1f, 1f, 1f, 1f);

        assertTrue(q1.equals(q2));
        assertTrue(q1.equals(q2));

        assertTrue(q1.hashCode() == q2.hashCode());
        assertTrue(q1.hashCode() == q2.hashCode());

        q1 = new Quaternionf(.5f, 1.5f, 1.5f, 2f);
        assertFalse(q1.equals(q2));
        assertFalse(q1.hashCode() == q2.hashCode());

        assertFalse(q1.equals(null));
        assertFalse(q2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Quaternionf.identity().equals(null));
        assertFalse(new Quaternionf(1f, 2f, 3f, 4f).equals(null));
    }

    @Test
    public void testToString() {
        Quaternion iq = Quaternionf.identity();

        assertEquals(Quaternionf.class.getSimpleName() + "(1.0, [0.0, 0.0, 0.0])", iq.toString());
    }

    // ========================================================================
    // Helper implementation methods
    // ========================================================================

    private static Vector3 rotate(Quaternion quat, Vector3 vec) {
        // turn the vector into a its pure/unrotated quaternion form, with w = 0
        Quaternion p = Quaternionf.normalize(0, vec.x(), vec.y(), vec.z());
        Quaternion q = quat.normalize();

        // p' = qpq*, where p' is the rotated vector in quaternion form
        Quaternion v = q.mult(p).mult(q.conjugate()).normalize();
        return v.axis();
    }

    private static Quaternion inverse(Quaternion q) {
        float s = 1f / q.lengthSquared();
        return new Quaternionf(q.w() * s, -q.x() * s, -q.y() * s, -q.z() * s);
    }

    private static Quaternion conjugate(Quaternion q) {
        return new Quaternionf(q.w(), -q.x(), -q.y(), -q.z());
    }

    private static Quaternion lerp(Quaternion q0, Quaternion q1, float t) {
        // lerp(q0, q1, t) = [(1 - t) * q0] + [t * q1]
        // https://en.wikipedia.org/wiki/Slerp#Geometric_Slerp
        Quaternion t0 = q0.mult(1.0f - t);
        Quaternion t1 = q1.mult(t);
        return t0.add(t1);
    }

    private static Quaternion slerp(Quaternion p, Quaternion q, float t) {
        // only normalized quaternions are valid rotations
        p = p.normalize();
        q = q.normalize();

        float cosTheta = p.dot(q);
        if (cosTheta < 0.0f) {
            q = q.mult(-1);
            cosTheta = -cosTheta;
        }

        if (Math.abs(cosTheta) < 1 - TestUtil.EPSILON) {
            // identity: sin^2(theta) + cos^2(theta) = 1 =>
            // sin^2(theta) = 1 - cos^2(theta)
            final float sinTheta = (float) Math.sqrt(1 - Math.pow(cosTheta, 2));
            final float theta = (float) Math.atan2(sinTheta, cosTheta);
            final float wp = (float) ((Math.sin((1f - t) * theta) / sinTheta));
            final float wq = (float) ((Math.sin(t * theta) / sinTheta));

            return p.mult(wp).add(q.mult(wq));
        } else {
            return p.lerp(q, t).normalize();
        }
    }

}
