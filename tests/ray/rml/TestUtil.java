/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

class TestUtil {

    public static final float EPSILON   = getMachineEpsilon();
    public static final float TOLERANCE = 1e-06f;

    private TestUtil() {}

    private static float getMachineEpsilon() {
        // https://en.wikipedia.org/wiki/Machine_epsilon#Approximation
        float epsilon = 1.0f;
        while ((1.0f + 0.5f * epsilon) != 1.0f)
            epsilon *= 0.5;

        return epsilon;
    }

    public static float[] generateValuesArray(int length) {
        float[] values = new float[length];
        for (int i = 0; i < length; ++i)
            values[i] = i;
        return values;
    }

    public static float[][] generateValuesArray(int rows, int cols) {
        float[][] values = new float[cols][rows];
        float i = 0f;
        for (int c = 0; c < cols; ++c)
            for (int r = 0; r < rows; ++r)
                values[c][r] = i++;
        return values;
    }

}
