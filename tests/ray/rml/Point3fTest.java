/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class Point3fTest {

    @Test
    public void testConstructorFromPrimitiveFloat2() {
        Point3 p = new Point3f(2f, 3f);

        assertEquals(0, Float.compare(p.x(), 2f));
        assertEquals(0, Float.compare(p.y(), 3f));
        assertEquals(0, Float.compare(p.z(), 0f));
    }

    @Test
    public void testConstructorFromPrimitiveFloat3() {
        Point3 p = new Point3f(2f, 3f, 4f);

        assertEquals(0, Float.compare(p.x(), 2f));
        assertEquals(0, Float.compare(p.y(), 3f));
        assertEquals(0, Float.compare(p.z(), 4f));
    }

    @Test
    public void testConstructorFromFloatArray2() {
        Point3 p = new Point3f(new float[] { 2f, 3f });

        assertEquals(0, Float.compare(p.x(), 2f));
        assertEquals(0, Float.compare(p.y(), 3f));
        assertEquals(0, Float.compare(p.z(), 0f));
    }

    @Test
    public void testConstructorFromFloatArray3() {
        Point3 p = new Point3f(new float[] { 2f, 3f, 4f });

        assertEquals(0, Float.compare(p.x(), 2f));
        assertEquals(0, Float.compare(p.y(), 3f));
        assertEquals(0, Float.compare(p.z(), 4f));
    }

    @Test
    public void testConstructorFromFloatArrayIgnoresExtraInput() {
        Point3 p = new Point3f(new float[] { 1f, 2f, 3f, 4f });

        assertEquals(0, Float.compare(p.x(), 1f));
        assertEquals(0, Float.compare(p.y(), 2f));
        assertEquals(0, Float.compare(p.z(), 3f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromFloatArrayFailsOnIncompleteInput() {
        new Point3f(new float[] { 1f });
    }

    @Test
    public void testConstructorFromVector3() {
        Vector3 v = new Vector3f(1, 2, 3);
        Point3 p = new Point3f(v);

        assertEquals(0, Float.compare(v.x(), p.x()));
        assertEquals(0, Float.compare(v.y(), p.y()));
        assertEquals(0, Float.compare(v.z(), p.z()));
    }

    @Test
    public void testConstructorFromVector4() {
        Vector4 v = new Vector4f(1, 2, 3, 4);
        Point3 p = new Point3f(v);

        assertEquals(0, Float.compare(v.x(), p.x()));
        assertEquals(0, Float.compare(v.y(), p.y()));
        assertEquals(0, Float.compare(v.z(), p.z()));
    }

    @Test
    public void testOriginPointFactory() {
        Point3 p = Point3f.origin();

        assertEquals(0, Float.compare(p.x(), 0f));
        assertEquals(0, Float.compare(p.y(), 0f));
        assertEquals(0, Float.compare(p.z(), 0f));
    }

    @Test
    public void testPointAddition() {
        Point3 p0 = new Point3f(1f, 2f, 3f);
        Point3 p1 = new Point3f(4f, 5f, 6f);
        Point3 p3 = p0.add(p1);

        assertEquals(0, Float.compare(5f, p3.x()));
        assertEquals(0, Float.compare(7f, p3.y()));
        assertEquals(0, Float.compare(9f, p3.z()));
    }

    @Test
    public void testScalarAddition() {
        Point3 p0 = new Point3f(1f, 2f, 3f);
        Point3 p1 = p0.add(4f, 5f, 6f);

        assertEquals(0, Float.compare(5f, p1.x()));
        assertEquals(0, Float.compare(7f, p1.y()));
        assertEquals(0, Float.compare(9f, p1.z()));
    }

    @Test
    public void testPointSubtraction() {
        Point3 p0 = new Point3f(5f, 5f, 4f);
        Point3 p1 = new Point3f(3f, 2f, 4f);
        Point3 p3 = p0.sub(p1);

        assertEquals(0, Float.compare(2f, p3.x()));
        assertEquals(0, Float.compare(3f, p3.y()));
        assertEquals(0, Float.compare(0f, p3.z()));
    }

    @Test
    public void testScalarSubtraction() {
        Point3 p0 = new Point3f(5f, 5f, 4f);
        Point3 p1 = p0.sub(3f, 2f, 4f);

        assertEquals(0, Float.compare(2f, p1.x()));
        assertEquals(0, Float.compare(3f, p1.y()));
        assertEquals(0, Float.compare(0f, p1.z()));
    }

    @Test
    public void testPointMultiplicationFloat() {
        Point3 p0 = new Point3f(5f, 3f, 4f);
        Point3 p1 = p0.mult(2f);

        assertEquals(0, Float.compare(10f, p1.x()));
        assertEquals(0, Float.compare(6f, p1.y()));
        assertEquals(0, Float.compare(8f, p1.z()));
    }

    @Test
    public void testPointRotationAroundVectorAxis() {
        Angle angle = new Degreef(180f);
        Vector3 axis = Vector3f.unitZ();

        Point3 p0 = new Point3f(1, 1, 1);
        Point3 p1 = Matrix3f.rotation(angle, axis).mult(p0);

        // since the rotation is only about the z-axis, only the x/y values
        // should change
        assertEquals(-1f, p1.x(), TestUtil.TOLERANCE);
        assertEquals(-1f, p1.y(), TestUtil.TOLERANCE);
        assertEquals(1f, p1.z());
    }

    @Test
    public void testPointDivisionFloat() {
        Point3 p0 = new Point3f(10f, 6f, 8f);
        Point3 p1 = p0.div(2f);

        assertEquals(0, Float.compare(5f, p1.x()));
        assertEquals(0, Float.compare(3f, p1.y()));
        assertEquals(0, Float.compare(4f, p1.z()));
    }

    @Test
    public void testScalarMultiplicationFloat() {
        Point3 p0 = new Point3f(4f, 3f, 2f);
        Point3 p1 = p0.mult(2f);

        assertEquals(0, Float.compare(8f, p1.x()));
        assertEquals(0, Float.compare(6f, p1.y()));
        assertEquals(0, Float.compare(4f, p1.z()));
    }

    @Test
    public void testScalarDivisionFloat() {
        Point3 p0 = new Point3f(4f, 8f, 12f);
        Point3 p1 = p0.div(2f);

        assertEquals(0, Float.compare(2f, p1.x()));
        assertEquals(0, Float.compare(4f, p1.y()));
        assertEquals(0, Float.compare(6f, p1.z()));
    }

    @Test
    public void testFloatArrayValues() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;
        Point3 p = new Point3f(x, y, z);
        float[] vals = p.toFloatArray();

        assertTrue(vals.length == 3);

        assertEquals(0, Float.compare(vals[0], x));
        assertEquals(0, Float.compare(vals[1], y));
        assertEquals(0, Float.compare(vals[2], z));

        assertEquals(0, Float.compare(vals[0], p.x()));
        assertEquals(0, Float.compare(vals[1], p.y()));
        assertEquals(0, Float.compare(vals[2], p.z()));
    }

    @Test
    public void testLerp() {
        final float t = 0.5f;
        final float x1 = 0f;
        final float y1 = 0f;
        final float z1 = 0f;
        final float x2 = 1f;
        final float y2 = 0f;
        final float z2 = 0f;

        // lerp(p0, p1, t) = (1 - t) * p0 + t * p1
        // https://en.wikipedia.org/wiki/Slerp#Geometric_Slerp
        Point3 p0 = new Point3f(x1, y1, z1);
        Point3 p1 = new Point3f(x2, y2, z2);

        // temp values
        Point3 t0 = p0.mult(1.0f - t);
        Point3 t1 = p1.mult(t);

        // interpolated result
        Point3 p2 = t0.add(t1);

        assertTrue(p0.lerp(p1, t).equals(p2));
    }

    @Test
    public void testLerpParameterClampedMax() {
        final float t = 1.25f;
        Point3 p0 = new Point3f(1f, 0f, 0f);
        Point3 p1 = new Point3f(0f, 1f, 0f);

        Point3 p2 = p0.lerp(p1, t);
        Point3 p3 = p0.lerp(p1, 1);

        assertTrue(p2.equals(p3));
    }

    @Test
    public void testLerpParameterClampedMin() {
        final float t = -1.25f;
        Point3 p0 = new Point3f(1f, 0f, 0f);
        Point3 p1 = new Point3f(0f, 1f, 0f);

        Point3 p2 = p0.lerp(p1, t);
        Point3 p3 = p0.lerp(p1, 0);

        assertTrue(p2.equals(p3));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testLerpThrowsNullPointerException() {
        Point3 v0 = Point3f.origin();
        v0.lerp(null, 0.5f);
    }

    @Test
    public void testNegationMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;
        Point3 p = new Point3f(x, y, z);
        Point3 n = p.negate();

        assertEquals(0, Float.compare(n.x(), -x));
        assertEquals(0, Float.compare(n.y(), -y));
        assertEquals(0, Float.compare(n.z(), -z));
    }

    @Test
    public void testComparison() {
        Point3 p0 = new Point3f(0f, 0f, 0f);
        Point3 p1 = new Point3f(1f, 1f, 1f);
        Point3 p2 = new Point3f(2f, 2f, 2f);

        assertEquals(0, p1.compareTo(p1));
        assertEquals(1, p1.compareTo(p0));
        assertEquals(-1, p1.compareTo(p2));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Point3 p = new Point3f(1f, 1f, 1f);

        assertTrue(p.equals(p));
        assertFalse(p.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Point3 p0 = new Point3f(1f, 1f, 1f);
        Point3 p1 = new Point3f(1f, 1f, 1f);

        assertTrue(p0.equals(p1));
        assertTrue(p1.equals(p0));

        assertTrue(p0.hashCode() == p1.hashCode());

        assertFalse(p0.equals(null));
        assertFalse(p1.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Point3 p0 = new Point3f(1f, 1f, 1f);
        Point3 p1 = new Point3f(1f, 1f, 1f);
        Point3 p2 = new Point3f(1f, 1f, 1f);

        assertTrue(p0.equals(p1));
        assertTrue(p1.equals(p2));
        assertTrue(p0.equals(p2));

        assertTrue(p0.hashCode() == p1.hashCode());
        assertTrue(p1.hashCode() == p2.hashCode());
        assertTrue(p0.hashCode() == p2.hashCode());

        assertFalse(p0.equals(null));
        assertFalse(p1.equals(null));
        assertFalse(p2.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Point3 p0 = new Point3f(1f, 1f, 1f);
        Point3 p1 = new Point3f(1f, 1f, 1f);

        assertTrue(p0.equals(p1));
        assertTrue(p0.equals(p1));

        assertTrue(p0.hashCode() == p1.hashCode());
        assertTrue(p0.hashCode() == p1.hashCode());

        p0 = new Point3f(1.5f, 1.5f, 1.5f);
        assertFalse(p0.equals(p1));
        assertFalse(p0.hashCode() == p1.hashCode());

        assertFalse(p0.equals(null));
        assertFalse(p1.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Point3f.origin().equals(null));
        assertFalse(new Point3f(1f, 2f, 3f).equals(null));
    }

    @Test
    public void testToString() {
        Point3 p = Point3f.origin();

        assertEquals(Point3f.class.getSimpleName() + "(0.0, 0.0, 0.0)", p.toString());
    }
}
