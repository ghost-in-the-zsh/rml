/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.Assert.*;

import org.testng.annotations.*;

public class MathUtilTest {

    @Test
    public void testToDegreesMatches() {
        final float radians = (float) (Math.PI / 2.0);
        final float degrees = (float) Math.toDegrees(radians);

        assertEquals(MathUtil.toDegrees(radians), degrees, TestUtil.TOLERANCE);
    }

    @Test
    public void testToRadiansMatches() {
        final float degrees = 90.0f;
        final float radians = (float) Math.toRadians(degrees);

        assertEquals(MathUtil.toRadians(degrees), radians, TestUtil.TOLERANCE);
    }

    @Test
    public void testTrigFunctionSinMatches() {
        final float theta = (float) (Math.PI / 2.0);
        final float sin = (float) Math.sin(theta);

        assertEquals(MathUtil.sin(theta), sin, TestUtil.TOLERANCE);
    }

    @Test
    public void testTrigFunctionArcSinMatches() {
        final float theta = (float) (Math.PI / 2.0);
        final float sin = (float) Math.sin(theta);
        final float asin = (float) Math.asin(sin);

        assertEquals(MathUtil.asin(sin), asin, TestUtil.TOLERANCE);
    }

    @Test
    public void testTrigFunctionCosMatches() {
        final float theta = (float) (Math.PI / 2.0);
        final float cos = (float) Math.cos(theta);

        assertEquals(MathUtil.cos(theta), cos, TestUtil.TOLERANCE);
    }

    @Test
    public void testTrigFunctionArcCosMatches() {
        final float theta = (float) (Math.PI / 2.0);
        final float cos = (float) Math.cos(theta);
        final float acos = (float) Math.acos(cos);

        assertEquals(MathUtil.acos(cos), acos, TestUtil.TOLERANCE);
    }

    @Test
    public void testSqrtMatches() {
        final float x = 4f;
        final float r = (float) Math.sqrt(x);

        assertEquals(MathUtil.sqrt(x), r, TestUtil.TOLERANCE);
    }

    @Test
    public void testInverseSqrtMatches() {
        final float x = 4f;
        final float r = (float) (1.0 / Math.sqrt(x));

        assertEquals(MathUtil.invSqrt(x), r, TestUtil.TOLERANCE);
    }

    @Test
    public void testClampReturnsMinimum() {
        final float x = -2f;
        final float min = -1f;
        final float max = 1f;

        assertEquals(MathUtil.clamp(x, min, max), min);
    }

    @Test
    public void testClampReturnsInput() {
        final float x = 0f;
        final float min = -1f;
        final float max = 1f;

        assertEquals(MathUtil.clamp(x, min, max), x);
    }

    @Test
    public void testClampReturnsMaximum() {
        final float x = 2f;
        final float min = -1f;
        final float max = 1f;

        assertEquals(MathUtil.clamp(x, min, max), max);
    }

    @Test
    public void testLengthMatches3() {
        float x = 1;
        float y = 2;
        float z = 3;
        float len = (float) Math.sqrt(x * x + y * y + z * z);

        assertEquals(MathUtil.length(x, y, z), len, TestUtil.TOLERANCE);
    }

    @Test
    public void testLengthMatches4() {
        float x = 1;
        float y = 2;
        float z = 3;
        float w = 4;
        float len = (float) Math.sqrt(x * x + y * y + z * z + w * w);

        assertEquals(MathUtil.length(x, y, z, w), len, TestUtil.TOLERANCE);
    }

    @Test
    public void testLengthSquaredMatches3() {
        float x = 1;
        float y = 2;
        float z = 3;
        float slen = x * x + y * y + z * z;

        assertEquals(MathUtil.lengthSquared(x, y, z), slen, TestUtil.TOLERANCE);
    }

    @Test
    public void testLengthSquaredMatches4() {
        float x = 1;
        float y = 2;
        float z = 3;
        float w = 4;
        float slen = x * x + y * y + z * z + w * w;

        assertEquals(MathUtil.lengthSquared(x, y, z, w), slen, TestUtil.TOLERANCE);
    }

    // A 3D array is returned for the following reasons:
    // 1. outer dimension: number of times the target test case will be invoked
    // (i.e. just once);
    // 2. mid dimension: number of parameters the test case expects (2
    // parameters);
    // 3. inner dimension: the values each of the parameters in #2 will take
    // (i.e. each parameter is a float[])
    //
    // Reference:
    // http://testng.org/doc/documentation-main.html#parameters-dataproviders
    @DataProvider(name = "LerpDataProvider", parallel = true)
    private float[][][] createLerpData() {
        // arrays must have the same length
        final float[] s = { 1, 2, 3, 4 };
        final float[] e = { 2, 4, 6, 8 };
        return new float[][][] { { s, e } };
    }

    @Test(dataProvider = "LerpDataProvider")
    public void testLerpReturnsStartPoint(final float[] s, final float[] e) {
        final float t = 0.0f;

        final float[] r = MathUtil.lerp(s, e, t);
        for (int i = 0; i < s.length; ++i)
            assertEquals(r[i], s[i], TestUtil.TOLERANCE);
    }

    @Test(dataProvider = "LerpDataProvider")
    public void testLerpReturnsMidPoint(final float[] s, final float[] e) {
        final float t = 0.5f;
        final float d = 1.0f - t;
        final float[] m = new float[s.length];

        for (int i = 0; i < m.length; ++i)
            m[i] = d * s[i] + t * e[i];

        final float[] r = MathUtil.lerp(s, e, t);
        for (int i = 0; i < m.length; ++i)
            assertEquals(r[i], m[i], TestUtil.TOLERANCE);
    }

    @Test(dataProvider = "LerpDataProvider")
    public void testLerpReturnsEndPoint(final float[] s, final float[] e) {
        final float t = 1.0f;

        final float[] r = MathUtil.lerp(s, e, t);
        for (int i = 0; i < e.length; ++i)
            assertEquals(r[i], e[i], TestUtil.TOLERANCE);
    }

}
