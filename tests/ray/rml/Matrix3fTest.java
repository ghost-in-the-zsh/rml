/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

public class Matrix3fTest {

    @Test
    public void testConstructorFromColumnVectors() {
        Vector3 c0 = new Vector3f(1f, 2f, 3f);
        Vector3 c1 = new Vector3f(4f, 5f, 6f);
        Vector3 c2 = new Vector3f(7f, 8f, 9f);
        Matrix3 m = new Matrix3f(c0, c1, c2);

        assertEquals(0, Float.compare(m.value(0, 0), c0.x()));
        assertEquals(0, Float.compare(m.value(1, 0), c0.y()));
        assertEquals(0, Float.compare(m.value(2, 0), c0.z()));

        assertEquals(0, Float.compare(m.value(0, 1), c1.x()));
        assertEquals(0, Float.compare(m.value(1, 1), c1.y()));
        assertEquals(0, Float.compare(m.value(2, 1), c1.z()));

        assertEquals(0, Float.compare(m.value(0, 2), c2.x()));
        assertEquals(0, Float.compare(m.value(1, 2), c2.y()));
        assertEquals(0, Float.compare(m.value(2, 2), c2.z()));
    }

    @Test
    public void testConstructorFromPrimitiveArray1D() {
        final float[] values = new float[] { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f };
        Matrix3 m = new Matrix3f(values);

        assertEquals(0, Float.compare(m.value(0, 0), values[0]));
        assertEquals(0, Float.compare(m.value(1, 0), values[1]));
        assertEquals(0, Float.compare(m.value(2, 0), values[2]));

        assertEquals(0, Float.compare(m.value(0, 1), values[3]));
        assertEquals(0, Float.compare(m.value(1, 1), values[4]));
        assertEquals(0, Float.compare(m.value(2, 1), values[5]));

        assertEquals(0, Float.compare(m.value(0, 2), values[6]));
        assertEquals(0, Float.compare(m.value(1, 2), values[7]));
        assertEquals(0, Float.compare(m.value(2, 2), values[8]));
    }

    @Test
    public void testConstructorFromPrimitiveArray1DIgnoresExtraInput() {
        final float[] values = new float[] { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f };
        Matrix3 m = new Matrix3f(values);

        assertEquals(0, Float.compare(m.value(0, 0), values[0]));
        assertEquals(0, Float.compare(m.value(1, 0), values[1]));
        assertEquals(0, Float.compare(m.value(2, 0), values[2]));

        assertEquals(0, Float.compare(m.value(0, 1), values[3]));
        assertEquals(0, Float.compare(m.value(1, 1), values[4]));
        assertEquals(0, Float.compare(m.value(2, 1), values[5]));

        assertEquals(0, Float.compare(m.value(0, 2), values[6]));
        assertEquals(0, Float.compare(m.value(1, 2), values[7]));
        assertEquals(0, Float.compare(m.value(2, 2), values[8]));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromPrimitiveArray1DFailsOnIncompleteInput() {
        new Matrix3f(new float[] { 0f, 1f, 2f });
    }

    @Test
    public void testConstructorFromPrimitiveArray2D() {
        // @formatter:off
        float[][] values = new float[][] {
            { 0f, 1f, 2f },
            { 3f, 4f, 5f },
            { 6f, 7f, 8f }
        };
        // @formatter:on
        Matrix3 m = new Matrix3f(values);

        assertEquals(0, Float.compare(m.value(0, 0), values[0][0]));
        assertEquals(0, Float.compare(m.value(1, 0), values[0][1]));
        assertEquals(0, Float.compare(m.value(2, 0), values[0][2]));

        assertEquals(0, Float.compare(m.value(0, 1), values[1][0]));
        assertEquals(0, Float.compare(m.value(1, 1), values[1][1]));
        assertEquals(0, Float.compare(m.value(2, 1), values[1][2]));

        assertEquals(0, Float.compare(m.value(0, 2), values[2][0]));
        assertEquals(0, Float.compare(m.value(1, 2), values[2][1]));
        assertEquals(0, Float.compare(m.value(2, 2), values[2][2]));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromPrimitiveArray2DFailsOnIncompleteColumnData() {
        // @formatter:off
        new Matrix3f(new float[][] {
            { 0f, 1f },
            { 2f, 3f },
            { 4f, 5f }
        });
        // @formatter:on
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromPrimitiveArray2DFailsOnIncompleteColumnCount() {
        // @formatter:off
        new Matrix3f(new float[][] {
            { 0f, 2f, 4f },
            { 1f, 3f, 5f }
        });
        // @formatter:on
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testConstructorFromPrimitiveArray2DFailsOnCompleteDataInIncompleteColumnCount() {
        new Matrix3f(new float[][] { { 0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f } });
    }

    @Test
    public void testConstructorFromQuaternion() {
        // This test covers 2 cases:
        // 1. That the ctor works, and
        // 2. That an identity quaternion produces an identity matrix
        Quaternion q = Quaternionf.identity();
        Matrix3 m = new Matrix3f(q);

        assertEquals(0, Float.compare(m.value(0, 0), 1f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 1f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 1f));
    }

    @Test
    public void testFactoryIdentityMatrix() {
        Matrix3 m = Matrix3f.identity();

        assertEquals(0, Float.compare(m.value(0, 0), 1f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 1f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 1f));
    }

    @Test
    public void testFactoryZeroMatrix() {
        Matrix3 m = Matrix3f.zero();

        assertEquals(0, Float.compare(m.value(0, 0), 0f));
        assertEquals(0, Float.compare(m.value(1, 0), 0f));
        assertEquals(0, Float.compare(m.value(2, 0), 0f));

        assertEquals(0, Float.compare(m.value(0, 1), 0f));
        assertEquals(0, Float.compare(m.value(1, 1), 0f));
        assertEquals(0, Float.compare(m.value(2, 1), 0f));

        assertEquals(0, Float.compare(m.value(0, 2), 0f));
        assertEquals(0, Float.compare(m.value(1, 2), 0f));
        assertEquals(0, Float.compare(m.value(2, 2), 0f));
    }

    @Test
    public void testCanRetrieveRow() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        // @formatter:on
        Vector3 rv0 = m.row(0);
        Vector3 rv1 = m.row(1);
        Vector3 rv2 = m.row(2);

        assertEquals(0, Float.compare(rv0.x(), 1f));
        assertEquals(0, Float.compare(rv0.y(), 2f));
        assertEquals(0, Float.compare(rv0.z(), 3f));

        assertEquals(0, Float.compare(rv1.x(), 4f));
        assertEquals(0, Float.compare(rv1.y(), 5f));
        assertEquals(0, Float.compare(rv1.z(), 6f));

        assertEquals(0, Float.compare(rv2.x(), 7f));
        assertEquals(0, Float.compare(rv2.y(), 8f));
        assertEquals(0, Float.compare(rv2.z(), 9f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testRowRejectsLargeIndex() {
        Matrix3f.identity().row(3);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testRowRejectsSmallIndex() {
        Matrix3f.identity().row(-1);
    }

    @Test
    public void testCanRetrieveColumn() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        );
        // @formatter:on
        Vector3 cv0 = m.column(0);
        Vector3 cv1 = m.column(1);
        Vector3 cv2 = m.column(2);

        assertEquals(0, Float.compare(cv0.x(), 1f));
        assertEquals(0, Float.compare(cv0.y(), 2f));
        assertEquals(0, Float.compare(cv0.z(), 3f));

        assertEquals(0, Float.compare(cv1.x(), 4f));
        assertEquals(0, Float.compare(cv1.y(), 5f));
        assertEquals(0, Float.compare(cv1.z(), 6f));

        assertEquals(0, Float.compare(cv2.x(), 7f));
        assertEquals(0, Float.compare(cv2.y(), 8f));
        assertEquals(0, Float.compare(cv2.z(), 9f));
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testColumnRejectsLargeIndex() {
        Matrix3f.identity().column(3);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testColumnRejectsSmallIndex() {
        Matrix3f.identity().column(-1);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsNegativeRowIndex() {
        Matrix3f.identity().value(-1, 0);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsLargeRowIndex() {
        Matrix3f.identity().value(5, 0);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsNegativeColumnIndex() {
        Matrix3f.identity().value(0, -1);
    }

    @Test(expectedExceptions = { IndexOutOfBoundsException.class })
    public void testValueRejectsLargeColumnIndex() {
        Matrix3f.identity().value(0, 7);
    }

    @Test
    public void testMatrixAddition() {
        // @formatter:off
        Matrix3 m1 = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        Matrix3 m2 = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        // @formatter:on
        Matrix3 rm = m1.add(m2);

        assertEquals(0, Float.compare(rm.value(0, 0), 2f));
        assertEquals(0, Float.compare(rm.value(0, 1), 4f));
        assertEquals(0, Float.compare(rm.value(0, 2), 6f));

        assertEquals(0, Float.compare(rm.value(1, 0), 8f));
        assertEquals(0, Float.compare(rm.value(1, 1), 10f));
        assertEquals(0, Float.compare(rm.value(1, 2), 12f));

        assertEquals(0, Float.compare(rm.value(2, 0), 14f));
        assertEquals(0, Float.compare(rm.value(2, 1), 16f));
        assertEquals(0, Float.compare(rm.value(2, 2), 18f));
    }

    @Test
    public void testMatrixSubtraction() {
        // @formatter:off
        Matrix3 m1 = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        Matrix3 m2 = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        // @formatter:on
        Matrix3 rm = m1.sub(m2);

        assertEquals(0, Float.compare(rm.value(0, 0), 0f));
        assertEquals(0, Float.compare(rm.value(1, 0), 0f));
        assertEquals(0, Float.compare(rm.value(2, 0), 0f));

        assertEquals(0, Float.compare(rm.value(0, 1), 0f));
        assertEquals(0, Float.compare(rm.value(1, 1), 0f));
        assertEquals(0, Float.compare(rm.value(2, 1), 0f));

        assertEquals(0, Float.compare(rm.value(0, 2), 0f));
        assertEquals(0, Float.compare(rm.value(1, 2), 0f));
        assertEquals(0, Float.compare(rm.value(2, 2), 0f));
    }

    @Test
    public void testMatrixScalarMultiplication() {
        // @formatter:off
        Matrix3 m1 = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        // @formatter:on
        Matrix3 rm = m1.mult(5f);

        assertEquals(0, Float.compare(rm.value(0, 0), 5f));
        assertEquals(0, Float.compare(rm.value(0, 1), 10f));
        assertEquals(0, Float.compare(rm.value(0, 2), 15f));

        assertEquals(0, Float.compare(rm.value(1, 0), 20f));
        assertEquals(0, Float.compare(rm.value(1, 1), 25f));
        assertEquals(0, Float.compare(rm.value(1, 2), 30f));

        assertEquals(0, Float.compare(rm.value(2, 0), 35f));
        assertEquals(0, Float.compare(rm.value(2, 1), 40f));
        assertEquals(0, Float.compare(rm.value(2, 2), 45f));
    }

    @Test
    public void testMatrixVectorMultiplication() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
            2f, 2f, 2f,
            3f, 3f, 3f,
            4f, 4f, 4f
        );
        // @formatter:on
        Vector3 v = new Vector3f(2f, 3f, 4f);
        Vector3 rv = m.mult(v);

        assertEquals(0, Float.compare(rv.x(), 18f));
        assertEquals(0, Float.compare(rv.y(), 27f));
        assertEquals(0, Float.compare(rv.z(), 36f));
    }

    @Test
    public void testMatrixPointMultiplication() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
            2f, 2f, 2f,
            3f, 3f, 3f,
            4f, 4f, 4f
        );
        // @formatter:on
        Point3 p = new Point3f(2f, 3f, 4f);
        Point3 q = m.mult(p);

        assertEquals(0, Float.compare(q.x(), 18f));
        assertEquals(0, Float.compare(q.y(), 27f));
        assertEquals(0, Float.compare(q.z(), 36f));
    }

    @Test
    public void testMatrixMatrixMultiplication() {
        // @formatter:off
        Matrix3 m1 = new Matrix3f(
            0f, 3f, 6f,
            1f, 4f, 7f,
            2f, 5f, 8f
        );
        Matrix3 m2 = new Matrix3f(
            8f, 5f, 2f,
            7f, 4f, 1f,
            6f, 3f, 0f
        );
        // @formatter:on
        Matrix3 rm1 = m1.mult(m2);
        Matrix3 rm2 = multiply(m1, m2);

        assertEquals(0, Float.compare(rm1.value(0, 0), rm2.value(0, 0)));
        assertEquals(0, Float.compare(rm1.value(1, 0), rm2.value(1, 0)));
        assertEquals(0, Float.compare(rm1.value(2, 0), rm2.value(2, 0)));

        assertEquals(0, Float.compare(rm1.value(0, 1), rm2.value(0, 1)));
        assertEquals(0, Float.compare(rm1.value(1, 1), rm2.value(1, 1)));
        assertEquals(0, Float.compare(rm1.value(2, 1), rm2.value(2, 1)));

        assertEquals(0, Float.compare(rm1.value(0, 2), rm2.value(0, 2)));
        assertEquals(0, Float.compare(rm1.value(1, 2), rm2.value(1, 2)));
        assertEquals(0, Float.compare(rm1.value(2, 2), rm2.value(2, 2)));
    }

    @Test
    public void testMatrixIdentityMultiplicationYieldsOriginalMatrix() {
        Matrix3 im = Matrix3f.identity();
        Matrix3 cm = new Matrix3f(1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f);
        Matrix3 pm = cm.mult(im);

        assertEquals(0, Float.compare(cm.value(0, 0), pm.value(0, 0)));
        assertEquals(0, Float.compare(cm.value(1, 0), pm.value(1, 0)));
        assertEquals(0, Float.compare(cm.value(2, 0), pm.value(2, 0)));

        assertEquals(0, Float.compare(cm.value(0, 1), pm.value(0, 1)));
        assertEquals(0, Float.compare(cm.value(1, 1), pm.value(1, 1)));
        assertEquals(0, Float.compare(cm.value(2, 1), pm.value(2, 1)));

        assertEquals(0, Float.compare(cm.value(0, 2), pm.value(0, 2)));
        assertEquals(0, Float.compare(cm.value(1, 2), pm.value(1, 2)));
        assertEquals(0, Float.compare(cm.value(2, 2), pm.value(2, 2)));
    }

    @Test
    public void testMatrixInverseMultiplicationYieldsIdentity() {
        // @formatter:off
        Matrix3 matrix = new Matrix3f(
           -1f,  3f, -3f,
            0f, -6f,  5f,
           -5f, -3f,  1f
        );
        Matrix3 inverse = Matrix3f.inverse(
           -1f,  3f, -3f,
            0f, -6f,  5f,
           -5f, -3f,  1f
        );
        // @formatter:on
        Matrix3 rm = matrix.mult(inverse);
        Matrix3 im = Matrix3f.identity();

        final float tolerance = 1e-5f;
        assertEquals(im.value(0, 0), rm.value(0, 0), tolerance);
        assertEquals(im.value(1, 0), rm.value(1, 0), tolerance);
        assertEquals(im.value(2, 0), rm.value(2, 0), tolerance);

        assertEquals(im.value(0, 1), rm.value(0, 1), tolerance);
        assertEquals(im.value(1, 1), rm.value(1, 1), tolerance);
        assertEquals(im.value(2, 1), rm.value(2, 1), tolerance);

        assertEquals(im.value(0, 2), rm.value(0, 2), tolerance);
        assertEquals(im.value(1, 2), rm.value(1, 2), tolerance);
        assertEquals(im.value(2, 2), rm.value(2, 2), tolerance);
    }

    @Test
    public void testScalingByPrimitive() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix3 im = Matrix3f.identity();
        Matrix3 sm = im.scale(sx, sy, sz);

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
    }

    @Test
    public void testScalingByVector() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix3 im = Matrix3f.identity();
        Matrix3 sm = im.scale(new Vector3f(sx, sy, sz));

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
    }

    @Test
    public void testMatrixDeterminantMatches() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
             4f,  4f, -2f,
            -1f,  5f,  0f,
             1f,  3f,  0f
        );
        // @formatter:on
        float det = m.determinant();

        assertEquals(0, Float.compare(det, 16f));
    }

    @Test
    public void testInverse() {
        // @formatter:off
        // http://mathforum.org/library/drmath/view/51680.html
        Matrix3 m = new Matrix3f(
            -1f,  3f, -3f,
             0f, -6f,  5f,
            -5f, -3f,  1f
        );
        Matrix3 inv1 = m.inverse();
        Matrix3 inv2 = new Matrix3f(
                1.5f  ,      1f   ,    -0.5f  , //  3f/2f ,   1f  , -1f/2f,
            -4.166667f, -2.666667f,  0.833333f, // -25f/6f, -8f/3f,  5f/6f,
                -5f   ,     -3f   ,     1f      // -5f    ,  -3f  ,   1f
        );
        // @formatter:on
        Matrix3 inv3 = inverse(m);

        // test implementation against hard-coded inverse data
        assertEquals(inv1.value(0, 0), inv2.value(0, 0), TestUtil.TOLERANCE);
        assertEquals(inv1.value(1, 0), inv2.value(1, 0), TestUtil.TOLERANCE);
        assertEquals(inv1.value(2, 0), inv2.value(2, 0), TestUtil.TOLERANCE);

        assertEquals(inv1.value(0, 1), inv2.value(0, 1), TestUtil.TOLERANCE);
        assertEquals(inv1.value(1, 1), inv2.value(1, 1), TestUtil.TOLERANCE);
        assertEquals(inv1.value(2, 1), inv2.value(2, 1), TestUtil.TOLERANCE);

        assertEquals(inv1.value(0, 2), inv2.value(0, 2), TestUtil.TOLERANCE);
        assertEquals(inv1.value(1, 2), inv2.value(1, 2), TestUtil.TOLERANCE);
        assertEquals(inv1.value(2, 2), inv2.value(2, 2), TestUtil.TOLERANCE);

        // test 2nd test implementation against hard-coded inverse data
        assertEquals(inv2.value(0, 0), inv3.value(0, 0), TestUtil.TOLERANCE);
        assertEquals(inv2.value(1, 0), inv3.value(1, 0), TestUtil.TOLERANCE);
        assertEquals(inv2.value(2, 0), inv3.value(2, 0), TestUtil.TOLERANCE);

        assertEquals(inv2.value(0, 1), inv3.value(0, 1), TestUtil.TOLERANCE);
        assertEquals(inv2.value(1, 1), inv3.value(1, 1), TestUtil.TOLERANCE);
        assertEquals(inv2.value(2, 1), inv3.value(2, 1), TestUtil.TOLERANCE);

        assertEquals(inv2.value(0, 2), inv3.value(0, 2), TestUtil.TOLERANCE);
        assertEquals(inv2.value(1, 2), inv3.value(1, 2), TestUtil.TOLERANCE);
        assertEquals(inv2.value(2, 2), inv3.value(2, 2), TestUtil.TOLERANCE);
    }

    @Test(expectedExceptions = { SingularMatrixException.class })
    public void testSingularMatrixInverseThrowsException() {
        Matrix3f.zero().inverse();
    }

    @Test(expectedExceptions = { SingularMatrixException.class })
    public void testFactorySingularMatrixInverseThrowsException() {
        // @formatter:off
        Matrix3f.inverse(
            0, 0, 0,
            0, 0, 0,
            0, 0, 0
        );
        // @formatter:on
    }

    @Test
    public void testFactoryInverseFromPrimitive() {
        // @formatter:off
        Matrix3 inv1 = Matrix3f.inverse(
             4f,  4f, -2f,
            -1f,  5f,  0f,
             1f,  3f,  0f
        );
        Matrix3 inv2 = inverse(
            new Matrix3f(
                4f,  4f, -2f,
               -1f,  5f,  0f,
                1f,  3f,  0f
            )
        );
        // @formatter:on

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
    }

    @Test
    public void testFactoryInverseFromVectors() {
        Vector3 c0 = new Vector3f(4, -1, 1);
        Vector3 c1 = new Vector3f(4, 5, 3);
        Vector3 c2 = new Vector3f(-2, 0, 0);

        Matrix3 inv1 = Matrix3f.inverse(c0, c1, c2);
        Matrix3 inv2 = inverse(new Matrix3f(c0, c1, c2));

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
    }

    @Test
    public void testFactoryInverseFromArray1D() {
        // @formatter:off
        Matrix3 inv1 = Matrix3f.inverse(new float[] {
             4f, -1f, 1f,
             4f,  5f, 3f,
            -2f,  0f, 0f
        });
        Matrix3 inv2 = inverse(
            new Matrix3f(
                4f,  4f, -2f,
               -1f,  5f,  0f,
                1f,  3f,  0f
            )
        );
        // @formatter:on

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
    }

    @Test
    public void testFactoryInverseFromArray2D() {
        // @formatter:off
        Matrix3 inv1 = Matrix3f.inverse(new float[][] {
            { 4f, -1f, 1f },
            { 4f,  5f, 3f },
            {-2f,  0f, 0f }
        });
        Matrix3 inv2 = inverse(
            new Matrix3f(
                4f,  4f, -2f,
               -1f,  5f,  0f,
                1f,  3f,  0f
            )
        );
        // @formatter:on

        assertEquals(0, Float.compare(inv1.value(0, 0), inv2.value(0, 0)));
        assertEquals(0, Float.compare(inv1.value(1, 0), inv2.value(1, 0)));
        assertEquals(0, Float.compare(inv1.value(2, 0), inv2.value(2, 0)));

        assertEquals(0, Float.compare(inv1.value(0, 1), inv2.value(0, 1)));
        assertEquals(0, Float.compare(inv1.value(1, 1), inv2.value(1, 1)));
        assertEquals(0, Float.compare(inv1.value(2, 1), inv2.value(2, 1)));

        assertEquals(0, Float.compare(inv1.value(0, 2), inv2.value(0, 2)));
        assertEquals(0, Float.compare(inv1.value(1, 2), inv2.value(1, 2)));
        assertEquals(0, Float.compare(inv1.value(2, 2), inv2.value(2, 2)));
    }

    @Test
    public void testMatrixTranspose() {
        // @formatter:off
        Matrix3 m = new Matrix3f(
            1f, 2f, 3f,
            4f, 5f, 6f,
            7f, 8f, 9f
        );
        // @formatter:on
        Matrix3 t = m.transpose();

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));

        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));

        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
    }

    @Test
    public void testFactoryTransposeFromVectors() {
        Vector3 c0 = new Vector3f(4, -1, 1);
        Vector3 c1 = new Vector3f(4, 5, 3);
        Vector3 c2 = new Vector3f(-2, 0, 0);
        Matrix3 t = Matrix3f.transpose(c0, c1, c2);

        assertEquals(0, Float.compare(t.value(0, 0), c0.x()));
        assertEquals(0, Float.compare(t.value(0, 1), c0.y()));
        assertEquals(0, Float.compare(t.value(0, 2), c0.z()));

        assertEquals(0, Float.compare(t.value(1, 0), c1.x()));
        assertEquals(0, Float.compare(t.value(1, 1), c1.y()));
        assertEquals(0, Float.compare(t.value(1, 2), c1.z()));

        assertEquals(0, Float.compare(t.value(2, 0), c2.x()));
        assertEquals(0, Float.compare(t.value(2, 1), c2.y()));
        assertEquals(0, Float.compare(t.value(2, 2), c2.z()));
    }

    @Test
    public void testMatrixTransposeFromArray1D() {
        // @formatter:off
        final float[] values = new float[] {
             4f, -1f, 1f,
             4f,  5f, 3f,
            -2f,  0f, 0f
        };
        // @formatter:on
        Matrix3 m = new Matrix3f(values);
        Matrix3 t = m.transpose();

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));

        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));

        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
    }

    @Test
    public void testMatrixTransposeFromArray2D() {
        // @formatter:off
        final float[][] values = new float[][] {
            {  4f, -1f, 1f },
            {  4f,  5f, 3f },
            { -2f,  0f, 0f }
        };
        // @formatter:on
        Matrix3 m = new Matrix3f(values);
        Matrix3 t = m.transpose();

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));

        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));

        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
    }

    @Test
    public void testFactoryTransposeFromArray1D() {
        // @formatter:off
        final float[] values = new float[] {
             4f, -1f, 1f,
             4f,  5f, 3f,
            -2f,  0f, 0f
        };
        // @formatter:on
        Matrix3 m = new Matrix3f(values);
        Matrix3 t = Matrix3f.transpose(values);

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));

        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));

        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
    }

    @Test
    public void testFactoryTransposeFromArray2D() {
        // @formatter:off
        final float[][] values = new float[][] {
            {  4f, -1f, 1f },
            {  4f,  5f, 3f },
            { -2f,  0f, 0f }
        };
        // @formatter:on
        Matrix3 m = new Matrix3f(values);
        Matrix3 t = Matrix3f.transpose(values);

        assertEquals(0, Float.compare(m.value(0, 0), t.value(0, 0)));
        assertEquals(0, Float.compare(m.value(1, 0), t.value(0, 1)));
        assertEquals(0, Float.compare(m.value(2, 0), t.value(0, 2)));

        assertEquals(0, Float.compare(m.value(0, 1), t.value(1, 0)));
        assertEquals(0, Float.compare(m.value(1, 1), t.value(1, 1)));
        assertEquals(0, Float.compare(m.value(2, 1), t.value(1, 2)));

        assertEquals(0, Float.compare(m.value(0, 2), t.value(2, 0)));
        assertEquals(0, Float.compare(m.value(1, 2), t.value(2, 1)));
        assertEquals(0, Float.compare(m.value(2, 2), t.value(2, 2)));
    }

    @Test
    public void testFactoryScalingFromPrimitives() {
        final float sx = 2f;
        final float sy = 4f;
        final float sz = 6f;
        Matrix3 sm = Matrix3f.scaling(sx, sy, sz);

        assertEquals(0, Float.compare(sm.value(0, 0), sx));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sy));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sz));
    }

    @Test
    public void testFactoryScalingFromVector() {
        Vector3 sv = new Vector3f(2f, 4f, 6f);
        Matrix3 sm = Matrix3f.scaling(sv);

        assertEquals(0, Float.compare(sm.value(0, 0), sv.x()));
        assertEquals(0, Float.compare(sm.value(1, 0), 0f));
        assertEquals(0, Float.compare(sm.value(2, 0), 0f));

        assertEquals(0, Float.compare(sm.value(0, 1), 0f));
        assertEquals(0, Float.compare(sm.value(1, 1), sv.y()));
        assertEquals(0, Float.compare(sm.value(2, 1), 0f));

        assertEquals(0, Float.compare(sm.value(0, 2), 0f));
        assertEquals(0, Float.compare(sm.value(1, 2), 0f));
        assertEquals(0, Float.compare(sm.value(2, 2), sv.z()));
    }

    @Test
    public void testFactoryRotationFromAngleAxis() {
        final Angle angle = new Degreef(45f);
        final Vector3 axis = new Vector3f(1f, 0f, 0f);
        Matrix3 im = Matrix3f.identity();
        Matrix3 r1 = Matrix3f.rotation(angle, axis);
        Matrix3 r2 = rotate(im, angle, axis);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));

        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));

        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
    }

    @Test
    public void testFloatArrayValues() {
        // @formatter:off
        float[] m = new Matrix3f(
            1f, 4f, 7f,
            2f, 5f, 8f,
            3f, 6f, 9f
        ).toFloatArray();
        // @formatter:on

        assertEquals(0, Float.compare(m[0], 1f));
        assertEquals(0, Float.compare(m[1], 2f));
        assertEquals(0, Float.compare(m[2], 3f));

        assertEquals(0, Float.compare(m[3], 4f));
        assertEquals(0, Float.compare(m[4], 5f));
        assertEquals(0, Float.compare(m[5], 6f));

        assertEquals(0, Float.compare(m[6], 7f));
        assertEquals(0, Float.compare(m[7], 8f));
        assertEquals(0, Float.compare(m[8], 9f));
    }

    @Test
    public void testRotationWithAngles() {
        final Angle angleX = new Degreef(45f);
        final Angle angleY = new Degreef(45f);
        final Angle angleZ = new Degreef(45f);
        Matrix3 im = Matrix3f.identity();
        Matrix3 r1 = im.rotate(angleX, angleY, angleZ);
        Matrix3 r2 = rotate(im, angleX, angleY, angleZ);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));

        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));

        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
    }

    @Test
    public void testRotationAngleAxisVector() {
        final Angle angle = new Degreef(45f);
        final Vector3 axis = new Vector3f(0f, 1f, 0f);
        Matrix3 im = Matrix3f.identity();
        Matrix3 r1 = im.rotate(angle, axis);
        Matrix3 r2 = rotate(im, angle, axis);

        assertEquals(0, Float.compare(r1.value(0, 0), r2.value(0, 0)));
        assertEquals(0, Float.compare(r1.value(0, 1), r2.value(0, 1)));
        assertEquals(0, Float.compare(r1.value(0, 2), r2.value(0, 2)));

        assertEquals(0, Float.compare(r1.value(1, 0), r2.value(1, 0)));
        assertEquals(0, Float.compare(r1.value(1, 1), r2.value(1, 1)));
        assertEquals(0, Float.compare(r1.value(1, 2), r2.value(1, 2)));

        assertEquals(0, Float.compare(r1.value(2, 0), r2.value(2, 0)));
        assertEquals(0, Float.compare(r1.value(2, 1), r2.value(2, 1)));
        assertEquals(0, Float.compare(r1.value(2, 2), r2.value(2, 2)));
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Matrix3 m = Matrix3f.identity();

        assertTrue(m.equals(m));
        assertFalse(m.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Matrix3 m1 = Matrix3f.identity();
        Matrix3 m2 = Matrix3f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m1));

        assertTrue(m1.hashCode() == m2.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Matrix3 m1 = Matrix3f.identity();
        Matrix3 m2 = Matrix3f.identity();
        Matrix3 m3 = Matrix3f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m2.equals(m3));
        assertTrue(m1.equals(m3));

        assertTrue(m1.hashCode() == m2.hashCode());
        assertTrue(m2.hashCode() == m3.hashCode());
        assertTrue(m1.hashCode() == m3.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
        assertFalse(m3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Matrix3 m1 = Matrix3f.identity();
        Matrix3 m2 = Matrix3f.identity();

        assertTrue(m1.equals(m2));
        assertTrue(m1.equals(m2));

        assertTrue(m1.hashCode() == m2.hashCode());
        assertTrue(m1.hashCode() == m2.hashCode());

        m1 = Matrix3f.zero();
        assertFalse(m1.equals(m2));
        assertFalse(m1.hashCode() == m2.hashCode());

        assertFalse(m1.equals(null));
        assertFalse(m2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(Matrix3f.zero().equals(null));
        assertFalse(Matrix3f.identity().equals(null));
    }

    @Test
    public void testToString() {
        Matrix3 m = new Matrix3f(0, 1, 2, 3, 4, 5, 6, 7, 8);

        // TODO: Improve formatting
        String format = Matrix3f.class.getSimpleName();
        format += " = [  0.00000 |   1.00000 |   2.00000]\n";
        format += "           [  3.00000 |   4.00000 |   5.00000]\n";
        format += "           [  6.00000 |   7.00000 |   8.00000]";

        assertEquals(format, m.toString());
    }

    // ========================================================================
    // Helper implementation methods
    // ========================================================================

    private static Matrix3 multiply(Matrix3 a, Matrix3 b) {
        // @formatter:off
        return new Matrix3f(
            a.value(0, 0) * b.value(0, 0) + a.value(0, 1) * b.value(1, 0) + a.value(0, 2) * b.value(2, 0),
            a.value(0, 0) * b.value(0, 1) + a.value(0, 1) * b.value(1, 1) + a.value(0, 2) * b.value(2, 1),
            a.value(0, 0) * b.value(0, 2) + a.value(0, 1) * b.value(1, 2) + a.value(0, 2) * b.value(2, 2),
            a.value(1, 0) * b.value(0, 0) + a.value(1, 1) * b.value(1, 0) + a.value(1, 2) * b.value(2, 0),
            a.value(1, 0) * b.value(0, 1) + a.value(1, 1) * b.value(1, 1) + a.value(1, 2) * b.value(2, 1),
            a.value(1, 0) * b.value(0, 2) + a.value(1, 1) * b.value(1, 2) + a.value(1, 2) * b.value(2, 2),
            a.value(2, 0) * b.value(0, 0) + a.value(2, 1) * b.value(1, 0) + a.value(2, 2) * b.value(2, 0),
            a.value(2, 0) * b.value(0, 1) + a.value(2, 1) * b.value(1, 1) + a.value(2, 2) * b.value(2, 1),
            a.value(2, 0) * b.value(0, 2) + a.value(2, 1) * b.value(1, 2) + a.value(2, 2) * b.value(2, 2)
        );
        // @formatter:on
    }

    private static Matrix3 inverse(Matrix3 m) {
        Vector3 a = m.column(0);
        Vector3 b = m.column(1);
        Vector3 c = m.column(2);

        Vector3 r0 = b.cross(c);
        Vector3 r1 = c.cross(a);
        Vector3 r2 = a.cross(b);

        float invDet = 1f / r2.dot(c);

        r0 = r0.mult(invDet);
        r1 = r1.mult(invDet);
        r2 = r2.mult(invDet);

        // @formatter:off
        return new Matrix3f(
            r0.x(), r0.y(), r0.z(),
            r1.x(), r1.y(), r1.z(),
            r2.x(), r2.y(), r2.z()
        );
        // @formatter:on
    }

    public static Matrix3 rotate(Matrix3 m, Angle x, Angle y, Angle z) {
        float radx = x.valueRadians();
        float rady = y.valueRadians();
        float radz = z.valueRadians();
        float sinx = (float) Math.sin(radx);
        float siny = (float) Math.sin(rady);
        float sinz = (float) Math.sin(radz);
        float cosx = (float) Math.cos(radx);
        float cosy = (float) Math.cos(rady);
        float cosz = (float) Math.cos(radz);

        // @formatter:off
        Matrix3 rx = new Matrix3f(
            1f, 0f  ,  0f  ,
            0f, cosx, -sinx,
            0f, sinx,  cosx
        );
        Matrix3 ry = new Matrix3f(
             cosy, 0f, siny,
              0f , 1f,  0f ,
            -siny, 0f, cosy
        );
        Matrix3 rz = new Matrix3f(
            cosz, -sinz, 0f,
            sinz,  cosz, 0f,
             0f ,   0f , 1f
        );
        // @formatter:on
        return m.mult(rx).mult(ry).mult(rz);
    }

    public static Matrix3 rotate(Matrix3 m, Angle angle, Vector3 axis) {
        Vector3 nv = axis.normalize();
        float cos = (float) Math.cos(angle.valueRadians());
        float sin = (float) Math.sin(angle.valueRadians());
        float tmp = 1.0f - cos;

        float x = nv.x();
        float y = nv.y();
        float z = nv.z();

        float xx = x * x;
        float yy = y * y;
        float zz = z * z;
        float xy = x * y;
        float xz = x * z;
        float yz = y * z;

        // @formatter:off
        Matrix3 r = new Matrix3f(
            (  tmp * xx + cos  ), (tmp * xy - z * sin), (tmp * xz + y * sin),
            (tmp * xy + z * sin), (  tmp * yy + cos  ), (tmp * yz - x * sin),
            (tmp * xz - y * sin), (tmp * yz + x * sin), (  tmp * zz + cos  )
        );
        // @formatter:on
        return m.mult(r);
    }

}
