# How to Contribute

Please take a few moments to understand this document before submitting
pull requests; it explains the basic guidelines you should follow. It's
assumed that:

* [Git](https://git-scm.com/) is your version control system,
* you have Git locally installed and properly configured[^1], and
* you already have a GitLab account properly setup;

You should also take a few moments to [understand the branches](BRANCHES.md)
tracking the source tree and how they're used.


[^1]: [Pro Git](https://git-scm.com/book) is an online reference book
that's freely available and easy to follow. If you're a Windows user,
be sure to  use the **_Checkout Windows-style, commit Unix-style_** option
during installation; if it's already installed, then run the appropriate
`git config` command.


## 0. Find/Suggest Something of Interest

Start with the [issue tracker](https://gitlab.com/ghost-in-the-zsh/rage/issues).

Find something that's needed and/or seems interesting to you. If you don't find
it, propose it. Whatever the case, discuss the focus and scope of any proposed
changes and/or test cases before putting in the effort.

It's important to keep your changes small and limited in scope when getting
started. As you become familiar and earn some trust, the chances of larger
and more significant changes being accepted will increase.

Any code that gets merged is code that needs to be maintaned, so please be
patient/understanding and be ready for your changes to get reviewed, and
maybe even returned to you a few times, before they get merged.


### Issues vs. Feature Requests

Be sure to:

* verify that your issue has not already been reported,
* pick the appropriate template and fill out as much information as you can, and
* clearly describe the issue or feature of interest.

You're encouraged to discuss things before getting started. A bias towards
fixing issues before adding new features is generally desired.


## 1. Fork the Repository

* Fork the project in GitLab to get the current source tree under your account.
* Get a local clone of your fork.


## 2. Make Your Changes

Do not work in the `master` branch directly. Rather, create a topic branch
and do your work there. Please rely on the guidelines[^2] below:


[^2]: These guidelines were adapted from those used by the Git developers.


### Decide What to Base Your Work On

* In general, you should always base your branch on the oldest branch
that your change is relevant to. A bugfix should generally be based off
`maint`, but if the issue is not present in `maint`, then the changes
should be based on `master`. For bugs that are not present in `master`,
find the topic branch that introduces the problem and base your work off
the tip of that topic branch.

* New features should be based on `master`. However, if a feature depends
on a topic in some other branch, then you should base your work on the
tip of that topic.

* Fixes and/or improvements to topics not yet in `master` should be based
on the tip of said topic. It's ok to add a note to squash minor corrections
to the topic if the topic has not yet been merged to `next`.

* If a feature depends on several topics not in `master`, you should base
your work on `next`. (See the [branches](BRANCHES.md) document to
understand what this implies.) You may need to wait until the dependent
topics are merged to `master` and rebase your work before the final merge.
This case is expected to be the exception and should be avoided when
possible.

To find the tip of a topic branch, run `git log --first-parent master..pu`
and look for the merge commit. The second parent of this commit is the
tip of the topic branch.


### Make Separate Commits for Logically Separate Changes

* Unless your patch is really trivial, you should make commits with
complete and meaningful messages and generate a series of patches from
your repository.

* Explain the changes in sufficient detail for people to judge if it's
a good thing to do without having to read the code to determine how well
the code does what the explanation is promising to do.

* If your description starts get too long, then that's probably a sign
that you should split you commit into a set of smaller commits. The best
patches are those that plainly describe the things that help reviewers
check it and future maintainers to understand the code. Descriptions that
summarize the point in the subject well, describe the motivation for the
change, the approach taken by the change, and (if relevant) how this
differs substantially from the prior version, are all good things to have.


### Describe Your Changes Well

The first line of a commit should be a short summary of the changes (50
characters is the soft limit), similar to an email's subject. It's also
a good idea to prefix the first line with "Area: ", where the area is a
filename or identifier for the general section being modified. For
example:

* Doc: Add new guidelines and branching strategy
* Scene: Improve efficiency of scene graph updates
* GenericSceneNode.java: Fix incorrect negation
* README.md: Correct typos

In short, your commits should [follow proper formatting](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).

If in doubt on which identifier to use, run `git log --oneline --no-merges`
on the file(s) you are modifying to see the current conventions. Keep in
mind that prior to this update, there's no particular convention in place,
so it's ok to ask/discuss.


#### Meaningful Message

The body should provide a meaningful commit message. It should:

* explain the problem the change tries to solve, i.e. what is wrong
  with the current code without the change.

* justify the way the change solves the problem, i.e. why the
  result with the change is better.

* include alternate solutions considered but discarded, if any.


### Reference Commits

If you want to reference a previous commit in the history of a stable
branch, use the format "abbreviated sha1 (subject, date)", with the
subject enclosed in a pair of double-quotes, as shown below:

    Commit f86a374 ("README.md: Fix a typo", 2018-07-07)
    noticed that ...

The "Copy commit summary" command of `gitk` can be used to obtain this
format. You can also use this invocation of `git show`:

    git show -s --date=short --pretty='format:%h ("%s", %ad)' <commit>


## 3. Test Your Changes

When adding a new feature, make sure that you have new tests to show the
feature works as intended, and also to show the feature does not work in
a way that's not intended. After any code change, make sure that the _all_
the tests in the suite are passing.

Do not forget to update the documentation to describe the updated behavior
and make sure that the resulting documentation set formats well.


## 4. Style-Check Your Changes

Consistency is important. Follow the conventions already present in the
codebase and especially within the immediate vincinity of where the
changes are being made. If you're using the [Eclipse](https://eclipse.org/)
IDE, then you can import and use the following files as a convenience:

* `rml-code-formatter.xml`
* `rml-cleanup-actions.xml`

They should work "as is". If you can't use these files on your favorite
editor/IDE, you're still responsible for keeping the style consistent.
Those that have to read (and maintain!) your code will be grateful.


### (Reasonable) Exceptions to the Rule

Formatters sometimes mess things up and should be disabled in those
sections to improve readability. For example, compare:

    public static Matrix4 createScalingFrom(float sx, float sy, float sz) {
        float[] scale = new float[] { sx, 0f, 0f, 0f, 0f, sy, 0f, 0f, 0f, 0f, sz, 0f, 0f, 0f, 0f, 1f };
        return new Matrix4f(scale);
    }

with:

    public static Matrix4 createScalingFrom(float sx, float sy, float sz) {
        // @formatter:off
        float[] scale = new float[] {
            sx, 0f, 0f, 0f,
            0f, sy, 0f, 0f,
            0f, 0f, sz, 0f,
            0f, 0f, 0f, 1f
        };
        // @formatter:on
        return new Matrix4f(scale);
    }

You should disable the formatter only in cases where auto-formatting
ends up making the code more difficult to follow. In short, stay
consistent with the surrounding code, be reasonable, and use common
sense.


### Use Wild-Card Imports

Organize your imports in order to avoid noise. A single `java.util.*;` is
less noisy than dozens of `java.util.ClassNameHere;` lines for each
package. (If they were really *that* important, smart editors wouldn't be
auto-hiding them all the time.)

You can configure [Eclipse](https://eclipse.org/) to automatically use
wild-card imports by following these steps:

1. Go to `Window → Preferences → Java → Code Style → Organize Imports`
2. Set option `Number of imports needed for .* (e.g. org.eclipse.*):`
to `1`.
3. Set option `Number of static imports needed for .* (e.g. java.lang.Math.*):`
to `1`.


## 5. Respond to Review Comments

Your patch will almost certainly get comments from reviewers on ways in
which the patch can be improved. You must respond to those comments;
ignoring reviewers is a good way to get ignored in return. Review comments
or questions that do not lead to a code change should almost certainly
bring about a comment or changelog entry so that the next reviewer better
understands what is going on.

Be sure to tell the reviewers what changes you are making and to thank
them for their time. Code review is a tiring and time-consuming process,
and reviewers sometimes get grumpy -especially when they're running low
on caffeine and hot pockets. Even in that case, though, respond politely
and address the problems they have pointed out.

Understand that reviewing a piece of code is not a review of you as a
person or your character, so don't take it personally or react
defensively. It's just code and we all want it to be the best it can be.


## 6. Be Patient

After you have submitted your change, be patient and wait. Life is
complicated and everyone's busy; I may not be able to get to your patch
right away. Please, allow a reasonable time frame (e.g. at least a week)
before re-submitting or trying to ping me.


## 7. Sign Your Work - The Developer's Certificate of Origin

To track authorship of the work, you _must_ include a "sign-off" line in
your commit messages.

The sign-off is a simple line at the end of the explanation for the patch,
which certifies that you wrote it or otherwise have the right to pass it
on as a free/libre software patch. The rules are pretty simple: if you can
certify the following[^3]:

> #### Developer's Certificate of Origin 1.1
>
> By making a contribution to this project, I certify that:
>
> (a) The contribution was created in whole or in part by me and I
>     have the right to submit it under the open source license
>     indicated in the file; or
>
> (b) The contribution is based upon previous work that, to the best
>     of my knowledge, is covered under an appropriate open source
>     license and I have the right under that license to submit that
>     work with modifications, whether created in whole or in part
>     by me, under the same open source license (unless I am
>     permitted to submit under a different license), as indicated
>     in the file; or
>
> (c) The contribution was provided directly to me by some other
>     person who certified (a), (b) or (c) and I have not modified
>     it.
>
> (d) I understand and agree that this project and the contribution
>     are public and that a record of the contribution (including all
>     personal information I submit with it, including my sign-off) is
>     maintained indefinitely and may be redistributed consistent with
>     this project or the open source license(s) involved.

then you just add a line saying:

    Signed-off-by: Random J Developer <random.developer@example.org>

using your real name. (No pseudonyms or anonymous contributions will be
accepted.) Adding the `-s` option to `git commit` will include the
sign-off line automatically.

[^3]: https://developercertificate.org/
