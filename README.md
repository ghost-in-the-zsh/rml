# RML - RML Math Library

RML is a pure Java-based implementation for a linear algebra library for computer graphics and used in [RAGE](https://gitlab.com/ghost-in-the-zsh/rage)[^1][^2]. RML classes are (mostly) analogous to the vector and matrix types found in the OpenGL Shading Language (GLSL), used by OpenGL and Vulkan, but is not limited to it. These include:

| GLSL   | RML          | Description                       |
| :---   | :---         | :----------                       |
| `----` | `Angle`      | Angle to measure rotations        |
| `vec3` | `Vector3`    | 3D column vector                  |
| `vec4` | `Vector4`    | 4D column vector                  |
| `mat3` | `Matrix3`    | Square 3x3 column-major matrix    |
| `mat4` | `Matrix4`    | Square 4x4 column-major matrix    |
| `----` | `Quaternion` | 4D object with an angle and axis  |

Concrete single-precision floating point implementations for the interfaces specified above are included. You can learn more about these in the included [tutorial](docs/tutorial/00-introduction.md).

[^1]: This library can be used independently of the engine.
[^2]: The name could also be interpreted as **R**AGE **M**ath **L**ibrary.


## Design Goals and Trade-offs

This library was designed with some of the following ideas in mind:

1. **Thread-safety:** objects are immutable and don't require synchronization, locking, etc.
2. **Readability:** static factory methods sometimes allow more expressive code than constructors.
3. **Flexibility:** method invocations can be chained.
4. **Room for Optimization:** implementations can use caching to improve performance without having to update clients.
    1. For example, `Vector3f.unitX()` and similar calls always return the same instance.

Nothing is ever truly free, however. These benefits come with some trade-offs, including:

1. **Memory footprint:** having immutable objects means that expressions must result in new instances being created.
    1. That is, method calls must be treated as _expressions_ rather than _statements_.
2. **GC overhead:** complex expressions create temporary results that may get discarded quickly, adding work for the garbage collector.

I think these trade-offs are reasonable, especially when we take the non-professional context, target student audience, and pedagogic purpose of this system into account. That said, it's still quite fast and can handle itself pretty well.


## Minimum Requirements

* GNU+Linux, Windows, or any other Java-supported operating system
* Java (JDK/JRE) 1.8 or later
* [TestNG](http://testng.org)


## Documentation

You can start by looking at the included [tutorial](docs/tutorial/00-introduction.md), which may even help refresh some concepts for you.

You can also look at the unit tests under the `tests` source folder if you just want to start reading code directly, use your IDE to generate the javadocs, or the `doxygen .doxygen.conf` command to generate the [Doxygen](http://www.stack.nl/~dimitri/doxygen/)-based documentation. You have alternatives.


## Contributions

See the [contribution guide](CONTRIBUTING.md) for details.


## Licensing

This project is [Free Software](https://www.gnu.org/philosophy/free-sw.html) because it respects and protects your freedoms. For a quick summary of what your rights and responsibilities are, you should see [this article](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)). For the full text, you can see the [license](LICENSE.md).
