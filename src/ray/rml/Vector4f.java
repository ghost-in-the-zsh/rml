/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * An immutable single-precision floating point 4-component column
 * {@link Vector4 vector}. This {@link Vector4 vector} is useful for
 * transformations where <i>homogeneous coordinates</i> are needed.
 * <p>
 * This means the input sequence <code>(1, 2, 3, 4);</code> produces the
 * following layout:
 * <table summary="" style="border: 1px solid black;">
 * <tr style="border: 1px solid black; padding: 5px;">
 * <td style="border: 1px solid black; padding: 5px;">1</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">2</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">3</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">4</td>
 * </tr>
 * </table>
 *
 * @author Raymond L. Rivera
 *
 */
public final class Vector4f implements Vector4 {

    private final static Vector4 zero  = new Vector4f(0f, 0f, 0f, 0f);
    private final static Vector4 unitX = new Vector4f(1f, 0f, 0f, 0f);
    private final static Vector4 unitY = new Vector4f(0f, 1f, 0f, 0f);
    private final static Vector4 unitZ = new Vector4f(0f, 0f, 1f, 0f);

    private final float          x;
    private final float          y;
    private final float          z;
    private final float          w;

    /**
     * Creates a new {@link Vector4 vector} with the specified values, i.e.
     * <code>(x, y, z, w)</code>.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new {@link Vector4 vector} of the form
     *         <code>(x, y, z, w)</code>.
     */
    public Vector4f(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    /**
     * Creates a new <i>directional</i> {@link Vector4 vector} with the
     * specified values, i.e. <code>(x, y, z, 0)</code>.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @return A new {@link Vector4 vector} of the form
     *         <code>(x, y, z, 0)</code>.
     */
    public Vector4f(float x, float y, float z) {
        this(x, y, z, 0f);
    }

    /**
     * Creates a new {@link Vector4 vector} with the specified values.
     *
     * @param values
     *            An array with the values for the <code>x</code>,
     *            <code>y</code>, <code>z</code>, and (optionally)
     *            <code>w</code> components in the first, second, third, and
     *            fourth positions respectively.
     * @return A new {@link Vector4 vector} of the form
     *         <code>(x, y, z, w)</code>.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 3 elements.
     */
    public Vector4f(final float[] v) {
        this(v[0], v[1], v[2], v.length > 3 ? v[3] : 0f);
    }

    /**
     * Creates a new <i>directional</i> {@link Vector4 vector} from the
     * specified {@link Vector3 vector}, i.e. <code>(v.x, v.y, v.z, 0)</code>.
     *
     * @param v
     *            A {@link Vector3 vector} with values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components.
     * @return A new {@link Vector4 vector} with the specified values.
     */
    public Vector4f(final Vector3 v) {
        this(v, 0f);
    }

    /**
     * Creates a new {@link Vector4 vector} from the specified {@link Vector3
     * vector}, i.e. <code>(v.x, v.y, v.z, w)</code>.
     * <p>
     * This is a convenience for when you need to turn a {@link Vector3} into a
     * <i>positional</i> {@link Vector4} without having to create a temporary
     * {@link Point3} in-between.
     *
     * @param v
     *            A {@link Vector3 vector} with values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new {@link Vector4 vector} with the specified values.
     */
    public Vector4f(final Vector3 v, float w) {
        this(v.x(), v.y(), v.z(), w);
    }

    /**
     * Creates a new <i>positional</i> {@link Vector4 vector} from the specified
     * {@link Point3 point}, i.e. <code>(p.x, p.y, p.z, 1)</code>.
     *
     * @param p
     *            A {@link Point3 point} with values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components.
     * @return A new {@link Vector4 vector} with the specified values.
     */
    public Vector4f(final Point3 p) {
        this(p, 1f);
    }

    /**
     * Creates a new {@link Vector4 vector} from the specified {@link Point3
     * point}, i.e. <code>(p.x, p.y, p.z, w)</code>.
     * <p>
     * This is a convenience for when you need to turn a {@link Point3} into a
     * <i>directional</i> {@link Vector4} without having to create a temporary
     * {@link Vector3} in-between.
     *
     * @param p
     *            A {@link Point3 point} with values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new {@link Vector4 vector} with the specified values.
     */
    public Vector4f(final Point3 p, float w) {
        this(p.x(), p.y(), p.z(), w);
    }

    /**
     * Creates a new {@link Vector4 vector} with <code>x</code>, <code>y</code>,
     * and <code>z</code> components set to zero, i.e.
     * <code>(0, 0, 0, 0)</code>.
     *
     * @return A new zero-length {@link Vector4 vector}.
     */
    public static Vector4 zero() {
        return zero;
    }

    /**
     * Creates a new unit-length {@link Vector4 vector} along the <code>x</code>
     * axis, i.e. <code>(1, 0, 0, 0)</code>.
     *
     * @return A new unit-length {@link Vector4 vector}.
     */
    public static Vector4 unitX() {
        return unitX;
    }

    /**
     * Creates a new unit-length {@link Vector4 vector} along the <code>y</code>
     * axis, i.e. <code>(0, 1, 0, 0)</code>.
     *
     * @return A new unit-length {@link Vector4 vector}.
     */
    public static Vector4 unitY() {
        return unitY;
    }

    /**
     * Creates a new unit-length {@link Vector4 vector} along the <code>z</code>
     * axis, i.e. <code>(0, 0, 1, 0)</code>.
     *
     * @return A new unit-length {@link Vector4 vector}.
     */
    public static Vector4 unitZ() {
        return unitZ;
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new unit-length {@link Vector4 vector} from the specified
     *         values.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(float x, float y, float z, float w) {
        final float sqlen = MathUtil.lengthSquared(x, y, z, w);
        if (FloatUtil.isZero(sqlen))
            throw new ArithmeticException("Cannot normalize zero-length vector");

        final float invLen = MathUtil.invSqrt(sqlen);
        return new Vector4f(x * invLen, y * invLen, z * invLen, w * invLen);
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values, where <code>w = 0</code> implicitly.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @return A new unit-length {@link Vector4 vector} from the specified
     *         values.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(float x, float y, float z) {
        return normalize(x, y, z, 0f);
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values.
     *
     * @param v
     *            An array with the values for the <code>x</code>,
     *            <code>y</code>, <code>z</code>, and (optionally)
     *            <code>w</code> components in the first, second, third, and
     *            fourth positions respectively.
     * @return A new unit-length {@link Vector4 vector} from the specified
     *         values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 3 elements.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(final float[] v) {
        return normalize(v[0], v[1], v[2], v.length > 3 ? v[3] : 0f);
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values, where <code>w = 0</code> implicitly.
     *
     * @param v
     *            A {@link Vector3 vector} with values for the x, y, and z
     *            components.
     * @return A new unit-length {@link Vector4 vector} from the input
     *         {@link Vector3 vector}.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(final Vector3 v) {
        return normalize(v.x(), v.y(), v.z());
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values.
     *
     * @param v
     *            A {@link Vector3 vector} with values for the x, y, and z
     *            components.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new unit-length {@link Vector4 vector} from the input
     *         {@link Vector3 vector}.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(final Vector3 v, float w) {
        return normalize(v.x(), v.y(), v.z(), w);
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values, where <code>w = 1</code> implicitly.
     *
     * @param p
     *            A {@link Point3 point} with values for the x, y, and z
     *            components.
     * @return A new unit-length {@link Vector4 vector} from the input
     *         {@link Point3 point}.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(final Point3 p) {
        return normalize(p.x(), p.y(), p.z(), 1f);
    }

    /**
     * Creates a new normalized {@link Vector4 vector} from the specified
     * values.
     *
     * @param p
     *            A {@link Point3 point} with values for the x, y, and z
     *            components.
     * @param w
     *            The value for the <code>w</code> component.
     * @return A new unit-length {@link Vector4 vector} from the input
     *         {@link Point3 point}.
     * @throws ArithmeticException
     *             If the length of the {@link Vector4 vector} is zero.
     */
    public static Vector4 normalize(final Point3 p, float w) {
        return normalize(p.x(), p.y(), p.z(), w);
    }

    @Override
    public float x() {
        return x;
    }

    @Override
    public float y() {
        return y;
    }

    @Override
    public float z() {
        return z;
    }

    @Override
    public float w() {
        return w;
    }

    @Override
    public Vector4 add(final Vector4 v) {
        return add(v.x(), v.y(), v.z(), v.w());
    }

    @Override
    public Vector4 add(float x, float y, float z) {
        return add(x, y, z, 0f);
    }

    @Override
    public Vector4 add(float x, float y, float z, float w) {
        return new Vector4f(this.x + x, this.y + y, this.z + z, this.w + w);
    }

    @Override
    public Vector4 sub(final Vector4 v) {
        return sub(v.x(), v.y(), v.z(), v.w());
    }

    @Override
    public Vector4 sub(float x, float y, float z) {
        return sub(x, y, z, 0f);
    }

    @Override
    public Vector4 sub(float x, float y, float z, float w) {
        return new Vector4f(this.x - x, this.y - y, this.z - z, this.w - w);
    }

    @Override
    public Vector4 mult(float s) {
        return new Vector4f(x * s, y * s, z * s, w * s);
    }

    @Override
    public Vector4 div(float s) {
        s = 1f / s;
        return new Vector4f(x * s, y * s, z * s, w * s);
    }

    @Override
    public float dot(final Vector4 v) {
        return x * v.x() + y * v.y() + z * v.z() + w * v.w();
    }

    @Override
    public Angle angleTo(Vector4 v) {
        return new Radianf(MathUtil.acos(dot(v)));
    }

    @Override
    public Vector4 normalize() {
        return normalize(x, y, z, w);
    }

    @Override
    public Vector4 rotate(Angle angle, Vector4 axis) {
        return Matrix4f.rotation(angle, axis).mult(this);
    }

    @Override
    public float length() {
        return MathUtil.length(x, y, z, w);
    }

    @Override
    public float lengthSquared() {
        return MathUtil.lengthSquared(x, y, z, w);
    }

    @Override
    public boolean isZeroLength() {
        return FloatUtil.isZero(lengthSquared());
    }

    /**
     * The values are in the following order: <code>(x, y, z, w)</code>.
     *
     * @see Bufferable#toFloatArray()
     */
    @Override
    public float[] toFloatArray() {
        return new float[] { x, y, z, w };
    }

    @Override
    public Vector4 lerp(Vector4 end, float t) {
        // t in [0, 1]
        t = MathUtil.clamp(t, 0f, 1f);
        float[] v = MathUtil.lerp(toFloatArray(), end.toFloatArray(), t);
        return new Vector4f(v);
    }

    @Override
    public Vector4 negate() {
        return new Vector4f(-x, -y, -z, -w);
    }

    @Override
    public int compareTo(final Vector4 v) {
        return FloatUtil.compare(lengthSquared(), v.lengthSquared());
    }

    @Override
    public Vector3 toVector3() {
        return new Vector3f(x, y, z);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 3;
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        result = prime * result + Float.floatToIntBits(z);
        result = prime * result + Float.floatToIntBits(w);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Vector4))
            return false;

        Vector4 other = (Vector4) obj;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x()))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y()))
            return false;
        if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z()))
            return false;
        if (Float.floatToIntBits(w) != Float.floatToIntBits(other.w()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return Vector4f.class.getSimpleName() + "(" + x + ", " + y + ", " + z + ", " + w + ")";
    }

}
