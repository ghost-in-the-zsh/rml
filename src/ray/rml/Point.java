/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * A <i>point</i> is an object that represents a position in space.
 *
 * @author Raymond L. Rivera
 *
 * @param <P>
 *            The type representing a point.
 */
interface Point<P extends Point<P>> extends Addable<P>, Subtractable<P>, MultiplicableScalar<P>, DivisibleScalar<P>,
               Comparable<P>, Bufferable, Negatable<P>, LinearlyInterpolable<P> {

    /**
     * Calculates the distance between <code>this</code> and the specified
     * {@link Point point}.
     *
     * @param p
     *            The {@link Point point} to which <code>this</code> measures
     *            its distance.
     * @return The distance between <code>this</code> and the given {@link Point
     *         point}.
     * @throws NullPointerException
     *             If the argument is <code>null</code>.
     */
    float distanceTo(final P p);

}
