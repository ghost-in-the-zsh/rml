/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * An immutable single-precision floating point 3-component column
 * {@link Vector3 vector}.
 * <p>
 * This means the input sequence <code>(1, 2, 3);</code> produces the following
 * layout:
 * <table summary="" style="border: 1px solid black;">
 * <tr style="border: 1px solid black; padding: 5px;">
 * <td style="border: 1px solid black; padding: 5px;">1</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">2</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">3</td>
 * </tr>
 * </table>
 *
 * @author Raymond L. Rivera
 *
 */
public final class Vector3f implements Vector3 {

    private static final Vector3 zero  = new Vector3f(0f, 0f, 0f);
    private final static Vector3 unitX = new Vector3f(1f, 0f, 0f);
    private final static Vector3 unitY = new Vector3f(0f, 1f, 0f);
    private final static Vector3 unitZ = new Vector3f(0f, 0f, 1f);

    private final float          x;
    private final float          y;
    private final float          z;

    /**
     * Creates a new {@link Vector3 vector} with the specified values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @return A new {@link Vector3 vector} with the specified values.
     */
    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new {@link Vector3 vector} with the specified values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @return A new {@link Vector3 vector} with the specified values. The
     *         unspecified <code>z</code> component defaults to zero.
     */
    public Vector3f(float x, float y) {
        this(x, y, 0f);
    }

    /**
     * Creates a new {@link Vector3 vector} with the specified values.
     *
     * @param v
     *            An array with the values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components in the first,
     *            second, and third positions respectively.
     * @return A new {@link Vector3 vector} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 2 elements.
     */
    public Vector3f(final float[] v) {
        this(v[0], v[1], v.length > 2 ? v[2] : 0f);
    }

    /**
     * Creates a new {@link Vector3 vector} from the given {@link Point3 point}.
     *
     * @param p
     *            The {@link Point3 point}.
     */
    public Vector3f(final Point3 p) {
        this(p.x(), p.y(), p.z());
    }

    /**
     * Creates a new {@link Vector3 vector} with all components set to zero.
     *
     * @return A new zero-length {@link Vector3 vector}.
     */
    public static Vector3 zero() {
        return zero;
    }

    /**
     * Creates a new unit-length {@link Vector3 vector} along the <code>x</code>
     * axis.
     *
     * @return A new unit-length {@link Vector3 vector}.
     */
    public static Vector3 unitX() {
        return unitX;
    }

    /**
     * Creates a new unit-length {@link Vector3 vector} along the <code>y</code>
     * axis.
     *
     * @return A new unit-length {@link Vector3 vector}.
     */
    public static Vector3 unitY() {
        return unitY;
    }

    /**
     * Creates a new unit-length {@link Vector3 vector} along the <code>z</code>
     * axis.
     *
     * @return A new unit-length {@link Vector3 vector}.
     */
    public static Vector3 unitZ() {
        return unitZ;
    }

    /**
     * Creates a new normalized {@link Vector3 vector} from the specified
     * values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @return A new normalized {@link Vector3 vector} from the specified values
     *         and a length of 1.
     * @throws ArithmeticException
     *             If the length of the {@link Vector3 vector} is zero.
     */
    public static Vector3 normalize(float x, float y, float z) {
        final float sqlen = MathUtil.lengthSquared(x, y, z);
        if (FloatUtil.isZero(sqlen))
            throw new ArithmeticException("Cannot normalize zero-length vector");

        final float oneOverLen = MathUtil.invSqrt(sqlen);
        return new Vector3f(x * oneOverLen, y * oneOverLen, z * oneOverLen);
    }

    /**
     * Creates a new normalized {@link Vector3 vector} from the specified
     * values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @return A new normalized {@link Vector3 vector} from the specified values
     *         and a length of 1. The unspecified <code>z</code> component
     *         defaults to zero.
     * @throws ArithmeticException
     *             If the length of the {@link Vector3 vector} is zero.
     */
    public static Vector3 normalize(float x, float y) {
        return normalize(x, y, 0f);
    }

    /**
     * Creates a new normalized {@link Vector3 vector} from the specified
     * values.
     *
     * @param v
     *            An array with the values for the <code>x</code>,
     *            <code>y</code>, and <code>z</code> components in the first,
     *            second, and third positions respectively.
     * @return A new normalized {@link Vector3 vector} from the specified values
     *         and a length of 1.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 2 elements.
     * @throws ArithmeticException
     *             If the length of the {@link Vector3 vector} is zero.
     */
    public static Vector3 normalize(final float[] v) {
        return normalize(v[0], v[1], v.length > 2 ? v[2] : 0f);
    }

    /**
     * Creates a new normalized {@link Vector3 vector} from the specified
     * {@link Point3 point} values.
     *
     * @param p
     *            The {@link Point3 point}.
     *
     * @return A new normalized {@link Vector3 vector} from the specified values
     *         and a length of 1.
     * @throws ArithmeticException
     *             If the length of the {@link Vector3 vector} is zero.
     */
    public static Vector3 normalize(final Point3 p) {
        return normalize(p.x(), p.y(), p.z());
    }

    @Override
    public float x() {
        return x;
    }

    @Override
    public float y() {
        return y;
    }

    @Override
    public float z() {
        return z;
    }

    @Override
    public Vector3 add(final Vector3 v) {
        return new Vector3f(x + v.x(), y + v.y(), z + v.z());
    }

    @Override
    public Vector3 add(float x, float y, float z) {
        return new Vector3f(this.x + x, this.y + y, this.z + z);
    }

    @Override
    public Vector3 sub(final Vector3 v) {
        return new Vector3f(x - v.x(), y - v.y(), z - v.z());
    }

    @Override
    public Vector3 sub(float x, float y, float z) {
        return new Vector3f(this.x - x, this.y - y, this.z - z);
    }

    @Override
    public Vector3 mult(float s) {
        return new Vector3f(x * s, y * s, z * s);
    }

    @Override
    public Vector3 div(float s) {
        s = 1.0f / s;
        return new Vector3f(x * s, y * s, z * s);
    }

    @Override
    public float dot(final Vector3 v) {
        return x * v.x() + y * v.y() + z * v.z();
    }

    @Override
    public Angle angleTo(Vector3 v) {
        return new Radianf(MathUtil.acos(dot(v)));
    }

    @Override
    public Vector3 cross(final Vector3 v) {
        return new Vector3f(y * v.z() - z * v.y(), z * v.x() - x * v.z(), x * v.y() - y * v.x());
    }

    @Override
    public Vector3 normalize() {
        return normalize(x, y, z);
    }

    @Override
    public Vector3 rotate(Angle angle, Vector3 axis) {
        return Matrix3f.rotation(angle, axis).mult(this);
    }

    @Override
    public float length() {
        return MathUtil.length(x, y, z);
    }

    @Override
    public float lengthSquared() {
        return MathUtil.lengthSquared(x, y, z);
    }

    @Override
    public boolean isZeroLength() {
        return FloatUtil.isZero(lengthSquared(), FloatUtil.EPSILON);
    }

    /**
     * The values are in the following order: <code>[x, y, z]</code>.
     *
     * @see Bufferable#toFloatArray()
     */
    @Override
    public float[] toFloatArray() {
        return new float[] { x, y, z };
    }

    @Override
    public Vector3 lerp(Vector3 end, float t) {
        // t in [0, 1]
        t = MathUtil.clamp(t, 0f, 1f);
        float[] v = MathUtil.lerp(toFloatArray(), end.toFloatArray(), t);
        return new Vector3f(v);
    }

    @Override
    public Vector3 negate() {
        return new Vector3f(-x, -y, -z);
    }

    @Override
    public int compareTo(final Vector3 v) {
        return FloatUtil.compare(lengthSquared(), v.lengthSquared());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 3;
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        result = prime * result + Float.floatToIntBits(z);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Vector3))
            return false;

        Vector3 other = (Vector3) obj;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x()))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y()))
            return false;
        if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return Vector3f.class.getSimpleName() + "(" + x + ", " + y + ", " + z + ")";
    }

}
