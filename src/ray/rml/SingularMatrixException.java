/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * Exception thrown when an attempt is made to invert a <i>singular</i>
 * {@link Matrix matrix}.
 * <p>
 * A singular matrix is non-invertible because its determinant, which must be
 * used in divisions, is zero.
 *
 * @author Raymond L. Rivera
 *
 */
public class SingularMatrixException extends ArithmeticException {

    private static final long serialVersionUID = 1L;

    public SingularMatrixException(String msg) {
        super(msg);
    }

}
