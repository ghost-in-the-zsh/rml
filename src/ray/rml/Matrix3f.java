/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * An immutable single-precision floating point column-major 3x3 square
 * {@link Matrix3 matrix}.
 * <p>
 * Constructors and factory method arguments still get arranged in a
 * WYSIWYG-style format. This means the following input sequence in a
 * constructor or factory method:
 *
 * <pre>
 * (1, 2, 3,
 *  4, 5, 6,
 *  7, 8, 9);
 * </pre>
 *
 * will produce the following matrix layout:
 * <p>
 * <table summary="" style="border: 1px solid black;">
 * <tr style="border: 1px solid black; padding: 5px;">
 * <td style="border: 1px solid black; padding: 5px;">1</td>
 * <td style="border: 1px solid black; padding: 5px;">2</td>
 * <td style="border: 1px solid black; padding: 5px;">3</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">4</td>
 * <td style="border: 1px solid black; padding: 5px;">5</td>
 * <td style="border: 1px solid black; padding: 5px;">6</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 5px;">7</td>
 * <td style="border: 1px solid black; padding: 5px;">8</td>
 * <td style="border: 1px solid black; padding: 5px;">9</td>
 * </tr>
 * </table>
 * <p>
 * Array-based inputs must be in <i>column</i>-major order and contain the
 * minimum number of elements needed to completely fill up the matrix. Extra
 * elements are automatically ignored.
 *
 * @author Raymond L. Rivera
 *
 */
public final class Matrix3f implements Matrix3 {

    private static final int      DIMENSIONS = 3;

    private static final Matrix3f zero       = new Matrix3f(0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f);
    private static final Matrix3f identity   = new Matrix3f(1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f);

    private final float[][]       matrix     = new float[DIMENSIONS][DIMENSIONS];

    /**
     * Creates a new {@link Matrix3 matrix} from the specified values.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     */
    // @formatter:off
    public Matrix3f(float m00, float m01, float m02,
                    float m10, float m11, float m12,
                    float m20, float m21, float m22) {
    // @formatter:on
        matrix[0][0] = m00;
        matrix[0][1] = m10;
        matrix[0][2] = m20;
        matrix[1][0] = m01;
        matrix[1][1] = m11;
        matrix[1][2] = m21;
        matrix[2][0] = m02;
        matrix[2][1] = m12;
        matrix[2][2] = m22;
    }

    /**
     * Creates a new {@link Matrix3 matrix} from the specified
     * column-{@link Vector3 vectors}.
     *
     * @param c0
     *            A {@link Vector3 vector} with values for the first column.
     * @param c1
     *            A {@link Vector3 vector} with values for the second column.
     * @param c2
     *            A {@link Vector3 vector} with values for the third column.
     * @return A new {@link Matrix3 matrix} with the specified values.
     */
    public Matrix3f(final Vector3 c0, final Vector3 c1, final Vector3 c2) {
        // @formatter:off
        this(
            c0.x(), c1.x(), c2.x(),
            c0.y(), c1.y(), c2.y(),
            c0.z(), c1.z(), c2.z()
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix3 matrix} with the specified values.
     *
     * @param m
     *            An array of at least 9 values, in column-major order.
     * @return A new {@link Matrix3 matrix} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 9 elements.
     */
    public Matrix3f(final float[] m) {
        // @formatter:off
        this(
            m[0], m[3], m[6],
            m[1], m[4], m[7],
            m[2], m[5], m[8]
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix3 matrix} with the specified values.
     *
     * @param m
     *            A 3x3 array of values, in column-major order.
     * @return A new {@link Matrix3 matrix} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 3 elements per dimension.
     */
    public Matrix3f(final float[][] m) {
        // @formatter:off
        this(
            m[0][0], m[1][0], m[2][0],
            m[0][1], m[1][1], m[2][1],
            m[0][2], m[1][2], m[2][2]
        );
        // @formatter:on
    }

    /**
     * Creates a new rotation {@link Matrix3 matrix} from the given
     * {@link Quaternion quaternion}.
     * <p>
     * A {@link Quaternion quaternion} <code>q = (w, [x, y, z])</code>
     * represents the following rotation {@link Matrix3 matrix}:
     *
     * <pre>
     *     +----------------+----------------+----------------+
     *     | 1 - 2(y² + z²) |   2(xy - wz)   |   2(wy + xz)   |
     * M = |   2(xy + wz)   | 1 - 2(x² + z²) |   2(yz - wx)   |
     *     |   2(xz - wy)   |   2(yz + wx)   | 1 - 2(x² + y²) |
     *     +----------------+----------------+----------------+
     * </pre>
     *
     * @param q
     *            The {@link Quaternion quaternion}.
     * @returns A new rotation {@link Matrix3 matrix}.
     */
    public Matrix3f(final Quaternion q) {
        final float w = q.w();
        final float x = q.x();
        final float y = q.y();
        final float z = q.z();

        final float x2 = x * x;
        final float y2 = y * y;
        final float z2 = z * z;
        final float wx = w * x;
        final float wy = w * y;
        final float wz = w * z;
        final float xy = x * y;
        final float xz = x * z;
        final float yz = y * z;

        matrix[0][0] = 1f - 2f * (y2 + z2);
        matrix[0][1] = 2f * (xy + wz);
        matrix[0][2] = 2f * (xz - wy);

        matrix[1][0] = 2f * (xy - wz);
        matrix[1][1] = 1f - 2f * (x2 + z2);
        matrix[1][2] = 2f * (yz + wx);

        matrix[2][0] = 2f * (wy + xz);
        matrix[2][1] = 2f * (yz - wx);
        matrix[2][2] = 1f - 2f * (x2 + y2);
    }

    /**
     * Creates a new {@link Matrix3 matrix} with all values set to zero.
     *
     * @return A new zero-value {@link Matrix3 matrix}.
     */
    public static Matrix3 zero() {
        return zero;
    }

    /**
     * Creates a new identity {@link Matrix3 matrix}.
     * <p>
     * The identity {@link Matrix3 matrix} has values along the diagonal set to
     * one and all others set to zero.
     *
     * @return A new identity {@link Matrix3 matrix}.
     */
    public static Matrix3 identity() {
        return identity;
    }

    /**
     * Creates a new inverse {@link Matrix3 matrix} from the specified values,
     * if possible.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     * @return A new inverse {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws ArithmeticException
     *             If the values represent a singular {@link Matrix3 matrix}. A
     *             singular {@link Matrix3 matrix} is non-invertible because its
     *             determinant is zero.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 9 elements.
     */
    // @formatter:off
    public static Matrix3 inverse(float m00, float m01, float m02,
                                  float m10, float m11, float m12,
                                  float m20, float m21, float m22) {
    // @formatter:on
        Vector3 a = new Vector3f(m00, m10, m20);
        Vector3 b = new Vector3f(m01, m11, m21);
        Vector3 c = new Vector3f(m02, m12, m22);
        return inverse(a, b, c);
    }

    /**
     * Creates a new inverse {@link Matrix3 matrix} from the specified values,
     * if possible.
     *
     * @param a
     *            A {@link Vector3 vector} with values for the first column.
     * @param b
     *            A {@link Vector3 vector} with values for the second column.
     * @param c
     *            A {@link Vector3 vector} with values for the third column.
     * @return A new inverse {@link Matrix3 matrix} calculated from the
     *         specified column-{@link Vector3 vectors}.
     * @throws ArithmeticException
     *             If the values represent a singular {@link Matrix3 matrix}. A
     *             singular {@link Matrix3 matrix} is non-invertible because its
     *             determinant is zero.
     */
    public static Matrix3 inverse(final Vector3 a, final Vector3 b, final Vector3 c) {
        // Adapted from FGED vol 1, p.48
        Vector3 r0 = b.cross(c);
        Vector3 r1 = c.cross(a);
        Vector3 r2 = a.cross(b);

        float invDet = 1f / r2.dot(c);

        if (Double.isInfinite(invDet) || Double.isNaN(invDet))
            throw new SingularMatrixException("Matrix is non-invertible: determinant is zero");

        r0 = r0.mult(invDet);
        r1 = r1.mult(invDet);
        r2 = r2.mult(invDet);

        // @formatter:off
        return new Matrix3f(
            r0.x(), r0.y(), r0.z(),
            r1.x(), r1.y(), r1.z(),
            r2.x(), r2.y(), r2.z()
        );
        // @formatter:on
    }

    /**
     * Creates a new inverse {@link Matrix3 matrix} from the specified values,
     * if possible.
     *
     * @param m
     *            An array of at least 9 values, in column-major order.
     * @return A new inverse {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws SingularMatrixException
     *             If the values represent a singular {@link Matrix3 matrix}. A
     *             singular {@link Matrix3 matrix} is non-invertible because its
     *             determinant is zero.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 9 elements.
     */
    public static Matrix3 inverse(final float[] m) {
        Vector3 a = new Vector3f(m[0], m[1], m[2]);
        Vector3 b = new Vector3f(m[3], m[4], m[5]);
        Vector3 c = new Vector3f(m[6], m[7], m[8]);
        return inverse(a, b, c);
    }

    /**
     * Creates a new inverse {@link Matrix3 matrix} from the specified values,
     * if possible.
     *
     * @param m
     *            A 3x3 array of values, in column-major order.
     * @return A new inverse {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws SingularMatrixException
     *             If the values represent a singular {@link Matrix3 matrix}. A
     *             singular {@link Matrix3 matrix} is non-invertible because its
     *             determinant is zero.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 3 elements per dimension.
     */
    public static Matrix3 inverse(final float[][] m) {
        Vector3 a = new Vector3f(m[0]);
        Vector3 b = new Vector3f(m[1]);
        Vector3 c = new Vector3f(m[2]);
        return inverse(a, b, c);
    }

    /**
     * Creates a new transposed {@link Matrix3 matrix} from the specified
     * values.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     * @return A new transposed {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 9 elements.
     */
    // @formatter:off
    public static Matrix3 transpose(float m00, float m01, float m02,
                                    float m10, float m11, float m12,
                                    float m20, float m21, float m22) {
        return new Matrix3f(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
    }
    // @formatter:on

    /**
     * Creates a new inverse {@link Matrix3 matrix} from the specified values.
     *
     * @param c0
     *            A {@link Vector3 vector} with values for the first column.
     * @param c1
     *            A {@link Vector3 vector} with values for the second column.
     * @param c2
     *            A {@link Vector3 vector} with values for the third column.
     * @return A new inverse {@link Matrix3 matrix} calculated from the
     *         specified column {@link Vector3 vectors}.
     */
    public static Matrix3 transpose(final Vector3 c0, final Vector3 c1, final Vector3 c2) {
        // @formatter:off
        return new Matrix3f(
            c0.x(), c0.y(), c0.z(),
            c1.x(), c1.y(), c1.z(),
            c2.x(), c2.y(), c2.z()
        );
        // @formatter:on
    }

    /**
     * Creates a new transposed {@link Matrix3 matrix} from the specified
     * values.
     *
     * @param m
     *            An array of at least 9 values, in column-major order.
     * @return A new transposed {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 9 elements.
     */
    public static Matrix3 transpose(final float[] m) {
        // @formatter:off
        return new Matrix3f(
            m[0], m[1], m[2],
            m[3], m[4], m[5],
            m[6], m[7], m[8]
        );
        // @formatter:on
    }

    /**
     * Creates a new transposed {@link Matrix3 matrix} from the specified
     * values.
     *
     * @param m
     *            A 3x3 array of values, in column-major order.
     * @return A new transposed {@link Matrix3 matrix} calculated from the
     *         specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 3 elements per dimension.
     */
    public static Matrix3 transpose(final float[][] m) {
        // @formatter:off
        return new Matrix3f(
            m[0][0], m[0][1], m[0][2],
            m[1][0], m[1][1], m[1][2],
            m[2][0], m[2][1], m[2][2]
        );
        // @formatter:on
    }

    /**
     * Creates a new scaling {@link Matrix3 matrix} from the specified scalar
     * values.
     *
     * @param sx
     *            A scalar value used to scale the <code>x</code> component in
     *            the first column.
     * @param sy
     *            A scalar value used to scale the <code>y</code> component in
     *            the second column.
     * @param sz
     *            A scalar value used to scale the <code>z</code> component in
     *            the third column.
     * @return A new {@link Matrix3 matrix} that can be used to scale another
     *         {@link Matrix3 matrix} when multiplied together.
     */
    public static Matrix3 scaling(float sx, float sy, float sz) {
        // @formatter:off
        return new Matrix3f(
            sx, 0f, 0f,
            0f, sy, 0f,
            0f, 0f, sz
        );
        // @formatter:on
    }

    /**
     * Creates a new scaling {@link Matrix3 matrix} from the specified
     * column-{@link Vector3 vector}.
     *
     * @param sv
     *            A {@link Vector3 vector} with scaling values for the
     *            <code>x</code>, <code>y</code>, and <code>z</code> components.
     * @return A new scaling {@link Matrix3 matrix} based on the specified
     *         column-{@link Vector3 vector}.
     */
    public static Matrix3 scaling(final Vector3 sv) {
        return scaling(sv.x(), sv.y(), sv.z());
    }

    /**
     * Creates a new {@link Matrix3 matrix} rotated by the specified amount
     * along the specified axis.
     *
     * @param angle
     *            The {@link Angle angle} of rotation to be applied.
     * @param axis
     *            A {@link Vector3 vector} specifying the axis of rotation.
     * @return A new {@link Matrix3 matrix} with the specified rotation applied.
     */
    public static Matrix3 rotation(final Angle angle, final Vector3 axis) {
        final float r = angle.valueRadians();
        final Vector3 v = axis.normalize();
        final float c = MathUtil.cos(r);
        final float s = MathUtil.sin(r);
        final float t = 1.0f - c;

        final float x = v.x();
        final float y = v.y();
        final float z = v.z();

        final float x2 = x * x;
        final float y2 = y * y;
        final float z2 = z * z;
        final float xy = x * y;
        final float xz = x * z;
        final float yz = y * z;

        final float txy = t * xy;
        final float txz = t * xz;
        final float tyz = t * yz;

        final float xsin = x * s;
        final float ysin = y * s;
        final float zsin = z * s;

        // @formatter:off
        return new Matrix3f(
            (t * x2 + c), (txy - zsin), (txz + ysin),
            (txy + zsin), (t * y2 + c), (tyz - xsin),
            (txz - ysin), (tyz + xsin), (t * z2 + c)
        );
        // @formatter:on
    }

    @Override
    public float value(int row, int col) {
        return matrix[col][row];
    }

    @Override
    public Vector3 row(int row) {
        return new Vector3f(matrix[0][row], matrix[1][row], matrix[2][row]);
    }

    @Override
    public Vector3 column(int col) {
        return new Vector3f(matrix[col][0], matrix[col][1], matrix[col][2]);
    }

    @Override
    public Matrix3 add(final Matrix3 m) {
        final float m00 = matrix[0][0] + m.value(0, 0);
        final float m01 = matrix[0][1] + m.value(1, 0);
        final float m02 = matrix[0][2] + m.value(2, 0);
        final float m10 = matrix[1][0] + m.value(0, 1);
        final float m11 = matrix[1][1] + m.value(1, 1);
        final float m12 = matrix[1][2] + m.value(2, 1);
        final float m20 = matrix[2][0] + m.value(0, 2);
        final float m21 = matrix[2][1] + m.value(1, 2);
        final float m22 = matrix[2][2] + m.value(2, 2);
        // @formatter:off
        return new Matrix3f(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
        // @formatter:on
    }

    @Override
    public Matrix3 sub(final Matrix3 m) {
        final float m00 = matrix[0][0] - m.value(0, 0);
        final float m01 = matrix[0][1] - m.value(1, 0);
        final float m02 = matrix[0][2] - m.value(2, 0);
        final float m10 = matrix[1][0] - m.value(0, 1);
        final float m11 = matrix[1][1] - m.value(1, 1);
        final float m12 = matrix[1][2] - m.value(2, 1);
        final float m20 = matrix[2][0] - m.value(0, 2);
        final float m21 = matrix[2][1] - m.value(1, 2);
        final float m22 = matrix[2][2] - m.value(2, 2);
        // @formatter:off
        return new Matrix3f(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
        // @formatter:on
    }

    @Override
    public Matrix3 mult(float s) {
        final float m00 = matrix[0][0] * s;
        final float m01 = matrix[0][1] * s;
        final float m02 = matrix[0][2] * s;
        final float m10 = matrix[1][0] * s;
        final float m11 = matrix[1][1] * s;
        final float m12 = matrix[1][2] * s;
        final float m20 = matrix[2][0] * s;
        final float m21 = matrix[2][1] * s;
        final float m22 = matrix[2][2] * s;
        // @formatter:off
        return new Matrix3f(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
        // @formatter:on
    }

    @Override
    public Vector3 mult(final Vector3 v) {
        final float tx = matrix[0][0] * v.x() + matrix[1][0] * v.y() + matrix[2][0] * v.z();
        final float ty = matrix[0][1] * v.x() + matrix[1][1] * v.y() + matrix[2][1] * v.z();
        final float tz = matrix[0][2] * v.x() + matrix[1][2] * v.y() + matrix[2][2] * v.z();
        return new Vector3f(tx, ty, tz);
    }

    @Override
    public Point3 mult(final Point3 p) {
        final float tx = matrix[0][0] * p.x() + matrix[1][0] * p.y() + matrix[2][0] * p.z();
        final float ty = matrix[0][1] * p.x() + matrix[1][1] * p.y() + matrix[2][1] * p.z();
        final float tz = matrix[0][2] * p.x() + matrix[1][2] * p.y() + matrix[2][2] * p.z();
        return new Point3f(tx, ty, tz);
    }

    @Override
    public Matrix3 mult(final Matrix3 m) {
        final float m00 = value(0, 0) * m.value(0, 0) + value(0, 1) * m.value(1, 0) + value(0, 2) * m.value(2, 0);
        final float m10 = value(0, 0) * m.value(0, 1) + value(0, 1) * m.value(1, 1) + value(0, 2) * m.value(2, 1);
        final float m20 = value(0, 0) * m.value(0, 2) + value(0, 1) * m.value(1, 2) + value(0, 2) * m.value(2, 2);
        final float m01 = value(1, 0) * m.value(0, 0) + value(1, 1) * m.value(1, 0) + value(1, 2) * m.value(2, 0);
        final float m11 = value(1, 0) * m.value(0, 1) + value(1, 1) * m.value(1, 1) + value(1, 2) * m.value(2, 1);
        final float m21 = value(1, 0) * m.value(0, 2) + value(1, 1) * m.value(1, 2) + value(1, 2) * m.value(2, 2);
        final float m02 = value(2, 0) * m.value(0, 0) + value(2, 1) * m.value(1, 0) + value(2, 2) * m.value(2, 0);
        final float m12 = value(2, 0) * m.value(0, 1) + value(2, 1) * m.value(1, 1) + value(2, 2) * m.value(2, 1);
        final float m22 = value(2, 0) * m.value(0, 2) + value(2, 1) * m.value(1, 2) + value(2, 2) * m.value(2, 2);
        // @formatter:off
        return new Matrix3f(
            m00, m10, m20,
            m01, m11, m21,
            m02, m12, m22
        );
        // @formatter:on
    }

    @Override
    public Matrix3 rotate(final Angle x, final Angle y, final Angle z) {
        Matrix3 rx = rotation(x, Vector3f.unitX());
        Matrix3 ry = rotation(y, Vector3f.unitY());
        Matrix3 rz = rotation(z, Vector3f.unitZ());
        return mult(rx).mult(ry).mult(rz);
    }

    @Override
    public Matrix3 rotate(final Angle angle, final Vector3 axis) {
        return mult(rotation(angle, axis));
    }

    @Override
    public Matrix3 scale(float sx, float sy, float sz) {
        return mult(scaling(sx, sy, sz));
    }

    @Override
    public Matrix3 scale(final Vector3 v) {
        return scale(v.x(), v.y(), v.z());
    }

    @Override
    public float determinant() {
        // https://www.khanacademy.org/math/precalculus/precalc-matrices/inverting_matrices/v/finding-the-determinant-of-a-3x3-matrix-method-2
        // @formatter:off
        return +(matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]))
               -(matrix[1][0] * (matrix[0][1] * matrix[2][2] - matrix[2][1] * matrix[0][2]))
               +(matrix[2][0] * (matrix[0][1] * matrix[1][2] - matrix[1][1] * matrix[0][2]));
        // @formatter:on
    }

    @Override
    public Matrix3 inverse() {
        Vector3 a = column(0);
        Vector3 b = column(1);
        Vector3 c = column(2);
        return inverse(a, b, c);
    }

    @Override
    public Matrix3 transpose() {
        // @formatter:off
        return transpose(
            matrix[0][0], matrix[1][0], matrix[2][0],
            matrix[0][1], matrix[1][1], matrix[2][1],
            matrix[0][2], matrix[1][2], matrix[2][2]
        );
        // @formatter:on
    }

    /**
     * The values are ordered as documented {@link Matrix3f here}.
     *
     * @see Bufferable#toFloatArray()
     */
    @Override
    public float[] toFloatArray() {
        return MatrixUtil.toFlatArray(matrix);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        final int result = 3;
        return prime * result + matrix.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Matrix3))
            return false;

        Matrix3 other = (Matrix3) obj;
        return MatrixUtil.areEqual(this, other, DIMENSIONS);
    }

    @Override
    public String toString() {
        // TODO: Improve formatting
        StringBuilder fmt = new StringBuilder();
        fmt.append(Matrix3f.class.getSimpleName() + " = [%9.5f | %9.5f | %9.5f]%n");
        fmt.append("           [%9.5f | %9.5f | %9.5f]%n");
        fmt.append("           [%9.5f | %9.5f | %9.5f]");

        // @formatter:off
        return String.format(fmt.toString(),
            matrix[0][0], matrix[1][0], matrix[2][0],
            matrix[0][1], matrix[1][1], matrix[2][1],
            matrix[0][2], matrix[1][2], matrix[2][2]
        );
        // @formatter:on
    }

}
