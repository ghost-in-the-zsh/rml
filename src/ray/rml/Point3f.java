/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * An immutable single-precision floating point 3-dimensional {@link Point3
 * point}.
 * <p>
 * The implementation assumes the input arrays have enough elements to build the
 * vector. When the input array does not have enough elements, an
 * {@link IndexOutOfBoundsException} will be thrown. However, when the input
 * array has extra elements, the extra data are ignored.
 *
 * @author Raymond L. Rivera
 *
 */
public final class Point3f implements Point3 {

    private final static Point3 origin = new Point3f(0f, 0f, 0f);

    private final float         x;
    private final float         y;
    private final float         z;

    /**
     * Creates a new {@link Point3 point} instance with the specified values.
     *
     * @param x
     *            The value for the <code>x</code> component.
     * @param y
     *            The value for the <code>y</code> component.
     * @param z
     *            The value for the <code>z</code> component.
     * @return A new {@link Point3 point} with the specified values.
     */
    public Point3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Creates a new {@link Point3 point} instance with the specified values.
     *
     * @param p
     *            An array with the values for the x and y components in the
     *            first and second positions respectively.
     * @return A new {@link Point3 point} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 2 elements.
     */
    public Point3f(final float[] p) {
        this(p[0], p[1], p.length > 2 ? p[2] : 0f);
    }

    /**
     * Creates a new {@link Point3 point} with the specified values.
     *
     * @param x
     *            The value for the x component.
     * @param y
     *            The value for the y component.
     * @return A new {@link Point3 point} with the specified values. The
     *         unspecified z component defaults to zero.
     */
    public Point3f(float x, float y) {
        this(x, y, 0f);
    }

    /**
     * Creates a new {@link Point3 point} from the given {@link Vector3 vector}.
     *
     * @param v
     *            The {@link Vector3 vector}.
     */
    public Point3f(final Vector3 v) {
        this(v.x(), v.y(), v.z());
    }

    /**
     * Creates a new {@link Point3 point} from the given {@link Vector4 vector},
     * dropping the extra <code>w</code> component.
     *
     * @param v
     *            The {@link Vector4 vector}.
     */
    public Point3f(final Vector4 v) {
        this(v.x(), v.y(), v.z());
    }

    /**
     * Creates a new {@link Point3 point} placed at the origin.
     *
     * @return A new {@link Point3 point} at <code>(0,0,0)</code>.
     */
    public static Point3 origin() {
        return origin;
    }

    @Override
    public float x() {
        return x;
    }

    @Override
    public float y() {
        return y;
    }

    @Override
    public float z() {
        return z;
    }

    @Override
    public Point3 add(final Point3 p) {
        return new Point3f(x + p.x(), y + p.y(), z + p.z());
    }

    @Override
    public Point3 add(float x, float y, float z) {
        return new Point3f(this.x + x, this.y + y, this.z + z);
    }

    @Override
    public Point3 sub(final Point3 p) {
        return new Point3f(x - p.x(), y - p.y(), z - p.z());
    }

    @Override
    public Point3 sub(float x, float y, float z) {
        return new Point3f(this.x - x, this.y - y, this.z - z);
    }

    @Override
    public Point3 mult(float s) {
        return new Point3f(x * s, y * s, z * s);
    }

    @Override
    public Point3 div(float s) {
        s = 1.0f / s;
        return new Point3f(x * s, y * s, z * s);
    }

    @Override
    public float distanceTo(final Point3 p) {
        return MathUtil.sqrt(MathUtil.pow(p.x() - x, 2) + MathUtil.pow(p.y() - y, 2) + MathUtil.pow(p.z() - z, 2));
    }

    @Override
    public Point3 rotate(Angle angle, Vector3 axis) {
        return Matrix3f.rotation(angle, axis).mult(this);
    }

    /**
     * The values are in the following order: <code>[x, y]</code>.
     *
     * @see Bufferable#toFloatArray()
     */
    @Override
    public float[] toFloatArray() {
        return new float[] { x, y, z };
    }

    @Override
    public Point3 lerp(Point3 end, float t) {
        // t in [0, 1]
        t = MathUtil.clamp(t, 0f, 1f);
        return new Point3f(MathUtil.lerp(toFloatArray(), end.toFloatArray(), t));
    }

    @Override
    public Point3 negate() {
        return new Point3f(-x, -y, -z);
    }

    @Override
    public int compareTo(final Point3 p) {
        final float a = MathUtil.lengthSquared(x, y, z);
        final float b = MathUtil.lengthSquared(p.x(), p.y(), p.z());
        return FloatUtil.compare(a, b);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 11;
        result = prime * result + Float.floatToIntBits(x);
        result = prime * result + Float.floatToIntBits(y);
        result = prime * result + Float.floatToIntBits(z);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Point3))
            return false;

        Point3 other = (Point3) obj;
        if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x()))
            return false;
        if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y()))
            return false;
        if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z()))
            return false;

        return true;
    }

    @Override
    public String toString() {
        return Point3f.class.getSimpleName() + "(" + x + ", " + y + ", " + z + ")";
    }

}
