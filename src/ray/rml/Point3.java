/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * A 3-dimensional {@link Point point}.
 * <p>
 * {@link Point Points} can be transformed using {@link Matrix3 matrix}
 * multiplication and converted to {@link Vector3 vectors}. For the purposes of
 * {@link Matrix3 matrix} multiplication, the {@link Point point} is treated as
 * a column-{@link Vector3 vector}.
 * <p>
 * This convention is made explicit by the lack of a <code>mult</code> method
 * expecting a {@link Matrix3 matrix}, where the {@link Point3 point} being
 * multiplied is on the left side of the {@link Matrix3 matrix}. In other words,
 * it's not possible to take a {@link Matrix3 matrix} <code>M</code> and a
 * {@link Point3 point} <code>p</code> and multiply them as <code>p' = pM</code>
 * to produce a transformed {@link Point3 point} <code>p'</code> as the output;
 * it must always be in the form <code>p' = Mp</code>.
 *
 * @see Matrix3
 * @see Vector3
 *
 * @author Raymond L. Rivera
 *
 */
public interface Point3 extends Point<Point3>, ThreeDimensional, AngleAxisRotatable<Point3, Vector3> {

    /**
     * Adds the specified scalar values to the respective components of
     * <code>this</code> {@link Point3 point}.
     *
     * @param x
     *            The scalar to add to <code>this</code> x component.
     * @param y
     *            The scalar to add to <code>this</code> y component.
     * @param z
     *            The scalar to add to <code>this</code> z component.
     * @return A new {@link Point3 point} with the result of this addition.
     */
    Point3 add(float x, float y, float z);

    /**
     * Subtracts the specified scalar values from the respective components of
     * <code>this</code> {@link Point3 point}.
     *
     * @param x
     *            The scalar to subtract from <code>this</code> x component.
     * @param y
     *            The scalar to subtract from <code>this</code> y component.
     * @param z
     *            The scalar to subtract from <code>this</code> z component.
     * @return A new {@link Point3 point} with the result of this subtraction.
     */
    Point3 sub(float x, float y, float z);

}
