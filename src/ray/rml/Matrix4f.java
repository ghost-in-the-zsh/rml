/**
 * This file is part of RML.
 *
 * RML is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RML is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rml;

/**
 * An immutable single-precision floating point column-major 4x4 square
 * {@link Matrix4 matrix}.
 * <p>
 * Constructors and factory method arguments still get arranged in a
 * WYSIWYG-style format. This means the following input sequence in a
 * constructor or factory method:
 *
 * <pre>
 *     ( 1,  2,  3,  4,
 *       5,  6,  7,  8,
 *       9, 10, 11, 12,
 *      13, 14, 15, 16)
 * </pre>
 *
 * will produce the following matrix layout:
 * <p>
 * <table summary="" style="border: 1px solid black;">
 * <tr style="border: 1px solid black; padding: 10px;">
 * <td style="border: 1px solid black; padding: 10px;">1</td>
 * <td style="border: 1px solid black; padding: 10px;">2</td>
 * <td style="border: 1px solid black; padding: 10px;">3</td>
 * <td style="border: 1px solid black; padding: 10px;">4</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 10px;">5</td>
 * <td style="border: 1px solid black; padding: 10px;">6</td>
 * <td style="border: 1px solid black; padding: 10px;">7</td>
 * <td style="border: 1px solid black; padding: 10px;">8</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 10px;">9</td>
 * <td style="border: 1px solid black; padding: 10px;">10</td>
 * <td style="border: 1px solid black; padding: 10px;">11</td>
 * <td style="border: 1px solid black; padding: 10px;">12</td>
 * </tr>
 * <tr>
 * <td style="border: 1px solid black; padding: 10px;">13</td>
 * <td style="border: 1px solid black; padding: 10px;">14</td>
 * <td style="border: 1px solid black; padding: 10px;">15</td>
 * <td style="border: 1px solid black; padding: 10px;">16</td>
 * </tr>
 * </table>
 * <p>
 * Array-based inputs must be in <i>column</i>-major order and contain the
 * minimum number of elements needed to completely fill up the matrix. Extra
 * elements are automatically ignored.
 *
 * @author Raymond L. Rivera
 *
 */
public final class Matrix4f implements Matrix4 {

    private static final int      DIMENSIONS = 4;

    // @formatter:off
    private static final Matrix4f zero       = new Matrix4f(
        0f, 0f, 0f, 0f,
        0f, 0f, 0f, 0f,
        0f, 0f, 0f, 0f,
        0f, 0f, 0f, 0f
    );
    private static final Matrix4f identity   = new Matrix4f(
        1f, 0f, 0f, 0f,
        0f, 1f, 0f, 0f,
        0f, 0f, 1f, 0f,
        0f, 0f, 0f, 1f
    );
    // @formatter:on

    private final float[][]       matrix     = new float[DIMENSIONS][DIMENSIONS];

    /**
     * Creates a new {@link Matrix4 matrix} from the specified values.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m03
     *            The element at the 1st row and 4th column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m13
     *            The element at the 2nd row and 4th column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     * @param m23
     *            The element at the 3rd row and 4th column.
     * @param m30
     *            The element at the 4th row and 1st column.
     * @param m31
     *            The element at the 4th row and 2nd column.
     * @param m32
     *            The element at the 4th row and 3rd column.
     * @param m33
     *            The element at the 4th row and 4th column.
     */
    // @formatter:off
    public Matrix4f(float m00, float m01, float m02, float m03,
                    float m10, float m11, float m12, float m13,
                    float m20, float m21, float m22, float m23,
                    float m30, float m31, float m32, float m33) {
    // @formatter:on
        matrix[0][0] = m00;
        matrix[0][1] = m10;
        matrix[0][2] = m20;
        matrix[0][3] = m30;
        matrix[1][0] = m01;
        matrix[1][1] = m11;
        matrix[1][2] = m21;
        matrix[1][3] = m31;
        matrix[2][0] = m02;
        matrix[2][1] = m12;
        matrix[2][2] = m22;
        matrix[2][3] = m32;
        matrix[3][0] = m03;
        matrix[3][1] = m13;
        matrix[3][2] = m23;
        matrix[3][3] = m33;
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified values.
     *
     * @param m
     *            A 4x4 array of values, in column-major order.
     * @return A new {@link Matrix4 matrix} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 4 elements per dimension.
     */
    public Matrix4f(final float[][] m) {
        // @formatter:off
        this(
            m[0][0], m[1][0], m[2][0], m[3][0],
            m[0][1], m[1][1], m[2][1], m[3][1],
            m[0][2], m[1][2], m[2][2], m[3][2],
            m[0][3], m[1][3], m[2][3], m[3][3]
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified values.
     *
     * @param m
     *            An array of at least 16 values, in column-major order.
     * @return A new {@link Matrix4 matrix} with the specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 16 elements.
     */
    public Matrix4f(final float[] m) {
        // @formatter:off
        this(
            m[0], m[4], m[8] , m[12],
            m[1], m[5], m[9] , m[13],
            m[2], m[6], m[10], m[14],
            m[3], m[7], m[11], m[15]
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified column
     * {@link Vector3 vectors}.
     * <p>
     * The added <code>w</code> component of each {@link Vector3 vector} is set
     * to <code>0</code> by default, except for the last column, which is set to
     * <code>1</code>.
     *
     * @param c0
     *            A {@link Vector3 vector} with values for the first column,
     *            with <code>w = 0</code>.
     * @param c1
     *            A {@link Vector3 vector} with values for the second column,
     *            with <code>w = 0</code>.
     * @param c2
     *            A {@link Vector3 vector} with values for the third column,
     *            with <code>w = 0</code>.
     * @param c3
     *            A {@link Vector3 vector} with values for the fourth column,
     *            with <code>w = 1</code>.
     * @return A new {@link Matrix4 matrix} with the specified values.
     */
    public Matrix4f(final Vector3 c0, final Vector3 c1, final Vector3 c2, final Vector3 c3) {
        // @formatter:off
        this(
            c0.x(), c1.x(), c2.x(), c3.x(),
            c0.y(), c1.y(), c2.y(), c3.y(),
            c0.z(), c1.z(), c2.z(), c3.z(),
              0f  ,   0f  ,   0f  ,   1f
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified column
     * {@link Vector3 vectors} and {@link Point3 point} location.
     * <p>
     * The added <code>w</code> component of each {@link Vector3 vector} is set
     * to <code>0</code> by default, except for the last column, which
     * represents a position and is set to <code>1</code>.
     *
     * @param c0
     *            A {@link Vector3 vector} with values for the first column,
     *            with <code>w = 0</code>.
     * @param c1
     *            A {@link Vector3 vector} with values for the second column,
     *            with <code>w = 0</code>.
     * @param c2
     *            A {@link Vector3 vector} with values for the third column,
     *            with <code>w = 0</code>.
     * @param p
     *            A {@link Point3 point} with values for the fourth column, with
     *            <code>w = 1</code>.
     * @return A new {@link Matrix4 matrix} with the specified values.
     */
    public Matrix4f(final Vector3 c0, final Vector3 c1, final Vector3 c2, final Point3 p) {
        // @formatter:off
        this(
            c0.x(), c1.x(), c2.x(), p.x(),
            c0.y(), c1.y(), c2.y(), p.y(),
            c0.z(), c1.z(), c2.z(), p.z(),
              0f  ,   0f  ,   0f  ,   1f
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified
     * column-{@link Vector4 vectors}.
     *
     * @param c0
     *            A {@link Vector4 vector} with values for the first column.
     * @param c1
     *            A {@link Vector4 vector} with values for the second column.
     * @param c2
     *            A {@link Vector4 vector} with values for the third column.
     * @param c3
     *            A {@link Vector4 vector} with values for the fourth column.
     * @return A new {@link Matrix4 matrix} with the specified values.
     */
    public Matrix4f(final Vector4 c0, final Vector4 c1, final Vector4 c2, final Vector4 c3) {
        // @formatter:off
        this(
            c0.x(), c1.x(), c2.x(), c3.x(),
            c0.y(), c1.y(), c2.y(), c3.y(),
            c0.z(), c1.z(), c2.z(), c3.z(),
            c0.w(), c1.w(), c2.w(), c3.w()
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified {@link Matrix3
     * matrix}.
     *
     * @param m
     *            The {@link Matrix3 matrix} from which this {@link Matrix4
     *            matrix} will be built.
     * @return A new {@link Matrix4 matrix} from the specified
     *         sub-{@link Matrix4 matrix}.
     */
    public Matrix4f(final Matrix3 m) {
        this(m, new Vector4f(0, 0, 0, 1f));
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified {@link Matrix3}.
     *
     * @param m
     *            The {@link Matrix3 matrix} from which this {@link Matrix4
     *            matrix} will be built.
     * @param v
     *            A {@link Vector3 vector} representing the current position.
     * @return A new {@link Matrix4 matrix} from the specified
     *         sub-{@link Matrix4 matrix}.
     */
    public Matrix4f(final Matrix3 m, final Vector3 v) {
        this(m, new Vector4f(v, 1f));
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified {@link Matrix3}.
     *
     * @param m
     *            The {@link Matrix3 matrix} from which this {@link Matrix4
     *            matrix} will be built.
     * @param p
     *            A {@link Point3 point} representing the current position.
     * @return A new {@link Matrix4 matrix} from the specified
     *         sub-{@link Matrix4 matrix}.
     */
    public Matrix4f(final Matrix3 m, final Point3 p) {
        this(m, new Vector4f(p));
    }

    /**
     * Creates a new {@link Matrix4 matrix} from the specified {@link Matrix3}.
     *
     * @param m
     *            The {@link Matrix3 matrix} from which this {@link Matrix4
     *            matrix} will be built.
     * @param v
     *            A <i>positional</i> {@link Vector4 vector}.
     * @return A new {@link Matrix4 matrix} from the specified
     *         sub-{@link Matrix4 matrix}.
     */
    public Matrix4f(final Matrix3 m, final Vector4 v) {
        // @formatter:off
        this(
            new Vector4f(m.column(0)),
            new Vector4f(m.column(1)),
            new Vector4f(m.column(2)),
            v
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} with all values set to zero.
     *
     * @return A new zero-value {@link Matrix4 matrix}.
     */
    public static Matrix4 zero() {
        return zero;
    }

    /**
     * Creates a new {@link Matrix4 matrix} of the identity {@link Matrix4
     * matrix}.
     * <p>
     * The identity {@link Matrix4 matrix} has values along the diagonal set to
     * one and all others set to zero.
     *
     * @return A new identity {@link Matrix4 matrix}.
     */
    public static Matrix4 identity() {
        return identity;
    }

    /**
     * Creates a new inverse {@link Matrix4 matrix} from the specified values,
     * if possible.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m03
     *            The element at the 1st row and 4th column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m13
     *            The element at the 2nd row and 4th column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     * @param m23
     *            The element at the 3rd row and 4th column.
     * @param m30
     *            The element at the 4th row and 1st column.
     * @param m31
     *            The element at the 4th row and 2nd column.
     * @param m32
     *            The element at the 4th row and 3rd column.
     * @param m33
     *            The element at the 4th row and 4th column.
     * @return A new inverse {@link Matrix4 matrix} calculated from the
     *         specified values.
     * @throws ArithmeticException
     *             If the values represent a singular {@link Matrix4 matrix}. A
     *             singular {@link Matrix4 matrix} is non-invertible because its
     *             determinant is zero.
     */
    // @formatter:off
    public static Matrix4 inverse(float m00, float m01, float m02, float m03,
                                  float m10, float m11, float m12, float m13,
                                  float m20, float m21, float m22, float m23,
                                  float m30, float m31, float m32, float m33) {
    // @formatter:on
        Vector4 a = new Vector4f(m00, m10, m20, m30);
        Vector4 b = new Vector4f(m01, m11, m21, m31);
        Vector4 c = new Vector4f(m02, m12, m22, m32);
        Vector4 d = new Vector4f(m03, m13, m23, m33);
        return inverse(a, b, c, d);
    }

    /**
     * Creates a new inverse {@link Matrix4 matrix} from the specified values,
     * if possible.
     *
     * @param m
     *            An array of at least 16 values, in column-major order.
     * @return A new inverse {@link Matrix4 matrix} calculated from the
     *         specified values.
     * @throws SingularMatrixException
     *             If the values represent a singular {@link Matrix4 matrix}. A
     *             singular {@link Matrix4 matrix} is non-invertible because its
     *             determinant is zero.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 16 elements.
     */
    public static Matrix4 inverse(final float[] m) {
        Vector4 a = new Vector4f(m[0], m[1], m[2], m[3]);
        Vector4 b = new Vector4f(m[4], m[5], m[6], m[7]);
        Vector4 c = new Vector4f(m[8], m[9], m[10], m[11]);
        Vector4 d = new Vector4f(m[12], m[13], m[14], m[15]);
        return inverse(a, b, c, d);
    }

    /**
     * Creates a new inverse {@link Matrix4 matrix} from the specified values,
     * if possible.
     *
     * @param m
     *            A 4x4 array of values, in column-major order.
     * @return A new inverse {@link Matrix4 matrix} calculated from the
     *         specified values.
     * @throws SingularMatrixException
     *             If the values represent a singular {@link Matrix4 matrix}. A
     *             singular {@link Matrix4 matrix} is non-invertible because its
     *             determinant is zero.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 4 elements per dimension.
     */
    public static Matrix4 inverse(final float[][] m) {
        Vector4 a = new Vector4f(m[0]);
        Vector4 b = new Vector4f(m[1]);
        Vector4 c = new Vector4f(m[2]);
        Vector4 d = new Vector4f(m[3]);
        return inverse(a, b, c, d);
    }

    /**
     * Creates a new inverse {@link Matrix4 matrix} from the specified column
     * {@link Vector4 vectors}, if possible.
     *
     * @param c0
     *            A {@link Vector4 vector} with values for the first column.
     * @param c1
     *            A {@link Vector4 vector} with values for the second column.
     * @param c2
     *            A {@link Vector4 vector} with values for the third column.
     * @param c3
     *            A {@link Vector4 vector} with values for the fourth column.
     * @return A new {@link Matrix4 matrix} with the specified values.
     * @throws ArithmeticException
     *             If the values represent a singular {@link Matrix4 matrix}. A
     *             singular {@link Matrix4 matrix} is non-invertible because its
     *             determinant is zero.
     */
    public static Matrix4 inverse(final Vector4 c0, final Vector4 c1, final Vector4 c2, final Vector4 c3) {
        // Adapted from FGED vol 1, p.50
        Vector3 a = c0.toVector3();
        Vector3 b = c1.toVector3();
        Vector3 c = c2.toVector3();
        Vector3 d = c3.toVector3();

        final float x = c0.w();
        final float y = c1.w();
        final float z = c2.w();
        final float w = c3.w();

        Vector3 s = a.cross(b);
        Vector3 t = c.cross(d);
        Vector3 u = a.mult(y).sub(b.mult(x));
        Vector3 v = c.mult(w).sub(d.mult(z));

        final float invDet = 1f / (s.dot(v) + t.dot(u));

        if (Double.isInfinite(invDet) || Double.isNaN(invDet))
            throw new SingularMatrixException("Matrix is non-invertible: determinant is zero");

        s = s.mult(invDet);
        t = t.mult(invDet);
        u = u.mult(invDet);
        v = v.mult(invDet);

        Vector3 r0 = b.cross(v).add(t.mult(y));
        Vector3 r1 = v.cross(a).sub(t.mult(x));
        Vector3 r2 = d.cross(u).add(s.mult(w));
        Vector3 r3 = u.cross(c).sub(s.mult(z));

        // @formatter:off
        return new Matrix4f(
            r0.x(), r0.y(), r0.z(), -b.dot(t),
            r1.x(), r1.y(), r1.z(),  a.dot(t),
            r2.x(), r2.y(), r2.z(), -d.dot(s),
            r3.x(), r3.y(), r3.z(),  c.dot(s)
        );
        // @formatter:on
    }

    /**
     * Creates a new transposed {@link Matrix4 matrix} from the specified
     * values.
     *
     * @param m00
     *            The element at the 1st row and 1st column.
     * @param m01
     *            The element at the 1st row and 2nd column.
     * @param m02
     *            The element at the 1st row and 3rd column.
     * @param m03
     *            The element at the 1st row and 4th column.
     * @param m10
     *            The element at the 2nd row and 1st column.
     * @param m11
     *            The element at the 2nd row and 2nd column.
     * @param m12
     *            The element at the 2nd row and 3rd column.
     * @param m13
     *            The element at the 2nd row and 4th column.
     * @param m20
     *            The element at the 3rd row and 1st column.
     * @param m21
     *            The element at the 3rd row and 2nd column.
     * @param m22
     *            The element at the 3rd row and 3rd column.
     * @param m23
     *            The element at the 3rd row and 4th column.
     * @param m30
     *            The element at the 4th row and 1st column.
     * @param m31
     *            The element at the 4th row and 2nd column.
     * @param m32
     *            The element at the 4th row and 3rd column.
     * @param m33
     *            The element at the 4th row and 4th column.
     * @return A new transposed {@link Matrix4 matrix} calculated from the
     *         specified values.
     */
    // @formatter:off
    public static Matrix4 transpose(float m00, float m01, float m02, float m03,
                                    float m10, float m11, float m12, float m13,
                                    float m20, float m21, float m22, float m23,
                                    float m30, float m31, float m32, float m33) {
        return new Matrix4f(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
    }
    // @formatter:on

    /**
     * Creates a new transposed {@link Matrix4 matrix} from the specified
     * values.
     *
     * @param m
     *            An array of at least 16 values, in column-major order.
     * @return A new transposed {@link Matrix4 matrix} calculated from the
     *         specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 16 elements.
     */
    public static Matrix4 transpose(final float[] m) {
        // @formatter:off
        return new Matrix4f(
            m[0], m[4], m[8] , m[12],
            m[1], m[5], m[9] , m[13],
            m[2], m[6], m[10], m[14],
            m[3], m[7], m[11], m[15]
        );
        // @formatter:on
    }

    /**
     * Creates a new transposed {@link Matrix4 matrix} from the specified
     * values.
     *
     * @param m
     *            A 4x4 array of values, in column-major order.
     * @return A new transposed {@link Matrix4 matrix} calculated from the
     *         specified values.
     * @throws IndexOutOfBoundsException
     *             If the input array has less than 4 elements per dimension.
     */
    public static Matrix4 transpose(final float[][] m) {
        // @formatter:off
        return new Matrix4f(
            m[0][0], m[0][1], m[0][2], m[0][3],
            m[1][0], m[1][1], m[1][2], m[1][3],
            m[2][0], m[2][1], m[2][2], m[2][3],
            m[3][0], m[3][1], m[3][2], m[3][3]
        );
        // @formatter:on
    }

    /**
     * Creates a new transposed {@link Matrix4 matrix} from the specified column
     * {@link Vector4 vectors}.
     *
     * @param c0
     *            A {@link Vector4 vector} with values for the first column.
     * @param c1
     *            A {@link Vector4 vector} with values for the second column.
     * @param c2
     *            A {@link Vector4 vector} with values for the third column.
     * @param c3
     *            A {@link Vector4 vector} with values for the fourth column.
     * @return A new {@link Matrix4 matrix} with the specified values
     *         transposed.
     */
    public static Matrix4 transpose(final Vector4 c0, final Vector4 c1, final Vector4 c2, final Vector4 c3) {
        // @formatter:off
        return new Matrix4f(
            c0.x(), c0.y(), c0.z(), c0.w(),
            c1.x(), c1.y(), c1.z(), c1.w(),
            c2.x(), c2.y(), c2.z(), c2.w(),
            c3.x(), c3.y(), c3.z(), c3.w()
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified translation
     * values applied to the identity {@link Matrix4 matrix}.
     *
     * @param tx
     *            A scalar value by which to translate along the
     *            <code>x</code>-axis.
     * @param ty
     *            A scalar value by which to translate along the
     *            <code>y</code>-axis.
     * @param tz
     *            A scalar value by which to translate along the
     *            <code>z</code>-axis.
     * @return A new translation {@link Matrix4 matrix} based on the specified
     *         values.
     */
    public static Matrix4 translation(float tx, float ty, float tz) {
        // @formatter:off
        return new Matrix4f(
            1f, 0f, 0f, tx,
            0f, 1f, 0f, ty,
            0f, 0f, 1f, tz,
            0f, 0f, 0f, 1f
        );
        // @formatter:on
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified translation
     * {@link Vector3 vector} applied to the identity {@link Matrix4 matrix}.
     *
     * @param tv
     *            The {@link Vector3 vector} used to create this translation.
     * @return A new translation {@link Matrix4 matrix} based on the specified
     *         column {@link Vector3 vector}.
     */
    public static Matrix4 translation(final Vector3 tv) {
        return translation(tv.x(), tv.y(), tv.z());
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified {@link Point3
     * point}'s location applied to the identity {@link Matrix4 matrix}.
     *
     * @param p
     *            The {@link Point3 point} used to create this translation.
     * @return A new translation {@link Matrix4 matrix} based on the specified
     *         {@link Point3 point}.
     */
    public static Matrix4 translation(final Point3 p) {
        return translation(p.x(), p.y(), p.z());
    }

    /**
     * Creates a new {@link Matrix4 matrix} with the specified translation
     * {@link Vector4 vector} applied to the identity {@link Matrix4 matrix}.
     *
     * @param tv
     *            The {@link Vector4 vector} used to create this translation.
     * @return A new translation {@link Matrix4 matrix} based on the specified
     *         column {@link Vector4 vector}.
     */
    public static Matrix4 translation(final Vector4 tv) {
        return translation(tv.x(), tv.y(), tv.z());
    }

    /**
     * Creates a new scaling {@link Matrix4 matrix} from the specified scalar
     * values.
     *
     * @param sx
     *            A scalar value used to scale the <code>x</code> component in
     *            the first column.
     * @param sy
     *            A scalar value used to scale the <code>y</code> component in
     *            the second column.
     * @param sz
     *            A scalar value used to scale the <code>z</code> component in
     *            the third column.
     * @return A new {@link Matrix4 matrix} that can be used to scale another
     *         {@link Matrix4 matrix} when multiplied together.
     */
    public static Matrix4 scaling(float sx, float sy, float sz) {
        // @formatter:off
        return new Matrix4f(
            sx, 0f, 0f, 0f,
            0f, sy, 0f, 0f,
            0f, 0f, sz, 0f,
            0f, 0f, 0f, 1f
        );
        // @formatter:on
    }

    /**
     * Creates a new scaling {@link Matrix4 matrix} from the specified
     * column-{@link Vector3 vector}.
     *
     * @param sv
     *            A {@link Vector3 vector} with scaling values for the
     *            <code>x</code>, <code>y</code>, and <code>z</code> components.
     * @return A new scaling {@link Matrix4 matrix} based on the specified
     *         column-{@link Vector3 vector}.
     */
    public static Matrix4 scaling(final Vector3 sv) {
        return scaling(sv.x(), sv.y(), sv.z());
    }

    /**
     * Creates a new scaling {@link Matrix4 matrix} from the specified
     * column-{@link Vector4 vector}.
     *
     * @param sv
     *            A {@link Vector4 vector} with scaling values for the
     *            <code>x</code>, <code>y</code>, and <code>z</code> components.
     * @return A new scaling {@link Matrix4 matrix} based on the specified
     *         column-{@link Vector4 vector}.
     */
    public static Matrix4 scaling(final Vector4 sv) {
        return scaling(sv.x(), sv.y(), sv.z());
    }

    /**
     * Creates a new {@link Matrix4 matrix} rotated by the specified angle along
     * the specified {@link Vector3 axis}.
     *
     * @param angle
     *            The {@link Angle angle} of rotation to be applied.
     * @param axis
     *            A {@link Vector3 vector} specifying the axis of rotation.
     * @return A new {@link Matrix4 matrix} with the specified rotation applied.
     */
    public static Matrix4 rotation(final Angle angle, final Vector3 axis) {
        final Matrix3 rm = Matrix3f.rotation(angle, axis);
        return new Matrix4f(rm, new Vector4f(0, 0, 0, 1f));
    }

    /**
     * Creates a new {@link Matrix4 matrix} rotated by the specified amount
     * along the specified {@link Vector4 axis}.
     *
     * @param angle
     *            The {@link Angle angle} of rotation to be applied.
     * @param axis
     *            A {@link Vector4 vector} specifying the axis of rotation.
     * @return A new {@link Matrix4 matrix} with the specified rotation applied.
     */
    public static Matrix4 rotation(final Angle angle, final Vector4 axis) {
        return rotation(angle, axis.toVector3());
    }

    /**
     * Creates a new perspective projection {@link Matrix4 matrix}.
     *
     * @param fovy
     *            The {@link Angle angle} specifying the vertical field of view.
     * @param aspect
     *            The width to height aspect ratio.
     * @param near
     *            The near clipping plane. This must be a positive value.
     * @param far
     *            The far clipping plane. This must be a positive value greater
     *            than <code>near</code>.
     * @return A new {@link Matrix4 matrix} with a perspective projection.
     * @throws IllegalArgumentException
     *             If any of the following conditions holds true:
     *             <code>fovy <= 0, near <= 0, far <= 0, near >= far, aspect <= 0</code>
     */
    public static Matrix4 perspective(final Angle fovy, float aspect, float near, float far) {
        float degs = fovy.valueDegrees();
        if (degs <= 0f)
            throw new IllegalArgumentException("Vertical field of view must be > 0");
        if (near <= 0f)
            throw new IllegalArgumentException("Near must be > 0");
        if (far <= 0f)
            throw new IllegalArgumentException("Far must be > 0");
        if (near >= far)
            throw new IllegalArgumentException("Near must be < Far");
        if (aspect <= 0f)
            throw new IllegalArgumentException("Aspect ratio must be > 0");

        float q = 1.0f / MathUtil.tan(MathUtil.toRadians(0.5f * degs));
        float A = q / aspect;
        float B = (near + far) / (near - far);
        float C = (2.0f * near * far) / (near - far);

        // @formatter:off
        return new Matrix4f(
            A , 0f, 0f, 0f,
            0f, q , 0f, 0f,
            0f, 0f, B , C ,
            0f, 0f,-1f, 0f
        );
        // @formatter:on
    }

    /**
     * Creates a new view {@link Matrix4 matrix} from a camera's world-space
     * orientation and position.
     * <p>
     * The camera rotation is commonly denoted by 3 orthonormal axis
     * {@link Vector4 vectors}, labeled <code>(u, v, n)</code>, which specify
     * the <i>side</i>, <i>up</i>, and <i>forward</i> directions respectively. A
     * view {@link Matrix4 matrix} is used to transform objects from world-space
     * into view-space.
     *
     * @param u
     *            The side {@link Vector4 vector} pointing right, in
     *            world-space.
     * @param v
     *            The vertical {@link Vector4 vector} pointing up, in
     *            world-space.
     * @param n
     *            The front {@link Vector4 vector} pointing forward, in
     *            world-space.
     * @param p
     *            The observer's position {@link Vector4 vector}, in
     *            world-space.
     * @return A new {@link Matrix4 matrix} with the viewing transform.
     * @see #view(Vector3, Vector3, Vector3, Point3)
     */
    public static Matrix4 view(Vector4 u, Vector4 v, Vector4 n, Vector4 p) {
        // @formatter:off
        return view(
            u.toVector3(),
            v.toVector3(),
            n.toVector3(),
            new Point3f(p)
        );
        // @formatter:on
    }

    /**
     * Creates a new view {@link Matrix4 matrix} from a camera's world-space
     * orientation and position.
     * <p>
     * The camera rotation is commonly denoted by 3 orthonormal axis
     * {@link Vector3 vectors}, labeled <code>(u, v, n)</code>, which specify
     * the <i>side</i>, <i>up</i>, and <i>forward</i> directions respectively. A
     * view {@link Matrix4 matrix} is used to transform objects from world-space
     * into view-space.
     *
     * @param u
     *            The side {@link Vector3 vector} pointing right, in
     *            world-space.
     * @param v
     *            The vertical {@link Vector3 vector} pointing up, in
     *            world-space.
     * @param n
     *            The front {@link Vector3 vector} pointing forward, in
     *            world-space.
     * @param p
     *            The observer's {@link Point3 position}, in world-space.
     * @return A new {@link Matrix4 matrix} with the viewing transform.
     * @see #view(Vector4, Vector4, Vector4, Vector4)
     */
    public static Matrix4 view(Vector3 u, Vector3 v, Vector3 n, Point3 p) {
        // @formatter:off
        Matrix4 r = new Matrix4f(
            u.x(), u.y(), u.z(), 0f,
            v.x(), v.y(), v.z(), 0f,
            n.x(), n.y(), n.z(), 0f,
             0f  ,  0f  ,  0f  , 1f
        );
        Matrix4 t = new Matrix4f(
            1f, 0f, 0f, -p.x(),
            0f, 1f, 0f, -p.y(),
            0f, 0f, 1f, -p.z(),
            0f, 0f, 0f, 1f
        );
        // @formatter:on
        return r.mult(t);
    }

    /**
     * Create a new look-at {@link Matrix4 matrix}.
     *
     * @param eye
     *            The viewer's {@link Point3 position}, in world-space.
     * @param target
     *            The target's {@link Point3 position}, in world-space.
     * @param up
     *            A {@link Vector3 vector} specifying the viewer's up axis, in
     *            world-space.
     * @return A new look-at {@link Matrix4 matrix}.
     * @see #lookAt(Vector4, Vector4, Vector4)
     */
    public static Matrix4 lookAt(Point3 eye, Point3 target, Vector3 up) {
        // f = normalize(eye - target);
        // r = normalize(cross(up, f));
        // u = cross(f, r);
        Vector3 f = Vector3f.normalize(eye.sub(target));
        Vector3 s = up.cross(f).normalize();
        Vector3 u = f.cross(s);
        Vector3 e = new Vector3f(eye);

        // @formatter:off
        return inverse(
            s.x(), s.y(), s.z(), -s.dot(e),
            u.x(), u.y(), u.z(), -u.dot(e),
            f.x(), f.y(), f.z(), -f.dot(e),
             0f  ,  0f  ,  0f  ,  1f
        );
        // @formatter:on
    }

    /**
     * Create a new look-at {@link Matrix4 matrix}.
     *
     * @param eye
     *            A {@link Vector4 vector} specifying the viewer's position, in
     *            world-space.
     * @param target
     *            A {@link Vector4 vector} specifying the target's position, in
     *            world-space.
     * @param up
     *            A {@link Vector4 vector} specifying the world's up axis, in
     *            world-space.
     * @return A new look-at {@link Matrix4 matrix}.
     * @see #lookAt(Point3, Point3, Vector3)
     */
    public static Matrix4 lookAt(Vector4 eye, Vector4 target, Vector4 up) {
        // @formatter:off
        return lookAt(
            new Point3f(eye),
            new Point3f(target),
            up.toVector3()
        );
        // @formatter:on
    }

    /**
     * Create a new look-at {@link Matrix4 matrix}.
     *
     * @param orientation
     *            A {@link Quaternion quaternion} with the orientation of the
     *            camera, in world-space.
     * @param position
     *            The camera's {@link Point3 position}, in world-space.
     * @return A new look-at {@link Matrix4 matrix}.
     */
    public static Matrix4 lookAt(final Quaternion orientation, final Point3 position) {
        // Make the translation relative to new axes; adapted from the OGRE3D
        Matrix3 m = new Matrix3f(orientation).transpose();
        Point3 p = m.mult(position).negate();
        return new Matrix4f(m, p);
    }

    /**
     * Create a new look-at {@link Matrix4 matrix}.
     *
     * @param orientation
     *            A {@link Quaternion quaternion} with the orientation of the
     *            camera, in world-space.
     * @param position
     *            A {@link Vector4 vector} with the position of the camera, in
     *            world-space.
     * @return A new look-at {@link Matrix4 matrix}.
     */
    public static Matrix4 lookAt(final Quaternion orientation, final Vector4 position) {
        return lookAt(orientation, new Point3f(position));
    }

    @Override
    public float value(int row, int col) {
        return matrix[col][row];
    }

    @Override
    public Vector4 row(int row) {
        // @formatter:off
        return new Vector4f(
            matrix[0][row],
            matrix[1][row],
            matrix[2][row],
            matrix[3][row]
        );
        // @formatter:on
    }

    @Override
    public Vector4 column(int col) {
        // @formatter:off
        return new Vector4f(
            matrix[col][0],
            matrix[col][1],
            matrix[col][2],
            matrix[col][3]
        );
        // @formatter:on
    }

    @Override
    public Matrix4 add(final Matrix4 m) {
        final float m00 = matrix[0][0] + m.value(0, 0);
        final float m01 = matrix[0][1] + m.value(1, 0);
        final float m02 = matrix[0][2] + m.value(2, 0);
        final float m03 = matrix[0][3] + m.value(3, 0);
        final float m10 = matrix[1][0] + m.value(0, 1);
        final float m11 = matrix[1][1] + m.value(1, 1);
        final float m12 = matrix[1][2] + m.value(2, 1);
        final float m13 = matrix[1][3] + m.value(3, 1);
        final float m20 = matrix[2][0] + m.value(0, 2);
        final float m21 = matrix[2][1] + m.value(1, 2);
        final float m22 = matrix[2][2] + m.value(2, 2);
        final float m23 = matrix[2][3] + m.value(3, 2);
        final float m30 = matrix[3][0] + m.value(0, 3);
        final float m31 = matrix[3][1] + m.value(1, 3);
        final float m32 = matrix[3][2] + m.value(2, 3);
        final float m33 = matrix[3][3] + m.value(3, 3);
        // @formatter:off
        return new Matrix4f(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
        // @formatter:on
    }

    @Override
    public Matrix4 sub(final Matrix4 m) {
        final float m00 = matrix[0][0] - m.value(0, 0);
        final float m01 = matrix[0][1] - m.value(1, 0);
        final float m02 = matrix[0][2] - m.value(2, 0);
        final float m03 = matrix[0][3] - m.value(3, 0);
        final float m10 = matrix[1][0] - m.value(0, 1);
        final float m11 = matrix[1][1] - m.value(1, 1);
        final float m12 = matrix[1][2] - m.value(2, 1);
        final float m13 = matrix[1][3] - m.value(3, 1);
        final float m20 = matrix[2][0] - m.value(0, 2);
        final float m21 = matrix[2][1] - m.value(1, 2);
        final float m22 = matrix[2][2] - m.value(2, 2);
        final float m23 = matrix[2][3] - m.value(3, 2);
        final float m30 = matrix[3][0] - m.value(0, 3);
        final float m31 = matrix[3][1] - m.value(1, 3);
        final float m32 = matrix[3][2] - m.value(2, 3);
        final float m33 = matrix[3][3] - m.value(3, 3);
        // @formatter:off
        return new Matrix4f(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
        // @formatter:on
    }

    @Override
    public Matrix4 mult(float s) {
        final float m00 = matrix[0][0] * s;
        final float m01 = matrix[0][1] * s;
        final float m02 = matrix[0][2] * s;
        final float m03 = matrix[0][3] * s;
        final float m10 = matrix[1][0] * s;
        final float m11 = matrix[1][1] * s;
        final float m12 = matrix[1][2] * s;
        final float m13 = matrix[1][3] * s;
        final float m20 = matrix[2][0] * s;
        final float m21 = matrix[2][1] * s;
        final float m22 = matrix[2][2] * s;
        final float m23 = matrix[2][3] * s;
        final float m30 = matrix[3][0] * s;
        final float m31 = matrix[3][1] * s;
        final float m32 = matrix[3][2] * s;
        final float m33 = matrix[3][3] * s;
        // @formatter:off
        return new Matrix4f(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
        // @formatter:on
    }

    @Override
    public Vector4 mult(final Vector4 v) {
        final float tx = matrix[0][0] * v.x() + matrix[1][0] * v.y() + matrix[2][0] * v.z() + matrix[3][0] * v.w();
        final float ty = matrix[0][1] * v.x() + matrix[1][1] * v.y() + matrix[2][1] * v.z() + matrix[3][1] * v.w();
        final float tz = matrix[0][2] * v.x() + matrix[1][2] * v.y() + matrix[2][2] * v.z() + matrix[3][2] * v.w();
        final float tw = matrix[0][3] * v.x() + matrix[1][3] * v.y() + matrix[2][3] * v.z() + matrix[3][3] * v.w();
        return new Vector4f(tx, ty, tz, tw);
    }

    @Override
    public Matrix4 mult(final Matrix4 m) {
        // @formatter:off
        final float m00 = value(0, 0) * m.value(0, 0) + value(0, 1) * m.value(1, 0)
                        + value(0, 2) * m.value(2, 0) + value(0, 3) * m.value(3, 0);
        final float m10 = value(0, 0) * m.value(0, 1) + value(0, 1) * m.value(1, 1)
                        + value(0, 2) * m.value(2, 1) + value(0, 3) * m.value(3, 1);
        final float m20 = value(0, 0) * m.value(0, 2) + value(0, 1) * m.value(1, 2)
                        + value(0, 2) * m.value(2, 2) + value(0, 3) * m.value(3, 2);
        final float m30 = value(0, 0) * m.value(0, 3) + value(0, 1) * m.value(1, 3)
                        + value(0, 2) * m.value(2, 3) + value(0, 3) * m.value(3, 3);
        final float m01 = value(1, 0) * m.value(0, 0) + value(1, 1) * m.value(1, 0)
                        + value(1, 2) * m.value(2, 0) + value(1, 3) * m.value(3, 0);
        final float m11 = value(1, 0) * m.value(0, 1) + value(1, 1) * m.value(1, 1)
                        + value(1, 2) * m.value(2, 1) + value(1, 3) * m.value(3, 1);
        final float m21 = value(1, 0) * m.value(0, 2) + value(1, 1) * m.value(1, 2)
                        + value(1, 2) * m.value(2, 2) + value(1, 3) * m.value(3, 2);
        final float m31 = value(1, 0) * m.value(0, 3) + value(1, 1) * m.value(1, 3)
                        + value(1, 2) * m.value(2, 3) + value(1, 3) * m.value(3, 3);
        final float m02 = value(2, 0) * m.value(0, 0) + value(2, 1) * m.value(1, 0)
                        + value(2, 2) * m.value(2, 0) + value(2, 3) * m.value(3, 0);
        final float m12 = value(2, 0) * m.value(0, 1) + value(2, 1) * m.value(1, 1)
                        + value(2, 2) * m.value(2, 1) + value(2, 3) * m.value(3, 1);
        final float m22 = value(2, 0) * m.value(0, 2) + value(2, 1) * m.value(1, 2)
                        + value(2, 2) * m.value(2, 2) + value(2, 3) * m.value(3, 2);
        final float m32 = value(2, 0) * m.value(0, 3) + value(2, 1) * m.value(1, 3)
                        + value(2, 2) * m.value(2, 3) + value(2, 3) * m.value(3, 3);
        final float m03 = value(3, 0) * m.value(0, 0) + value(3, 1) * m.value(1, 0)
                        + value(3, 2) * m.value(2, 0) + value(3, 3) * m.value(3, 0);
        final float m13 = value(3, 0) * m.value(0, 1) + value(3, 1) * m.value(1, 1)
                        + value(3, 2) * m.value(2, 1) + value(3, 3) * m.value(3, 1);
        final float m23 = value(3, 0) * m.value(0, 2) + value(3, 1) * m.value(1, 2)
                        + value(3, 2) * m.value(2, 2) + value(3, 3) * m.value(3, 2);
        final float m33 = value(3, 0) * m.value(0, 3) + value(3, 1) * m.value(1, 3)
                        + value(3, 2) * m.value(2, 3) + value(3, 3) * m.value(3, 3);

        return new Matrix4f(
            m00, m10, m20, m30,
            m01, m11, m21, m31,
            m02, m12, m22, m32,
            m03, m13, m23, m33
        );
        // @formatter:on
    }

    @Override
    public Matrix4 rotate(final Angle x, final Angle y, final Angle z) {
        Matrix4 rx = rotation(x, Vector4f.unitX());
        Matrix4 ry = rotation(y, Vector4f.unitY());
        Matrix4 rz = rotation(z, Vector4f.unitZ());
        return mult(rx).mult(ry).mult(rz);
    }

    @Override
    public Matrix4 rotate(final Angle angle, final Vector3 axis) {
        return mult(rotation(angle, axis));
    }

    @Override
    public Matrix4 rotate(final Angle angle, final Vector4 axis) {
        return mult(rotation(angle, axis));
    }

    @Override
    public Matrix4 translate(float tx, float ty, float tz) {
        return mult(translation(tx, ty, tz));
    }

    @Override
    public Matrix4 translate(final Vector3 tv) {
        return translate(tv.x(), tv.y(), tv.z());
    }

    @Override
    public Matrix4 translate(final Point3 p) {
        return translate(p.x(), p.y(), p.z());
    }

    @Override
    public Matrix4 translate(final Vector4 tv) {
        return translate(tv.x(), tv.y(), tv.z());
    }

    @Override
    public Matrix4 scale(float sx, float sy, float sz) {
        return mult(scaling(sx, sy, sz));
    }

    @Override
    public Matrix4 scale(final Vector3 v) {
        return scale(v.x(), v.y(), v.z());
    }

    @Override
    public Matrix4 scale(final Vector4 v) {
        return scale(v.x(), v.y(), v.z());
    }

    @Override
    public float determinant() {
        final float m[][] = matrix;
        // @formatter:off
        float a = m[0][0] * ( m[1][1] * m[2][2] * m[3][3] + m[2][1] * m[3][2] * m[1][3] + m[3][1] * m[1][2] * m[2][3]
                            - m[3][1] * m[2][2] * m[1][3] - m[1][1] * m[3][2] * m[2][3] - m[2][1] * m[1][2] * m[3][3]);
        float b = m[1][0] * ( m[0][1] * m[2][2] * m[3][3] + m[2][1] * m[3][2] * m[0][3] + m[3][1] * m[0][2] * m[2][3]
                            - m[3][1] * m[2][2] * m[0][3] - m[0][1] * m[3][2] * m[2][3] - m[2][1] * m[0][2] * m[3][3]);
        float c = m[2][0] * ( m[0][1] * m[1][2] * m[3][3] + m[1][1] * m[3][2] * m[0][3] + m[3][1] * m[0][2] * m[1][3]
                            - m[3][1] * m[1][2] * m[0][3] - m[0][1] * m[3][2] * m[1][3] - m[1][1] * m[0][2] * m[3][3]);
        float d = m[3][0] * ( m[0][1] * m[1][2] * m[2][3] + m[1][1] * m[2][2] * m[0][3] + m[2][1] * m[0][2] * m[1][3]
                            - m[2][1] * m[1][2] * m[0][3] - m[0][1] * m[2][2] * m[1][3] - m[1][1] * m[0][2] * m[2][3]);
        // @formatter:on
        return a - b + c - d;
    }

    @Override
    public Matrix4 inverse() {
        Vector4 a = column(0);
        Vector4 b = column(1);
        Vector4 c = column(2);
        Vector4 d = column(3);
        return inverse(a, b, c, d);
    }

    @Override
    public Matrix4 transpose() {
        // @formatter:off
        return transpose(
            matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0],
            matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1],
            matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2],
            matrix[0][3], matrix[1][3], matrix[2][3], matrix[3][3]
        );
        // @formatter:on
    }

    @Override
    public Matrix3 toMatrix3() {
        // @formatter:off
        return new Matrix3f(
            value(0, 0), value(0, 1), value(0, 2),
            value(1, 0), value(1, 1), value(1, 2),
            value(2, 0), value(2, 1), value(2, 2)
        );
        // @formatter:on
    }

    /**
     * The values are ordered as documented {@link Matrix4f here}.
     *
     * @see Bufferable#toFloatArray()
     */
    @Override
    public float[] toFloatArray() {
        return MatrixUtil.toFlatArray(matrix);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        final int result = 3;
        return prime * result + matrix.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Matrix4))
            return false;

        Matrix4 other = (Matrix4) obj;
        return MatrixUtil.areEqual(this, other, DIMENSIONS);
    }

    @Override
    public String toString() {
        // TODO: Improve formatting
        StringBuilder fmt = new StringBuilder();
        fmt.append(Matrix4f.class.getSimpleName() + " = [%9.5f | %9.5f | %9.5f | %9.5f]%n");
        fmt.append("           [%9.5f | %9.5f | %9.5f | %9.5f]%n");
        fmt.append("           [%9.5f | %9.5f | %9.5f | %9.5f]%n");
        fmt.append("           [%9.5f | %9.5f | %9.5f | %9.5f]");

        // @formatter:off
        return String.format(fmt.toString(),
            matrix[0][0], matrix[1][0], matrix[2][0], matrix[3][0],
            matrix[0][1], matrix[1][1], matrix[2][1], matrix[3][1],
            matrix[0][2], matrix[1][2], matrix[2][2], matrix[3][2],
            matrix[0][3], matrix[1][3], matrix[2][3], matrix[3][3]
        );
        // @formatter:on
    }
}
