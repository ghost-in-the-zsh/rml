# Angles

Most math libraries[^1] use primitive types, such as `float` and `double`, to measure angles. This is probably for performance reasons[^2]. However, it makes more sense to write code in the domain of the problem and have the code be easier to understand, so RML uses actual `Angle` types instead. This way, if you have a scalar variable with the value `1.5`, you don't have to guess if you are looking at degrees or radians; the code makes it obvious at first glance.

[^1]: That I have come across anyway.
[^2]: Though I'm not sure it has been benchmarked.

## Where are they in RML?

| Interface | Class     | Description                    |
| :---      | :---      | :----------                    |
| `Angle`   | `Degreef` | An angle, measured in degrees. |
| `Angle`   | `Radianf` | An angle, measured in radians. |


## Usage Examples

### Creating Angles

There are only two constructors for implementation classes with one taking a primitive `float` and the other taking an `Angle` as parameters.

```java
Angle d0 = new Degreef(90.0f);
Angle r0 = new Radianf((float)Math.PI);
Angle d1 = new Degreef(r0);
Angle r1 = new Radianf(d0);
```

Note that when passing a primitive `float` to a constructor, it is assumed that the value is already in the correct unit of measure.


### Basic Operations

Assume we have two `Angle` objects called _**a**_ and _**b**_.

#### Addition and Subtraction

```java
Angle c = a.add(b); // c = a + b
Angle d = a.sub(b); // d = a - b
```

#### Multiplication and Division

```java
Angle c = a.mult(b); // c = a * b
Angle d = a.div(b);  // d = a / b
```

#### Extracting Primitive Values

The `Angle` interface has two methods to extract the correct value as a primitive type:

```java
float radians = angle.valueRadians();
float degrees = angle.valueDegrees();
```

You are done here. You are now ready for [vectors](02-vectors.md).
