# RML Tutorial

## Introduction

This is tutorial _briefly_ covers and/or refreshes some key concepts, how they are representated by the system, and a few usage examples. Its main goal is to _refresh your intuition_ on concepts you may not have used in a while, but in a way that is informal and practical. It is _not_ meant to be exhaustive or a substitute for more formal or in-depth training if you have not covered the topics before.

In most cases, implementation classes are named the same as the interfaces they implement, but with a trailing `f` to indicate that they're `float`-based implementations (e.g. `Matrix4f`, `Vector3f`, etc.). In addition, _all_ object instances are _immutable_ and cannot be modified after creation. If a value needs to be changed, a new object must be created. This means operations _should be seen as value-returning expressions instead of statements_.


## Table of Contents

1. [Angles](01-angles.md)
2. [Vectors](02-vectors.md)
3. [Matrices](03-matrices.md)
4. [Quaternions](04-quaternions.md)
