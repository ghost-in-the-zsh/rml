# Matrices

Matrices are objects that store scalar values in rows and columns. They are very convenient data structures to store and transform vectors. Values in a matrix can be stored in row-major or column-major order. The common convention in math textbooks and graphics systems (e.g. OpenGL, Vulkan) is to use column-major order, and this is how _all_ matrices and vectors in RML are stored.

You can quickly identify if row-major or column-major matrices and vectors are being used in a system based on how expressions between them are written. Row-major systems always have vectors on the _left-hand side_ of a matrix whereas systems using a column-major order always have vectors on the _right-hand side_ of the matrix.

For example, if we have a matrix _**M**_ and a vector _**v**_, then _**v' = vM**_ shows that row-major order is being used, and _**v' = Mv**_ shows column-major order is being used.

**Important:** Remember that some matrix operations (e.g. multiplication) are _not_ commutative. For example, _**AB**_ and _**BA**_ are not (generally) equal.


## Why matrices?

There're several benefits to using matrices. Key reasons include:

1. **Efficiency:** Matrices are good data structures to store (column) vectors and accumulate transforms.
2. **Convenience:** Matrices can represent one or more (concatenated) coordinate space transforms, reducing the total number of required operations (i.e. multiply/adds).
3. **Intuitive:** Since each matrix column is a vector, it is easier to interpret its contents at a glance.
4. **Standard:** Low-level graphics APIs (e.g. OpenGL, Vulkan) expect to receive matrices, including shader programs.

There are more reasons, but these are probably the most important ones.


## Where are they in RML?

| Interface | Class      | Description                                                          |
| :---      | :---       | :----------                                                          |
| `Matrix3` | `Matrix3f` | An immutable single-precision floating-point 3x3 column-major matrix |
| `Matrix4` | `Matrix4f` | An immutable single-precision floating-point 4x4 column-major matrix |


## Usage Examples

### Creating Matrices

There're several constructors and static factory methods.

```java
Matrix3 M = new Matrix3f(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9
            );
Matrix3 I = Matrix3f.identity();
Matrix3 Z = Matrix3f.zero();
Matrix4 N = new Matrix4f(M);    // 4x4 from 3x3 matrix
```

In the cases above, _**M**_'s internal values are stored exactly as writen, i.e. _**M**_'s internal layout is:

```text
    | 1  2  3 |
M = | 4  5  6 |
    | 7  8  9 |
```

There're also constructors and static factory methods that use array-based inputs. In these cases, the values within the arrays _**must**_ already be in column-major order. For a new matrix _**M**_ to be identical to the previous _**M**_, the array-based argument must be transposed:

```java
float[] a = new float[] {
                1, 4, 7,        // column 0
                2, 5, 8,        // column 1
                3, 6, 9         // column 2
            };
float[][] b = new float[][] {
                { 1, 4, 7 },    // column 0
                { 2, 5, 8 },    // column 1
                { 3, 6, 9 }     // column 2
            };
Matrix3 N = new Matrix3f(a);
Matrix3 M = new Matrix3f(b);
```

Matrices can also be created from vectors:

```java
Vector4 a = new Vector4f( 1,  2,  3,  4);
Vector4 b = new Vector4f( 5,  6,  7,  8);
Vector4 c = new Vector4f( 9, 10, 11, 12);
Vector4 d = new Vector4f(13, 14, 15, 16);

Matrix4 m = new Matrix4f(a, b, c, d);
```

Since these are column-vectors, the layout of our new matrix _**M**_ is:

```text
      a  b   c   d

    | 1  5   9  13 |
M = | 2  6  10  14 |
    | 3  7  11  15 |
    | 4  8  12  16 |
```


### Basic Operations

Assume:

* _**M**_ and _**N**_ are two 3x3 matrices,
* _**v**_ is a 3D vector, and
* _**s**_ is a scalar value


#### Addition and Subtraction

```java
Matrix3 A = M.add(N);   // A = M + N
Matrix3 A = M.sub(N);   // A = M - N
```

#### Multiplication

Multiplication is between matrices, vectors, and scalars:

```java
Matrix3 A = M.mult(N);  // A  = MN
Vector3 V = M.mult(v);  // v' = Mv
Matrix3 A = M.mult(s);  // A  = Ms
```

Note that matrix-vector multiplication _**Mv**_ must be written as `M.mult(v)`; writing `v.mult(M)` would communicate a row-major order and is not allowed. Matrix division is also not defined, but here's where inverses come in handy.


### Other Operations

#### Inverses

While matrix division is not defined, you can achieve your goal by multiplying a matrix by its inverse. Multiplying a matrix by its inverse is analogous to multiplication of scalars by their reciprocals. That is, _**I = MM<sup>-1</sup>**_, where _**I**_ is the identity matrix.

```java
Matrix3 I = M.mult(M.inverse());
```

Any matrix _**M**_ that gets multiplied by the identity matrix _**I**_ results in itself, i.e. _**M = MI**_; it is analogous to multiplying a scalar by 1.

Static factory methods can also be used to build inverse matrices.

```java
float[] a = new float[] { ... };
Matrix3 M = new Matrix3f(a);
Matrix3 W = Matrix3f.inverse(a);    // 'W' is an inverted 'M', get it?
```

**Important:** Keep in mind that _some_ matrices _cannot_ be inverted. These matrices are called _singular_ and/or _non-invertible_. A matrix is singular when its determinant is zero. RML will throw a `SingularMatrixException` when attempting to invert such a matrix.


#### Transpose

There are several ways in which a transpose matrix _**M<sup>T</sup>**_ can be built:

```java
Matrix3 T = Matrix3f.transpose(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9
            );
Matrix3 T = M.transpose();
```

In both cases, matrix _**T = M<sup>T</sup>**_:

```text
    | 1  4  7 |
T = | 2  5  8 |
    | 3  6  9 |
```


## Transforms

A transform is an operation that moves data from one coordinate system/space into another. The transforms we care about here are: object space → world space → view space → clip space.

Coordinate systems, and vertices in said coordinate space, are relative to an arbitrary origin location within their respective spaces. Briefly:

1. **Object Space:** is relative to a given 3D model's origin, based on how the model is defined.
2. **World Space:** is relative to a common world origin, based on translations, rotations, and scaling transforms applied to a vertex in object-space.
3. **View Space:** is relative to the camera/observer, based on the viewing transform applied to vertices in world-space.
4. **Clip Space:** is the clipping volume, based on a projection transform applied to a vertex in view-space, where anything outside the viewing volume is removed or "clipped".

All of these transforms are stored in 4x4 matrices. While each transform can be stored in its own independent matrix, it is often a good idea to concatenate multiple transforms into a single transformation matrix. For example _**M = TRS**_ shows a transform matrix _**M**_ as the concatenation of a translation matrix _**T**_, rotation matrix _**R**_, and scaling matrix _**S**_, read and applied from right to left. The transform in matrix _**M**_ moves vertices from object-space[^1] into world-space.

[^1]: This matrix is called the model matrix, even though world matrix would probably be a more fitting name.


#### Translations

Only 4x4 matrices, based on the `Matrix4` type, hold transformations. To create a translation matrix _**T**_:

```java
Matrix4 T = Matrix4f.translation(x, y, z);
```

where _**x**_, _**y**_, and _**z**_ are the translation amounts along their respective axes. Its layout looks as follows:

```text
    | 1  0  0  x |
T = | 0  1  0  y |
    | 0  0  1  z |
    | 0  0  0  1 |
```

There're also methods that build translations out of 3D and 4D vectors.


#### Scaling

To create a scaling matrix _**S**_:

```java
Matrix4 S = Matrix4f.scaling(x, y, z);
```

where _**x**_, _**y**_, and _**z**_ are the scaling amounts along their respective axes.

```text
    | x  0  0  0 |
S = | 0  y  0  0 |
    | 0  0  z  0 |
    | 0  0  0  1 |
```
Again, there're also methods that take 3D and 4D vectors.


#### Rotations

Rotation matrices are created from an angle and an axis of rotation. Assuming we have an angle _**t**_ and a normalized vector _**v**_, to create a rotation matrix _**R**_ simply write:

```java
Matrix4 R = Matrix4f.rotation(t, v);
```
Rotations can also be stored in 3x3 matrices if no additional information is needed.


#### View Matrix

A view matrix _**V**_ contains the viewing transform, which is used to move vertices from world-space into view-space[^2]. Assuming we have the following vectors:

1. _**u**_: A normalized vector pointing in the _right_ direction, in world-space.
2. _**v**_: A normalized vector pointing in the _up_ direction, in world-space.
3. _**n**_: A normalized vector pointing in the _forward_ direction, in world-space.
4. _**p**_: A vector with the camera position, in world-space.

a view matrix can be created as follows:

```java
Matrix4 V = Matrix4f.view(u, v, n, p);
```

A vector _**v**_ can be moved from object-space into view-space with the following transform _**v' = VMv**_, where _**M**_ is the model matrix; i.e.

```java
Vector4 t = V.mult(M.mult(v));
```

[^2]: Also referred to as camera-space and eye-space.


#### Look-at Matrix

Another way to calculate a viewing transform from a different set of data is by using a look-at matrix. The look-at matrix is a view matrix calculated from the following vectors:

1. _**e**_: The observer's (i.e. eye) position, in world-space.
2. _**t**_: The target's position, in world-space.
3. _**u**_: The global _up_ direction, in world-space.

Assuming these vectors, a look-at matrix _**V**_ can be created as follows:

```java
Matrix4 V = Matrix4f.lookAt(e, t, u);
```


#### Projection Matrix

A projection matrix is used to transform a vertex from view-space into clip-space. The projection of interest for us is the _perspective_ projection[^3]. This projection is the one that makes objects farther away from the camera appear smaller and those closer to the camera appear larger.

To build this matrix, you need the following parameters:

1. **Vertical Field of View:** The angle between the top and bottom of the view frustum relative to the forward vector; it specifies the field of view for the camera, often abbreviated as _fovy_.
2. **Aspect Ratio:** The ratio between the width and height of the screen, calculated as `width / height`.
3. **Near Plane:** A positive value specifying the near clipping plane. Objects that are closer than this value get clipped and are not displayed.
4. **Far Plane:** A positive value, greater than the near plane, specifing the far clipping plane. Objects farther away than this value get clipped and are not displayed.

Assuming the variables _**fovy**_, _**aspect**_, _**near**_, and _**far**_ exist as described above, we can create a perspective projection matrix _**P**_ as follows:

```java
Matrix4 P = Matrix4f.perspective(fovy, aspect, near, far);
```

The model, viewing, and projection transforms are often concatenated into a single matrix, called the model-view-projection matrix, or _**MVP**_ for short. In RML, this can be done as follows (keeping in mind that the column-major nature of the matrices means they're evaluated right-to-left):

```java
Matrix4 M = ...
Matrix4 V = ...
Matrix4 P = ...

Matrix4 MVP = P.mult(V.mult(M));
```
To immediately transform a vertex _**v**_ from object-space to clip-space, simply multiply it by the model-view-projection matrix: _**v' = (PVM)v**_:

```java
Matrix4 C = P.mult(V.mult(M));  // C for clip-space
Vector4 s = C.mult(v);
```

You are done here. You are now ready for [quaternions](04-quaternions.md).


[^3]: There's also the _orthographic_ projection, but this is not supported in RML because there is not need for it (yet?).
