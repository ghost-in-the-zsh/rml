# Vectors

Vectors are objects that represent a _direction_ and a _length_[^1] along that direction. Vectors are encoded as N-dimensional tuples, with 3-component tuples being used for 3D vectors. For homogeneous coordinates, 4D vectors are used. RML uses _column_-vectors, a detail that will be more important when working with matrices.


## Where are they in RML?

| Interface | Class      | Description                                                   |
| :---      | :---       | :----------                                                   |
| `Vector3` | `Vector3f` | An immutable single-precision floating-point 3D column vector |
| `Vector4` | `Vector4f` | An immutable single-precision floating-point 4D column vector |


## Usage Examples

### Creating Vectors

There're several constructors and static factory methods.

```java
Vector3 a = new Vector3f(1, 2);
Vector3 b = new Vector3f(1, 2, 3);
Vector3 c = new Vector3f(new float[] { 1, 2, 3 });
Vector3 d = Vector3f.unitX();
Vector3 e = Vector3f.normalize(2, 2, 2);
```

The same applies to `Vector4`s. Simply note the following distinction: a `Vector4` meant to specify a position in space (i.e. a _positional_ vector) must have `w = 1`, whereas a `Vector4` meant to specify a direction (i.e. a _directional_ vector) must have `w = 0`. That is,

```java
Vector4 pos = new Vector4f(1, 1, 1, 1);
Vector4 dir = new Vector4f(1, 1, 1, 0);
```

When interested in any vector's direction in an expression, the vector should be _normalized_ to avoid introducing unintended side-effects to the results.


### Basic Operations

Assume 3D vectors _**a**_ and _**b**_ and a scalar variable _**s**_ exist.


#### Addition and Subtraction

```java
Vector3 c = a.add(b);   // c = a + b
Vector3 d = a.sub(b);   // d = a - b
```


#### Multiplication and Division

Note that this operation only affects the vector's length by "scaling" it; it does not change its direction.

```java
Vector3 c = a.mult(s);  // c = a * s
Vector3 d = a.div(s);   // d = a / s
```

**Note:** Multiplication and division between two vectors is _not uniquely defined_[^2].


#### Length and Normalization

Calculating the length[^1] and normalizing a generic vector _**v**_:

```java
float s   = v.length();
Vector4 n = v.normalize();
```


### Other Operations

#### Dot Product

The dot product[^3] between two normalized vectors, expressed as _**a · b**_, gives you a scalar value that is the _cosine of the angle_ between them, i.e. a value in the `[-1, 1]` range, which is the domain of the cosine function. To get the actual angle, in radians, you must get the inverse cosine of the dot product, i.e. _**cos<sup>-1</sup>(a · b)**_:

```java
float cosAngle  = a.dot(b);
float angleRads = (float) Math.acos(cosAngle);
```

A simpler way to do this is to rely on RML functionality.

```java
Angle t = a.angleTo(b);
```


#### Cross Product

The cross-product[^4] between two vectors, expressed as _**a × b**_, results in a new vector that is perpendicular to the two original ones, i.e. it is at a right angle relative to the plane on which the two original vectors exist.

```java
Vector3 c = a.cross(b);
```

The cross-product is only defined for 3D vectors[^5], not 4D vectors[^6].

Remember that, unlike the dot product, the cross product is _not_ commutative, i.e. _**(a × b) ≠ (b × a)**_ because they result in orthogonal vectors that point in _opposite_ directions. What you actually need for the equality to hold is _**(a × b) = (-b × a)**_.

You are done here. You are now ready for [matrices](03-matrices.md).


[^1]: Also called the _magnitude_.
[^2]: http://mathworld.wolfram.com/VectorMultiplication.html
[^3]: Also called the _inner product_ and _scalar product_.
[^4]: Also called the _outter product_ and _cartesian product_.
[^5]: Also for 7D vectors, but RML doesn't need these.
[^6]: You can still convert 4D vectors into 3D vectors and then calculate it from there.
