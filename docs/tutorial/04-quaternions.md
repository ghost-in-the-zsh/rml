# Quaternions

Quaternions are objects that describe a rotation about an axis in 3D space. Quaternions are encoded as 4-element tuples, having a _scalar_ part _w_ and a 3D _vector_ part _**v**_.

The scalar part _w_ is not encoded as an angle directly. Rather, it is encoded as the _cosine of the half-angle_. This means that a quaternion _**q**_ is encoded as _[w, **v**]_, or more specifically, as _[w, **v=(x, y, z)**]_.


## Why quaternions?

Quaternions have several advantages over matrices in some areas. A few include:

1. **Efficiency:** You encode a rotation with four 32-bit values (16 bytes) instead of the nine 32-bit values required in a 3x3 matrix (36 bytes).
2. **Gimbal Lock:** Unlike matrices, quaternions are _not_ susceptible to gimbal lock, which causes the loss of a rotation axis.
3. **Interpolation:** Quaternion interpolation is very useful for animations.

However, these advantages come with some trade-offs. For example, they're not necessarily more intuitive or easy to interpret at a glance. Also, the graphics pipeline does not understand quaternions, so they must be converted into 3x3 matrices before they can be sent to the GPU.


## Where are they in RML?

| Interface    | Class         | Description                                             |
| :---         | :---          | :----------                                             |
| `Quaternion` | `Quaternionf` | An immutable single-precision floating point quaternion |


## Usage Examples

### Creating Quaternions

There're several constructors and static factory methods.

```java
Quaternion q = new Quaternionf(0, 1, 1, 1);
Quaternion p = new Quaternionf(0, new Vector3f(1, 0, 0));
Quaternion i = Quaternionf.identity();
Quaternion z = Quaternionf.zero();
Quaternion n = Quaternionf.normalize(new Degreef(45), Vector3f.unitY());
```

### Basic Operations

Assume quaternions _**p**_ and _**q**_ and scalar value _**s**_ exist

#### Addition and Subtraction

```java
Quaternion r = p.add(q);   // r = p + q
Quaternion r = p.sub(q);   // r = p - q
```

#### Multiplication

Multiplication is between quaternions and scalars:

```java
Quaternion r = p.mult(q);  // r = p * q
Quaternion r = p.mult(s);  // r = p * s
```

Quaternion division is not defined, but here is where inverses come in handy.


### Other Operations

#### Inverses

While quaternion division is not defined, multiplication by its inverse is. Multiplying a quaternion by its inverse is analogous to multiplication of scalars by their reciprocals. That is, _**I = qq<sup>-1</sup>**_ where _**I**_ is the identity quaternion _[1, **(0, 0, 0)**]_.

```java
Quaternion I = q.mult(q.inverse());
```


#### Conjugates

The conjugate of a quaternion _[w, **(x, y, z)**]_ is _[w, **(-x, -y, -z)**]_.

```java
Quaternion c = q.conjugate();
```


#### Length and Normalization

Just like vectors, quaternions have a length and can be normalized:

```java
float s      = q.length();
Quaternion p = q.normalize();
```


#### Interpolation

Quaternion interpolation can be done using LERP[^1] and SLERP[^2] by means of an interpolation parameter _**t**_ in the `[0,1]` range.

```java
Quaternion r = q.lerp(p, t);
Quaternion r = q.slerp(p, t);
```

[^1]: https://en.wikipedia.org/wiki/Linear_interpolation
[^2]: https://en.wikipedia.org/wiki/Slerp


#### Convertion to/from Matrices

Quaternions must eventually be converted into matrices before sending them over to the GPU pipeline. This can be done as follows:

```java
Matrix3 M = new Matrix3f(q);
```

Matrices can also be converted into quaternions. Assuming a 3x3 matrix _**M**_:

```java
Quaternion q = new Quaternionf(M);
```

Keep in mind that quaternions only represent rotations, therefore only 3x3 matrices can be created directly from them.


#### Rotations

Just like with matrices, quaternions can be used to rotate 3D vectors: _**v' = qv**_.

```java
v = q.rotate(v);
```


#### Angle and Axis

While you can get each quaternion component individually and perform your own calculations, RML allows you to get the angle and vector axis directly.

```java
Angle   a = q.angle();
Vector3 v = q.axis();
```
The axis vector will be normalized.


You are done here. Good luck!
